package BP.Sys;

import BP.DA.*;
import BP.En.*;
import BP.Port.*;
import BP.Web.*;

import java.util.*;
import java.io.*;
import java.time.*;
import java.math.*;

public class FrmEventListDtl {
    /**
     * 从表保存前
     */
    public static final String RowSaveBefore = "DtlRowSaveBefore";
    /**
     * 从表保存后
     */
    public static final String RowSaveAfter = "DtlRowSaveAfter";

    /**
     * 从表保存前
     */
    public static final String DtlRowDelBefore = "DtlRowDelBefore";
    /**
     * 从表保存后
     */
    public static final String DtlRowDelAfter = "DtlRowDelAfter";

}
