package BP.Frm;

import BP.DA.*;
import BP.En.*;
import BP.WF.Port.*;

import java.util.*;

/**
 * 单据可创建的人员属性
 */
public class EmpCreateAttr {
    /**
     * 表单ID
     */
    public static final String FrmID = "FrmID";
    /**
     * 人员
     */
    public static final String FK_Emp = "FK_Emp";
}
