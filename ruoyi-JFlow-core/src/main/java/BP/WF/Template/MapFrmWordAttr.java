package BP.WF.Template;

import BP.DA.*;
import BP.En.*;
import BP.WF.*;
import BP.Sys.*;
import BP.WF.*;

import java.util.*;

/**
 * Word表单属性 attr
 */
public class MapFrmWordAttr extends MapDataAttr {
    /**
     * 临时的版本号
     */
    public static final String TemplaterVer = "TemplaterVer";
    /**
     * 文件存储字段
     */
    public static final String DBSave = "DBSave";
}
