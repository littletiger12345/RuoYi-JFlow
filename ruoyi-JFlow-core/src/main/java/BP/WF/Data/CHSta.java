package BP.WF.Data;

import BP.DA.*;
import BP.En.*;
import BP.Port.*;
import BP.Web.*;
import BP.Sys.*;
import BP.WF.*;

import java.util.*;

/**
 * 完成状态
 */
public enum CHSta {
    /**
     * 按期完成
     */
    AnQi(0),
    /**
     * 预期完成
     */
    YuQi(1);

    public static final int SIZE = java.lang.Integer.SIZE;
    private static java.util.HashMap<Integer, CHSta> mappings;
    private int intValue;

    private CHSta(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    private static java.util.HashMap<Integer, CHSta> getMappings() {
        if (mappings == null) {
            synchronized (CHSta.class) {
                if (mappings == null) {
                    mappings = new java.util.HashMap<Integer, CHSta>();
                }
            }
        }
        return mappings;
    }

    public static CHSta forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
