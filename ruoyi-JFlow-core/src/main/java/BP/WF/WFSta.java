package BP.WF;

import BP.DA.*;
import BP.WF.*;
import BP.Port.*;
import BP.Sys.*;
import BP.En.*;
import BP.WF.Template.*;

import java.util.*;

/**
 * 流程状态(简)
 */
public enum WFSta {
    /**
     * 运行中
     */
    Runing(0),
    /**
     * 已完成
     */
    Complete(1),
    /**
     * 其他
     */
    Etc(2);

    public static final int SIZE = java.lang.Integer.SIZE;
    private static java.util.HashMap<Integer, WFSta> mappings;
    private int intValue;

    private WFSta(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    private static java.util.HashMap<Integer, WFSta> getMappings() {
        if (mappings == null) {
            synchronized (WFSta.class) {
                if (mappings == null) {
                    mappings = new java.util.HashMap<Integer, WFSta>();
                }
            }
        }
        return mappings;
    }

    public static WFSta forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
