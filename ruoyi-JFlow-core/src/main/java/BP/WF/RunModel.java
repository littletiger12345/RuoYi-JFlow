package BP.WF;

/**
 * 运行模式
 */
public enum RunModel {
    /**
     * 普通
     */
    Ordinary(0),
    /**
     * 合流
     */
    HL(1),
    /**
     * 分流
     */
    FL(2),
    /**
     * 分合流
     */
    FHL(3),
    /**
     * 子线程
     */
    SubThread(4);

    public static final int SIZE = java.lang.Integer.SIZE;
    private static java.util.HashMap<Integer, RunModel> mappings;
    private int intValue;

    private RunModel(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    private static java.util.HashMap<Integer, RunModel> getMappings() {
        if (mappings == null) {
            synchronized (RunModel.class) {
                if (mappings == null) {
                    mappings = new java.util.HashMap<Integer, RunModel>();
                }
            }
        }
        return mappings;
    }

    public static RunModel forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
