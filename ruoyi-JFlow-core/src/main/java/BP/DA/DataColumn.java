package BP.DA;

import java.util.HashMap;
import java.util.Map;

public class DataColumn {

    public final Properties ExtendedProperties = new Properties();
    /**
     * DataColumn的欄位名稱
     */
    public String ColumnName; // 欄名，當做DataRow的key
    public String oldColumnName; // 欄名，當做DataRow的key
    public Object DataType;
    /**
     * DataColumn所屬的DataTable
     */
    private DataTable table;
    private int ordinal;

    /**
     * DataColumn被建立時，一定要指定欄名
     *
     * @param columnName 欄名
     */
    public DataColumn(String columnName) {
        this.ColumnName = columnName;
        this.oldColumnName = columnName;
    }

    public DataColumn() {

    }

    public DataColumn(String columnName, Object DataType) {
        this.ColumnName = columnName;
        this.oldColumnName = columnName;
        this.DataType = DataType;
    }

    //区分大小写
    public DataColumn(String columnName, Object DataType, boolean cases) {
        this.ColumnName = columnName;
        this.oldColumnName = columnName;
        this.DataType = DataType;
    }

    public DataColumn(String columnName, Object DataType, String str) {
        this.ColumnName = columnName;
        this.oldColumnName = columnName;
        this.DataType = DataType;
    }

    public void setColumnName(String val) {
        if (oldColumnName == null)
            oldColumnName = ColumnName;

        ColumnName = val;
    }

    public Object getDataType() {
        return DataType;
    }

    public void setDataType(Object dataType) {
        DataType = dataType;
    }

    /**
     * 取得DataColumn所屬的DataTable，唯讀
     *
     * @return DataTable
     */
    public DataTable getTable() {
        return this.table;
    }

    /**
     * 給DataColumnCollection加入DataColumn時設定所屬的DataTable的方法，同一個package才用到
     *
     * @param table
     */
    void setTable(DataTable table) {
        this.table = table;
    }

    /**
     * DataColumn物件的toString()，會回傳自己的欄名
     *
     * @return
     */
    @Override
    public String toString() {
        return this.ColumnName;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    public static class Properties {

        private Map<Object, Object> properties = new HashMap<Object, Object>();

        public void Add(Object key, Object value) {
            this.properties.put(key, value);
        }

        public boolean ContainsKey(Object key) {
            return this.properties.containsKey(key);
        }

        public Object get(Object key) {
            return this.properties.get(key);
        }

    }

}
