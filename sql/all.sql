/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost:3306
 Source Schema         : ys

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 14/02/2020 15:40:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for frm_ctrlmodel
-- ----------------------------
DROP TABLE IF EXISTS `frm_ctrlmodel`;
CREATE TABLE `frm_ctrlmodel`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FrmID` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `CtrlObj` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控制权限',
  `IsEnableAll` int(11) NULL DEFAULT NULL COMMENT '任何人都可以',
  `IsEnableStation` int(11) NULL DEFAULT NULL COMMENT '按照岗位计算',
  `IsEnableDept` int(11) NULL DEFAULT NULL COMMENT '按照绑定的部门计算',
  `IsEnableUser` int(11) NULL DEFAULT NULL COMMENT '按照绑定的人员计算',
  `IDOfUsers` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绑定的人员ID',
  `IDOfStations` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绑定的岗位ID',
  `IDOfDepts` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绑定的部门ID',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '控制模型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of frm_ctrlmodel
-- ----------------------------

-- ----------------------------
-- Table structure for frm_ctrlmodeldtl
-- ----------------------------
DROP TABLE IF EXISTS `frm_ctrlmodeldtl`;
CREATE TABLE `frm_ctrlmodeldtl`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FrmID` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `CtrlObj` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控制权限',
  `OrgType` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织类型',
  `IDs` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'IDs',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '控制模型表Dtl' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of frm_ctrlmodeldtl
-- ----------------------------

-- ----------------------------
-- Table structure for frm_deptcreate
-- ----------------------------
DROP TABLE IF EXISTS `frm_deptcreate`;
CREATE TABLE `frm_deptcreate`  (
  `FrmID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表单 - 主键',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '可以创建部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FrmID`, `FK_Dept`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据部门' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of frm_deptcreate
-- ----------------------------

-- ----------------------------
-- Table structure for frm_empcreate
-- ----------------------------
DROP TABLE IF EXISTS `frm_empcreate`;
CREATE TABLE `frm_empcreate`  (
  `FrmID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表单 - 主键',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FrmID`, `FK_Emp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据可创建的人员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of frm_empcreate
-- ----------------------------

-- ----------------------------
-- Table structure for frm_generbill
-- ----------------------------
DROP TABLE IF EXISTS `frm_generbill`;
CREATE TABLE `frm_generbill`  (
  `WorkID` int(11) NOT NULL COMMENT 'WorkID - 主键',
  `FK_FrmTree` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据类别',
  `FrmID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据ID',
  `FrmName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据名称',
  `BillNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `Title` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `BillSta` int(11) NULL DEFAULT NULL COMMENT '状态(简),枚举类型:0 运行中;1 已完成;2 其他;',
  `BillState` int(11) NULL DEFAULT NULL COMMENT '单据状态,枚举类型:0 空白;1 草稿;2 编辑中;100 归档;',
  `Starter` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `StarterName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `Sender` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送人',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  `SendDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据活动时间',
  `NDStep` int(11) NULL DEFAULT NULL COMMENT '步骤',
  `NDStepName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '步骤名称',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门',
  `DeptName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `PRI` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `SDTOfNode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点应完成时间',
  `SDTOfFlow` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据应完成时间',
  `PFrmID` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父单据编号',
  `PWorkID` int(11) NULL DEFAULT NULL COMMENT '父单据ID',
  `TSpan` int(11) NULL DEFAULT NULL COMMENT '时间段,枚举类型:0 本周;1 上周;2 两周以前;3 三周以前;4 更早;',
  `AtPara` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数(单据运行设置临时存储的参数)',
  `Emps` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '参与人',
  `GUID` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `FK_NY` varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年月',
  PRIMARY KEY (`WorkID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据控制表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of frm_generbill
-- ----------------------------

-- ----------------------------
-- Table structure for frm_method
-- ----------------------------
DROP TABLE IF EXISTS `frm_method`;
CREATE TABLE `frm_method`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FrmID` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `MethodID` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方法ID',
  `MethodName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方法名',
  `WarningMsg` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '功能执行警告信息',
  `RefMethodType` int(11) NULL DEFAULT NULL COMMENT '方法类型,枚举类型:1 模态窗口打开;2 新窗口打开;3 右侧窗口打开;',
  `IsMyBillToolBar` int(11) NULL DEFAULT NULL COMMENT '是否显示在MyBill.htm工具栏上',
  `IsMyBillToolExt` int(11) NULL DEFAULT NULL COMMENT '是否显示在MyBill.htm工具栏右边的更多按钮里',
  `IsSearchBar` int(11) NULL DEFAULT NULL COMMENT '是否显示在Search.htm工具栏上(用于批处理)',
  `PopHeight` int(11) NULL DEFAULT NULL COMMENT '弹窗高度',
  `PopWidth` int(11) NULL DEFAULT NULL COMMENT '弹窗宽度',
  `MsgSuccess` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '成功提示信息',
  `MsgErr` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '失败提示信息',
  `WhatAreYouTodo` int(11) NULL DEFAULT NULL COMMENT '执行完毕后干啥？,枚举类型:0 关闭提示窗口;1 关闭提示窗口并刷新;2 转入到Search.htm页面上去;',
  `Idx` int(11) NULL DEFAULT NULL COMMENT 'Idx',
  `ShowModel` int(11) NULL DEFAULT NULL COMMENT '显示方式,枚举类型:0 按钮;1 超链接;',
  `MethodDocTypeOfFunc` int(11) NULL DEFAULT NULL COMMENT '内容类型,枚举类型:0 SQL;1 URL;2 JavaScript;3 业务单元;',
  `MethodDoc_Url` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '连接URL',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '表单方法' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of frm_method
-- ----------------------------

-- ----------------------------
-- Table structure for frm_stationcreate
-- ----------------------------
DROP TABLE IF EXISTS `frm_stationcreate`;
CREATE TABLE `frm_stationcreate`  (
  `FrmID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表单 - 主键',
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '可以创建岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FrmID`, `FK_Station`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据岗位' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of frm_stationcreate
-- ----------------------------

-- ----------------------------
-- Table structure for frm_track
-- ----------------------------
DROP TABLE IF EXISTS `frm_track`;
CREATE TABLE `frm_track`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `ActionType` int(11) NULL DEFAULT NULL COMMENT '类型',
  `ActionTypeText` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型(名称)',
  `FID` int(11) NULL DEFAULT NULL COMMENT '流程ID',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT '工作ID',
  `NDFrom` int(11) NULL DEFAULT NULL COMMENT '从节点',
  `NDFromT` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '从节点(名称)',
  `NDTo` int(11) NULL DEFAULT NULL COMMENT '到节点',
  `NDToT` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到节点(名称)',
  `EmpFrom` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '从人员',
  `EmpFromT` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '从人员(名称)',
  `EmpTo` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到人员',
  `EmpToT` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到人员(名称)',
  `RDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日期',
  `WorkTimeSpan` float NULL DEFAULT NULL COMMENT '时间跨度(天)',
  `Msg` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '消息',
  `NodeData` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '节点数据(日志信息)',
  `Tag` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `Exer` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '执行人',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '轨迹表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of frm_track
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '表描述',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成功能作者',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_app
-- ----------------------------
DROP TABLE IF EXISTS `gpm_app`;
CREATE TABLE `gpm_app`  (
  `No` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `AppModel` int(11) NULL DEFAULT NULL COMMENT '应用类型,枚举类型:0 BS系统;1 CS系统;',
  `Name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '名称',
  `FK_AppSort` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别,外键:对应物理表:GPM_AppSort,表描述:系统类别',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `Url` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '默认连接',
  `SubUrl` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '第二连接',
  `UidControl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名控件',
  `PwdControl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码控件',
  `ActionType` int(11) NULL DEFAULT NULL COMMENT '提交类型,枚举类型:0 GET;1 POST;',
  `SSOType` int(11) NULL DEFAULT NULL COMMENT '登录方式,枚举类型:0 SID验证;1 连接;2 表单提交;3 不传值;',
  `OpenWay` int(11) NULL DEFAULT NULL COMMENT '打开方式,枚举类型:0 新窗口;1 本窗口;2 覆盖新窗口;',
  `RefMenuNo` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '关联菜单编号',
  `AppRemark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  `MyFileName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ICON',
  `MyFilePath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) NULL DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) NULL DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float NULL DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gpm_app
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_appsort
-- ----------------------------
DROP TABLE IF EXISTS `gpm_appsort`;
CREATE TABLE `gpm_appsort`  (
  `No` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  `RefMenuNo` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联的菜单编号',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统类别' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gpm_appsort
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_bar
-- ----------------------------
DROP TABLE IF EXISTS `gpm_bar`;
CREATE TABLE `gpm_bar`  (
  `No` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '名称',
  `Title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '标题',
  `OpenWay` int(11) NULL DEFAULT NULL COMMENT '打开方式,枚举类型:0 新窗口;1 本窗口;2 覆盖新窗口;',
  `IsLine` int(11) NULL DEFAULT NULL COMMENT '是否独占一行',
  `MoreUrl` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '更多标签Url',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '信息块' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gpm_bar
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_baremp
-- ----------------------------
DROP TABLE IF EXISTS `gpm_baremp`;
CREATE TABLE `gpm_baremp`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Bar` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '信息块编号',
  `FK_Emp` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员编号',
  `Title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '标题',
  `IsShow` int(11) NULL DEFAULT NULL COMMENT '是否显示',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '人员信息块' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gpm_baremp
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_empapp
-- ----------------------------
DROP TABLE IF EXISTS `gpm_empapp`;
CREATE TABLE `gpm_empapp`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Emp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作员',
  `FK_App` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统',
  `Name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '系统-名称',
  `Url` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '连接',
  `MyFileName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) NULL DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) NULL DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float NULL DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员与系统权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gpm_empapp
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_empmenu
-- ----------------------------
DROP TABLE IF EXISTS `gpm_empmenu`;
CREATE TABLE `gpm_empmenu`  (
  `FK_Menu` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单 - 主键',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单功能,主外键:对应物理表:Port_Emp,表描述:用户',
  `IsChecked` int(11) NULL DEFAULT NULL COMMENT '是否选中',
  PRIMARY KEY (`FK_Menu`, `FK_Emp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '人员菜单对应' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gpm_empmenu
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_group
-- ----------------------------
DROP TABLE IF EXISTS `gpm_group`;
CREATE TABLE `gpm_group`  (
  `No` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gpm_group
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_groupemp
-- ----------------------------
DROP TABLE IF EXISTS `gpm_groupemp`;
CREATE TABLE `gpm_groupemp`  (
  `FK_Group` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限组 - 主键',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Group`, `FK_Emp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组人员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gpm_groupemp
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_groupmenu
-- ----------------------------
DROP TABLE IF EXISTS `gpm_groupmenu`;
CREATE TABLE `gpm_groupmenu`  (
  `FK_Group` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限组 - 主键',
  `FK_Menu` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单 - 主键',
  `IsChecked` int(11) NULL DEFAULT NULL COMMENT '是否选中',
  PRIMARY KEY (`FK_Group`, `FK_Menu`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gpm_groupmenu
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_groupstation
-- ----------------------------
DROP TABLE IF EXISTS `gpm_groupstation`;
CREATE TABLE `gpm_groupstation`  (
  `FK_Group` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限组 - 主键',
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Group`, `FK_Station`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组岗位' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gpm_groupstation
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_menu
-- ----------------------------
DROP TABLE IF EXISTS `gpm_menu`;
CREATE TABLE `gpm_menu`  (
  `No` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '功能编号 - 主键',
  `ParentNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父节点,外键:对应物理表:GPM_Menu,表描述:系统菜单',
  `Name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `MenuType` int(11) NULL DEFAULT NULL COMMENT '菜单类型,枚举类型:0 系统根目录;1 系统类别;2 系统;3 目录;4 功能/界面;5 功能控制点;',
  `FK_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统,外键:对应物理表:GPM_App,表描述:系统',
  `OpenWay` int(11) NULL DEFAULT NULL COMMENT '打开方式,枚举类型:0 新窗口;1 本窗口;2 覆盖新窗口;',
  `Url` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '连接',
  `UrlExt` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '连接',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用?',
  `Icon` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Icon',
  `MenuCtrlWay` int(11) NULL DEFAULT NULL COMMENT '控制方式,枚举类型:0 按照设置的控制;1 任何人都可以使用;2 Admin用户可以使用;',
  `Flag` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标记',
  `Tag1` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag3',
  `MyFileName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) NULL DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) NULL DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float NULL DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gpm_menu
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_persetting
-- ----------------------------
DROP TABLE IF EXISTS `gpm_persetting`;
CREATE TABLE `gpm_persetting`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Emp` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员',
  `FK_App` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统',
  `UserNo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'UserNo',
  `UserPass` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'UserPass',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '个人设置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gpm_persetting
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_stationmenu
-- ----------------------------
DROP TABLE IF EXISTS `gpm_stationmenu`;
CREATE TABLE `gpm_stationmenu`  (
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  `FK_Menu` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单 - 主键',
  `IsChecked` int(11) NULL DEFAULT NULL COMMENT '是否选中',
  PRIMARY KEY (`FK_Station`, `FK_Menu`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gpm_stationmenu
-- ----------------------------

-- ----------------------------
-- Table structure for jflow_dept
-- ----------------------------
DROP TABLE IF EXISTS `jflow_dept`;
CREATE TABLE `jflow_dept`  (
  `dept_id` bigint(50) NOT NULL,
  `NameOfPath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Idx` int(11) NULL DEFAULT 0 COMMENT '顺序号',
  `OrgNo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jflow_dept
-- ----------------------------

-- ----------------------------
-- Table structure for jflow_deptemp
-- ----------------------------
DROP TABLE IF EXISTS `jflow_deptemp`;
CREATE TABLE `jflow_deptemp`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门人员表',
  `FK_Dept` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jflow_deptemp
-- ----------------------------

-- ----------------------------
-- Table structure for jflow_deptempstation
-- ----------------------------
DROP TABLE IF EXISTS `jflow_deptempstation`;
CREATE TABLE `jflow_deptempstation`  (
  `MyPK` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FK_Dept` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FK_Station` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FK_Emp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jflow_deptempstation
-- ----------------------------

-- ----------------------------
-- Table structure for jflow_deptstation
-- ----------------------------
DROP TABLE IF EXISTS `jflow_deptstation`;
CREATE TABLE `jflow_deptstation`  (
  `FK_Dept` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`FK_Dept`, `FK_Station`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jflow_deptstation
-- ----------------------------

-- ----------------------------
-- Table structure for jflow_emp
-- ----------------------------
DROP TABLE IF EXISTS `jflow_emp`;
CREATE TABLE `jflow_emp`  (
  `user_id` bigint(20) NOT NULL,
  `SID` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PinYin` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `SignType` int(11) NULL DEFAULT 0,
  `Idx` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jflow_emp
-- ----------------------------

-- ----------------------------
-- Table structure for jflow_station
-- ----------------------------
DROP TABLE IF EXISTS `jflow_station`;
CREATE TABLE `jflow_station`  (
  `post_id` bigint(50) NOT NULL COMMENT '岗位id',
  `FK_StationType` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OrgNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jflow_station
-- ----------------------------

-- ----------------------------
-- Table structure for jflow_stationtype
-- ----------------------------
DROP TABLE IF EXISTS `jflow_stationtype`;
CREATE TABLE `jflow_stationtype`  (
  `No` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Idx` int(11) NULL DEFAULT 0 COMMENT '顺序',
  `OrgNo` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jflow_stationtype
-- ----------------------------

-- ----------------------------
-- Table structure for nd1rpt
-- ----------------------------
DROP TABLE IF EXISTS `nd1rpt`;
CREATE TABLE `nd1rpt`  (
  `Title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OID` int(11) NOT NULL,
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FID` int(11) NULL DEFAULT 0 COMMENT 'FID',
  `CDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Rec` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Emps` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FK_NY` varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MyNum` int(11) NULL DEFAULT 1 COMMENT '个数',
  `QJR` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '@WebUser.Name',
  `KSSJ` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JSSJ` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `QJYY` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `KZSPYJ_Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `KZSPYJ_Checker` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '@WebUser.No',
  `KZSPYJ_RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BZSPYJ_Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `BZSPYJ_Checker` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '@WebUser.No',
  `BZSPYJ_RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PNodeID` int(11) NULL DEFAULT 0,
  `GUID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `PEmp` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `PrjNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `BillNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `PrjName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `FlowNote` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `WFSta` int(11) NULL DEFAULT 0,
  `PWorkID` int(11) NULL DEFAULT 0,
  `FlowDaySpan` decimal(20, 4) NULL DEFAULT 0.0000 COMMENT '跨度(天)',
  `FlowEndNode` int(11) NULL DEFAULT 0,
  `FlowEnderRDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FlowStartRDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PFlowNo` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `FlowEmps` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `WFState` int(11) NULL DEFAULT 0,
  `FlowEnder` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `FlowStarter` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `XingMing` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nd1rpt
-- ----------------------------
INSERT INTO `nd1rpt` VALUES ('研发部门-admin,若依在2020-02-13 16:00发起.', 100, '2020-02-13 16:00', 0, '2020-02-13 16:00', 'admin', 'admin', '103', '2020-02', 1, '@WebUser.Name', '', '', '', '', '@WebUser.No', '', '', '@WebUser.No', '', 0, '', '', '', '', '', '', '', 2, 0, 0.0000, 101, '2020-02-12 14:31', '2020-02-12 14:31', '', '@admin,admin@', 2, 'admin', 'admin', '');
INSERT INTO `nd1rpt` VALUES ('研发部门-admin,若依在2020-02-13 16:04发起.', 101, '2020-02-13 16:04', 0, '2020-02-13 16:04', 'admin', 'admin', '100', '2020-02', 1, '若依', '2020-02-13', '2020-02-13', '325235', '', '@WebUser.No', '', '', '@WebUser.No', '', 0, '', '', '', '', '', '', '', 0, 0, 0.0000, 101, '2020-02-13 16:04', '2020-02-13 16:04', '', '@admin,若依@', 5, 'admin', 'admin', '');
INSERT INTO `nd1rpt` VALUES ('组织架构-admin,超级管理员在2020-02-13 18:44发起.', 102, '2020-02-13 18:44', 0, '2020-02-13 18:44', 'admin', 'admin', '100', '2020-02', 1, '超级管理员', '2020-02-13', '2020-02-13', '123123', '', '@WebUser.No', '', '', '@WebUser.No', '', 0, '', '', '', '', '', '', '', 0, 0, 0.0000, 103, '2020-02-13 18:44', '2020-02-13 18:44', '', '@超级管理员,admin@admin,超级管理员@', 2, 'admin', 'admin', '');
INSERT INTO `nd1rpt` VALUES ('组织架构-admin,超级管理员在2020-02-13 18:49发起.', 103, '2020-02-13 18:49', 0, '2020-02-13 18:49', 'admin', 'admin', '100', '2020-02', 1, '超级管理员', '2020-02-13', '2020-02-13', '', '', '@WebUser.No', '', '', '@WebUser.No', '', 0, '', '', '', '', '', '', '', 0, 0, 0.0000, 103, '2020-02-13 18:50', '2020-02-13 18:49', '', '@超级管理员,admin@admin,超级管理员@', 2, 'admin', 'admin', '123123');
INSERT INTO `nd1rpt` VALUES ('组织架构-admin,超级管理员在2020-02-13 18:49发起.', 104, '2020-02-13 18:49', 0, '2020-02-13 18:49', 'admin', 'admin', '100', '2020-02', 1, '@WebUser.Name', '', '', '', '', '@WebUser.No', '', '', '@WebUser.No', '', 0, '', '', '', '', '', '', '', 2, 0, 0.0000, 101, '2020-02-13 18:49', '2020-02-13 18:49', '', '@admin,超级管理员@', 0, 'admin', 'admin', '');

-- ----------------------------
-- Table structure for nd1track
-- ----------------------------
DROP TABLE IF EXISTS `nd1track`;
CREATE TABLE `nd1track`  (
  `MyPK` int(11) NOT NULL COMMENT 'MyPK - 主键',
  `ActionType` int(11) NULL DEFAULT NULL COMMENT '类型',
  `ActionTypeText` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型(名称)',
  `FID` int(11) NULL DEFAULT NULL COMMENT '流程ID',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT '工作ID',
  `NDFrom` int(11) NULL DEFAULT NULL COMMENT '从节点',
  `NDFromT` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '从节点(名称)',
  `NDTo` int(11) NULL DEFAULT NULL COMMENT '到节点',
  `NDToT` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到节点(名称)',
  `EmpFrom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '从人员',
  `EmpFromT` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '从人员(名称)',
  `EmpTo` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到人员',
  `EmpToT` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到人员(名称)',
  `RDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日期',
  `WorkTimeSpan` float NULL DEFAULT NULL COMMENT '时间跨度(天)',
  `Msg` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '消息',
  `NodeData` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '节点数据(日志信息)',
  `Tag` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `Exer` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '执行人',
  `FrmDB` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '轨迹表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nd1track
-- ----------------------------
INSERT INTO `nd1track` VALUES (22862671, 1, '发送', 0, 103, 101, '开始', 102, '申请', 'admin', '超级管理员', 'admin', '超级管理员', '2020-02-13 18:49:52', 0, '', '', 'null@SendNode=101', '(admin,超级管理员)', '{\"MyNum\":\"1\",\"CDT\":\"2020-02-13 18:49\",\"Rec\":\"admin\",\"RDT\":\"2020-02-13 18:49\",\"Title\":\"组织架构-admin,超级管理员在2020-02-13 18:49发起.\",\"Emps\":\"admin\",\"FK_Dept\":\"100\",\"FK_DeptText\":\"组织架构\",\"OID\":\"103\",\"FID\":\"0\",\"XingMing\":\"123123\",\"FK_NY\":\"2020-02\",\"OutEnd\":\"1\"}');
INSERT INTO `nd1track` VALUES (98571581, 1, '发送', 0, 102, 102, '申请', 103, '科长审批', 'admin', '超级管理员', 'zaoyun', '赵云', '2020-02-13 18:44:22', 0, '', '', 'null@SendNode=102', '(admin,超级管理员)', '{\"QJYY\":\"123123\",\"CDT\":\"2020-02-13 18:44\",\"QJR\":\"超级管理员\",\"KSSJ\":\"2020-02-13\",\"Rec\":\"admin\",\"RDT\":\"2020-02-13 18:44\",\"JSSJ\":\"2020-02-13\",\"Emps\":\"admin\",\"FK_Dept\":\"100\",\"OID\":\"102\",\"FID\":\"0\",\"OutEnd\":\"1\"}');
INSERT INTO `nd1track` VALUES (511287263, 201, '退回并原路返回', 0, 101, 102, '申请', 101, '开始', 'admin', '超级管理员', 'admin', '若依', '2020-02-13 18:39:30', 0, '345345', '', '', '(admin,超级管理员)', NULL);
INSERT INTO `nd1track` VALUES (1142873752, 1, '发送', 0, 101, 101, '开始', 102, '申请', 'admin', '若依', 'admin', '若依', '2020-02-13 16:04:19', 0, '', '', 'null@SendNode=101', '(admin,若依)', '{\"MyNum\":\"1\",\"CDT\":\"2020-02-13 16:04\",\"Rec\":\"admin\",\"RDT\":\"2020-02-13 16:04\",\"Title\":\"研发部门-admin,若依在2020-02-13 16:04发起.\",\"Emps\":\"admin\",\"FK_Dept\":\"103\",\"FK_DeptText\":\"研发部门\",\"OID\":\"101\",\"FID\":\"0\",\"FK_NY\":\"2020-02\",\"OutEnd\":\"1\"}');
INSERT INTO `nd1track` VALUES (1180853948, 1, '发送', 0, 103, 102, '申请', 103, '科长审批', 'admin', '超级管理员', 'zaoyun', '赵云', '2020-02-13 18:50:16', 0, '', '', 'null@SendNode=102', '(admin,超级管理员)', '{\"QJYY\":\"\",\"CDT\":\"2020-02-13 18:49\",\"QJR\":\"超级管理员\",\"KSSJ\":\"2020-02-13\",\"Rec\":\"admin\",\"RDT\":\"2020-02-13 18:49\",\"JSSJ\":\"2020-02-13\",\"Emps\":\"admin\",\"FK_Dept\":\"100\",\"OID\":\"103\",\"FID\":\"0\",\"OutEnd\":\"1\"}');
INSERT INTO `nd1track` VALUES (1525744535, 1, '发送', 0, 102, 101, '开始', 102, '申请', 'admin', '超级管理员', 'admin', '超级管理员', '2020-02-13 18:44:13', 0, '', '', 'null@SendNode=101', '(admin,超级管理员)', '{\"MyNum\":\"1\",\"CDT\":\"2020-02-13 18:44\",\"Rec\":\"admin\",\"RDT\":\"2020-02-13 18:44\",\"Title\":\"组织架构-admin,超级管理员在2020-02-13 18:44发起.\",\"Emps\":\"admin\",\"FK_Dept\":\"100\",\"FK_DeptText\":\"组织架构\",\"OID\":\"102\",\"FID\":\"0\",\"FK_NY\":\"2020-02\",\"OutEnd\":\"1\"}');

-- ----------------------------
-- Table structure for nd2rpt
-- ----------------------------
DROP TABLE IF EXISTS `nd2rpt`;
CREATE TABLE `nd2rpt`  (
  `OID` int(11) NOT NULL,
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FID` int(11) NULL DEFAULT 0 COMMENT 'FID',
  `CDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Rec` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Emps` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FK_NY` varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MyNum` int(11) NULL DEFAULT 1 COMMENT '个数',
  `PNodeID` int(11) NULL DEFAULT 0,
  `GUID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `PEmp` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `PrjNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `BillNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `PrjName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `FlowNote` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `WFSta` int(11) NULL DEFAULT 0,
  `PWorkID` int(11) NULL DEFAULT 0,
  `FlowDaySpan` decimal(20, 4) NULL DEFAULT 0.0000 COMMENT '跨度(天)',
  `FlowEndNode` int(11) NULL DEFAULT 0,
  `FlowEnderRDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FlowStartRDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PFlowNo` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `FlowEmps` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `WFState` int(11) NULL DEFAULT 0,
  `FlowEnder` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `FlowStarter` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nd2rpt
-- ----------------------------

-- ----------------------------
-- Table structure for nd2track
-- ----------------------------
DROP TABLE IF EXISTS `nd2track`;
CREATE TABLE `nd2track`  (
  `MyPK` int(11) NOT NULL,
  `ActionType` int(11) NULL DEFAULT 0 COMMENT '类型',
  `ActionTypeText` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FID` int(11) NULL DEFAULT 0 COMMENT '流程ID',
  `WorkID` int(11) NULL DEFAULT 0 COMMENT '工作ID',
  `NDFrom` int(11) NULL DEFAULT 0 COMMENT '从节点',
  `NDFromT` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NDTo` int(11) NULL DEFAULT 0 COMMENT '到节点',
  `NDToT` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EmpFrom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EmpFromT` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EmpTo` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EmpToT` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `WorkTimeSpan` float NULL DEFAULT NULL COMMENT '时间跨度(天)',
  `Msg` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `NodeData` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `Tag` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Exer` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nd2track
-- ----------------------------

-- ----------------------------
-- Table structure for nd3rpt
-- ----------------------------
DROP TABLE IF EXISTS `nd3rpt`;
CREATE TABLE `nd3rpt`  (
  `OID` int(11) NOT NULL,
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FID` int(11) NULL DEFAULT 0 COMMENT 'FID',
  `CDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Rec` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Emps` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FK_NY` varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MyNum` int(11) NULL DEFAULT 1 COMMENT '个数',
  `PNodeID` int(11) NULL DEFAULT 0,
  `GUID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `PEmp` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `PrjNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `BillNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `PrjName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `FlowNote` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `WFSta` int(11) NULL DEFAULT 0,
  `PWorkID` int(11) NULL DEFAULT 0,
  `FlowDaySpan` decimal(20, 4) NULL DEFAULT 0.0000 COMMENT '跨度(天)',
  `FlowEndNode` int(11) NULL DEFAULT 0,
  `FlowEnderRDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FlowStartRDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PFlowNo` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `FlowEmps` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `WFState` int(11) NULL DEFAULT 0,
  `FlowEnder` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `FlowStarter` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nd3rpt
-- ----------------------------

-- ----------------------------
-- Table structure for nd3track
-- ----------------------------
DROP TABLE IF EXISTS `nd3track`;
CREATE TABLE `nd3track`  (
  `MyPK` int(11) NOT NULL,
  `ActionType` int(11) NULL DEFAULT 0 COMMENT '类型',
  `ActionTypeText` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FID` int(11) NULL DEFAULT 0 COMMENT '流程ID',
  `WorkID` int(11) NULL DEFAULT 0 COMMENT '工作ID',
  `NDFrom` int(11) NULL DEFAULT 0 COMMENT '从节点',
  `NDFromT` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NDTo` int(11) NULL DEFAULT 0 COMMENT '到节点',
  `NDToT` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EmpFrom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EmpFromT` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EmpTo` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EmpToT` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `WorkTimeSpan` float NULL DEFAULT NULL COMMENT '时间跨度(天)',
  `Msg` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `NodeData` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `Tag` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Exer` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nd3track
-- ----------------------------

-- ----------------------------
-- Table structure for pub_nd
-- ----------------------------
DROP TABLE IF EXISTS `pub_nd`;
CREATE TABLE `pub_nd`  (
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '年度' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pub_nd
-- ----------------------------

-- ----------------------------
-- Table structure for pub_ny
-- ----------------------------
DROP TABLE IF EXISTS `pub_ny`;
CREATE TABLE `pub_ny`  (
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '年月' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pub_ny
-- ----------------------------

-- ----------------------------
-- Table structure for pub_yf
-- ----------------------------
DROP TABLE IF EXISTS `pub_yf`;
CREATE TABLE `pub_yf`  (
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '月份' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pub_yf
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 201 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '组织架构', 0, 'admin', '18888888888', '888888@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-02-13 16:07:22');
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '绵阳投资集团', 1, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-02-13 16:07:22');
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (200, 100, '0,100', '绵阳卷烟厂', 2, '无', '13600000000', NULL, '0', '0', 'admin', '2020-02-13 16:56:07', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录状态列表');

-- ----------------------------
-- Table structure for sys_docfile
-- ----------------------------
DROP TABLE IF EXISTS `sys_docfile`;
CREATE TABLE `sys_docfile`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FileName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `FileSize` int(11) NULL DEFAULT NULL COMMENT '大小',
  `FileType` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件类型',
  `D1` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'D1',
  `D2` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'D2',
  `D3` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'D3',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '备注字段文件管理者' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_docfile
-- ----------------------------

-- ----------------------------
-- Table structure for sys_encfg
-- ----------------------------
DROP TABLE IF EXISTS `sys_encfg`;
CREATE TABLE `sys_encfg`  (
  `No` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '实体名称 - 主键',
  `GroupTitle` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组标签',
  `Url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '要打开的Url',
  `FJSavePath` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存路径',
  `FJWebPath` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件Web路径',
  `Datan` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段数据分析方式',
  `UI` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'UI设置',
  `AtPara` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '实体配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_encfg
-- ----------------------------
INSERT INTO `sys_encfg` VALUES ('BP.Frm.FrmBill', '@No=基础信息,单据基础配置信息.@BtnNewLable=单据按钮权限,用于控制每个功能按钮启用规则.@BtnImpExcel=列表按钮,列表按钮控制@Designer=设计者,流程开发设计者信息', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.Frm.FrmDict', '@No=基础信息,单据基础配置信息.@BtnNewLable=单据按钮权限,用于控制每个功能按钮启用规则.@BtnImpExcel=列表按钮,列表按钮控制@Designer=设计者,流程开发设计者信息', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.Sys.FrmUI.FrmAttachmentExt', '@MyPK=基础信息,附件的基本配置.\n@DeleteWay=权限控制,控制附件的下载与上传权限.@IsRowLock=WebOffice属性,设置与公文有关系的属性配置.\n@IsToHeLiuHZ=流程相关,控制节点附件的分合流.', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Port.AdminEmp', '', '', '', '', '', '', '');
INSERT INTO `sys_encfg` VALUES ('BP.WF.Port.AdminEmps', '', '', '', '', '', '', '@UIRowStyleGlo=3@IsEnableDouclickGlo=1@IsEnableFocusField=0@FocusField=无@IsEnableRefFunc=1@IsEnableOpenICON=1@MoveToShowWay=0@MoveTo=无@WinCardW=1000@WinCardH=600@PageSize=10@FontSize=14@EditerType=0@SearchUrlOpenType=0@OrderBy=@IsDeSc=1@IsEnableLazyload=0@KeyLabel=@BtnLab1=@BtnJS1=@BtnLab2=@BtnJS2=');
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.FlowExt', '@No=基础信息,基础信息权限信息.@IsBatchStart=数据&表单,数据导入导出.@DesignerNo=设计者,流程开发设计者信息', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.FlowSheet', '@No=基本配置@FlowRunWay=启动方式,配置工作流程如何自动发起，该选项要与流程服务一起工作才有效.@StartLimitRole=启动限制规则@StartGuideWay=发起前置导航@CFlowWay=延续流程@DTSWay=流程数据与业务数据同步@PStarter=轨迹查看权限', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.FrmNodeComponent', '@NodeID=审核组件,适用于sdk表单审核组件与ccform上的审核组件属性设置.@SFLab=父子流程组件,在该节点上配置与显示父子流程.@FrmThreadLab=子线程组件,对合流节点有效，用于配置与现实子线程运行的情况。@FrmTrackLab=轨迹组件,用于显示流程运行的轨迹图.@FTCLab=流转自定义,在每个节点上自己控制节点的处理人.', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.MapDataExt', '@No=基本属性@Designer=设计者信息', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.MapDtlExt', '@No=基础信息,基础信息权限信息.@IsExp=数据导入导出,数据导入导出.@MTR=多表头,实现多表头.@IsEnableLink=超链接,显示在从表的右边.@IsCopyNDData=流程相关,与流程相关的配置非流程可以忽略.', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.MapFrmFool', '@No=基础属性,基础属性.@Designer=设计者信息,设计者的单位信息，人员信息，可以上传到表单云.', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.NodeExt', '@NodeID=基本配置@SendLab=按钮权限,控制工作节点可操作按钮.@RunModel=运行模式,分合流,父子流程@AutoJumpRole0=跳转,自动跳转规则当遇到该节点时如何让其自动的执行下一步.', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.NodeSheet', '@NodeID=基本配置@FormType=表单@FWCSta=审核组件,适用于sdk表单审核组件与ccform上的审核组件属性设置.@SFSta=父子流程,对启动，查看父子流程的控件设置.@SendLab=按钮权限,控制工作节点可操作按钮.@RunModel=运行模式,分合流,父子流程@AutoJumpRole0=跳转,自动跳转规则当遇到该节点时如何让其自动的执行下一步.@MPhone_WorkModel=移动,与手机平板电脑相关的应用设置.@OfficeOpen=公文按钮,只有当该节点是公文流程时候有效', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_enum
-- ----------------------------
DROP TABLE IF EXISTS `sys_enum`;
CREATE TABLE `sys_enum`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Lab` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Lab',
  `EnumKey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'EnumKey',
  `IntKey` int(11) NULL DEFAULT NULL COMMENT 'Val',
  `Lang` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '语言',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '枚举数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_enum
-- ----------------------------
INSERT INTO `sys_enum` VALUES ('ActionType_CH_0', 'GET', 'ActionType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ActionType_CH_1', 'POST', 'ActionType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertType_CH_0', '短信', 'AlertType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertType_CH_1', '邮件', 'AlertType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertType_CH_2', '邮件与短信', 'AlertType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertType_CH_3', '系统(内部)消息', 'AlertType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_0', '不接收', 'AlertWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_1', '短信', 'AlertWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_2', '邮件', 'AlertWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_3', '内部消息', 'AlertWay', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_4', 'QQ消息', 'AlertWay', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_5', 'RTX消息', 'AlertWay', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_6', 'MSN消息', 'AlertWay', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('AppModel_CH_0', 'BS系统', 'AppModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('AppModel_CH_1', 'CS系统', 'AppModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AppType_CH_0', '外部Url连接', 'AppType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('AppType_CH_1', '本地可执行文件', 'AppType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AthSaveWay_CH_0', '保存到web服务器', 'AthSaveWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('AthSaveWay_CH_1', '保存到数据库', 'AthSaveWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AthSaveWay_CH_2', 'ftp服务器', 'AthSaveWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('AthUploadWay_CH_0', '继承模式', 'AthUploadWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('AthUploadWay_CH_1', '协作模式', 'AthUploadWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AuthorWay_CH_0', '不授权', 'AuthorWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('AuthorWay_CH_1', '全部流程授权', 'AuthorWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AuthorWay_CH_2', '指定流程授权', 'AuthorWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('BillFileType_CH_0', 'Word', 'BillFileType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('BillFileType_CH_1', 'PDF', 'BillFileType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('BillFileType_CH_2', 'Excel(未完成)', 'BillFileType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('BillFileType_CH_3', 'Html(未完成)', 'BillFileType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('BillOpenModel_CH_0', '下载本地', 'BillOpenModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('BillOpenModel_CH_1', '在线WebOffice打开', 'BillOpenModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('BillState_CH_0', '空白', 'BillState', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('BillState_CH_1', '草稿', 'BillState', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('BillState_CH_100', '归档', 'BillState', 100, 'CH');
INSERT INTO `sys_enum` VALUES ('BillState_CH_2', '编辑中', 'BillState', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('BillSta_CH_0', '运行中', 'BillSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('BillSta_CH_1', '已完成', 'BillSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('BillSta_CH_2', '其他', 'BillSta', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('BtnNewModel_CH_0', '表格模式', 'BtnNewModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('BtnNewModel_CH_1', '卡片模式', 'BtnNewModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('BtnNewModel_CH_2', '不可用', 'BtnNewModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_0', '上一步可以撤销', 'CancelRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_1', '不能撤销', 'CancelRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_2', '上一步与开始节点可以撤销', 'CancelRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_3', '指定的节点可以撤销', 'CancelRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_0', '不能抄送', 'CCRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_1', '手工抄送', 'CCRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_2', '自动抄送', 'CCRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_3', '手工与自动', 'CCRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_4', '按表单SysCCEmps字段计算', 'CCRole', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_5', '在发送前打开抄送窗口', 'CCRole', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('CCWriteTo_CH_0', '写入抄送列表', 'CCWriteTo', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CCWriteTo_CH_1', '写入待办', 'CCWriteTo', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CCWriteTo_CH_2', '写入待办与抄送列表', 'CCWriteTo', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertRole_CH_0', '不提示', 'CHAlertRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertRole_CH_1', '每天1次', 'CHAlertRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertRole_CH_2', '每天2次', 'CHAlertRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertWay_CH_0', '邮件', 'CHAlertWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertWay_CH_1', '短信', 'CHAlertWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertWay_CH_2', 'CCIM即时通讯', 'CHAlertWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ChartType_CH_0', '几何图形', 'ChartType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ChartType_CH_1', '肖像图片', 'ChartType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CHRole_CH_0', '禁用', 'CHRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CHRole_CH_1', '启用', 'CHRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CHRole_CH_2', '只读', 'CHRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CHRole_CH_3', '启用并可以调整流程应完成时间', 'CHRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_0', '及时完成', 'CHSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_1', '按期完成', 'CHSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_2', '逾期完成', 'CHSta', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_3', '超期完成', 'CHSta', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('CodeStruct_CH_0', '普通的编码表(具有No,Name)', 'CodeStruct', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CodeStruct_CH_1', '树结构(具有No,Name,ParentNo)', 'CodeStruct', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CodeStruct_CH_2', '行政机构编码表(编码以两位编号标识级次树形关系)', 'CodeStruct', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpan_CH_0', '跨0个单元格', 'ColSpan', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpan_CH_1', '跨1个单元格', 'ColSpan', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpan_CH_2', '跨2个单元格', 'ColSpan', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpan_CH_3', '跨3个单元格', 'ColSpan', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpan_CH_4', '跨4个单元格', 'ColSpan', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('CondModel_CH_0', '由连接线条件控制', 'CondModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CondModel_CH_1', '按照用户选择计算', 'CondModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CondModel_CH_2', '发送按钮旁下拉框选择', 'CondModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ConfirmKind_CH_0', '当前单元格', 'ConfirmKind', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ConfirmKind_CH_1', '左方单元格', 'ConfirmKind', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ConfirmKind_CH_2', '上方单元格', 'ConfirmKind', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ConfirmKind_CH_3', '右方单元格', 'ConfirmKind', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ConfirmKind_CH_4', '下方单元格', 'ConfirmKind', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('ConnJudgeWay_CH_0', 'or', 'ConnJudgeWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ConnJudgeWay_CH_1', 'and', 'ConnJudgeWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWay_CH_0', '按岗位', 'CtrlWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWay_CH_1', '按部门', 'CtrlWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWay_CH_2', '按人员', 'CtrlWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWay_CH_3', '按SQL', 'CtrlWay', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('DataStoreModel_CH_0', '数据轨迹模式', 'DataStoreModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DataStoreModel_CH_1', '数据合并模式', 'DataStoreModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DataType_CH_0', '字符串', 'DataType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DataType_CH_1', '整数', 'DataType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DataType_CH_2', '浮点数', 'DataType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('DataType_CH_3', '日期', 'DataType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('DataType_CH_4', '日期时间', 'DataType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('DataType_CH_5', '外键', 'DataType', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('DataType_CH_6', '枚举', 'DataType', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('DefValType_CH_0', '默认值为空', 'DefValType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DefValType_CH_1', '按照设置的默认值设置', 'DefValType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_0', '不能删除', 'DelEnable', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_1', '逻辑删除', 'DelEnable', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_2', '记录日志方式删除', 'DelEnable', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_3', '彻底删除', 'DelEnable', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_4', '让用户决定删除方式', 'DelEnable', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('DeleteWay_CH_0', '不能删除', 'DeleteWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DeleteWay_CH_1', '删除所有', 'DeleteWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DeleteWay_CH_2', '只能删除自己上传的', 'DeleteWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('DocType_CH_0', '正式公文', 'DocType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DocType_CH_1', '便函', 'DocType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('Draft_CH_0', '无(不设草稿)', 'Draft', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('Draft_CH_1', '保存到待办', 'Draft', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('Draft_CH_2', '保存到草稿箱', 'Draft', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlAddRecModel_CH_0', '按设置的数量初始化空白行', 'DtlAddRecModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlAddRecModel_CH_1', '用按钮增加空白行', 'DtlAddRecModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlOpenType_CH_0', '操作员', 'DtlOpenType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlOpenType_CH_1', '工作ID', 'DtlOpenType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlOpenType_CH_2', '流程ID', 'DtlOpenType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlSaveModel_CH_0', '自动存盘(失去焦点自动存盘)', 'DtlSaveModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlSaveModel_CH_1', '手动存盘(保存按钮触发存盘)', 'DtlSaveModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlVer_CH_0', '2017传统版', 'DtlVer', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlVer_CH_1', '2019EasyUI版本', 'DtlVer', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSearchWay_CH_0', '不启用', 'DTSearchWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSearchWay_CH_1', '按日期', 'DTSearchWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSearchWay_CH_2', '按日期时间', 'DTSearchWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSField_CH_0', '字段名相同', 'DTSField', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSField_CH_1', '按设置的字段匹配', 'DTSField', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSTime_CH_0', '所有的节点发送后', 'DTSTime', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSTime_CH_1', '指定的节点发送后', 'DTSTime', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSTime_CH_2', '当流程结束时', 'DTSTime', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSWay_CH_0', '不考核', 'DTSWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSWay_CH_1', '按照时效考核', 'DTSWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSWay_CH_2', '按照工作量考核', 'DTSWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('EditerType_CH_0', '无编辑器', 'EditerType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('EditerType_CH_1', 'Sina编辑器0', 'EditerType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('EditerType_CH_2', 'FKEditer', 'EditerType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('EditerType_CH_3', 'KindEditor', 'EditerType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('EditerType_CH_4', '百度的UEditor', 'EditerType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityEditModel_CH_0', '表格', 'EntityEditModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityEditModel_CH_1', '行编辑', 'EntityEditModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityShowModel_CH_0', '表格', 'EntityShowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityShowModel_CH_1', '树干模式', 'EntityShowModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityType_CH_0', '独立表单', 'EntityType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityType_CH_1', '单据', 'EntityType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityType_CH_2', '编号名称实体', 'EntityType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityType_CH_3', '树结构实体', 'EntityType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('EventType_CH_0', '禁用', 'EventType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('EventType_CH_1', '执行URL', 'EventType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('EventType_CH_2', '执行CCFromRef.js', 'EventType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ExcelType_CH_0', '普通文件数据提取', 'ExcelType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ExcelType_CH_1', '流程附件数据提取', 'ExcelType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ExcType_CH_0', '超链接', 'ExcType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ExcType_CH_1', '函数', 'ExcType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ExpType_CH_3', '按照SQL计算', 'ExpType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ExpType_CH_4', '按照参数计算', 'ExpType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('FileType_CH_0', '普通附件', 'FileType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FileType_CH_1', '图片文件', 'FileType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_0', '关闭附件', 'FJOpen', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_1', '操作员', 'FJOpen', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_2', '工作ID', 'FJOpen', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_3', '流程ID', 'FJOpen', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowAppType_CH_0', '业务流程', 'FlowAppType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowAppType_CH_1', '工程类(项目组流程)', 'FlowAppType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowAppType_CH_2', '公文流程(VSTO)', 'FlowAppType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_0', '超级管理员可以删除', 'FlowDeleteRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_1', '分级管理员可以删除', 'FlowDeleteRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_2', '发起人可以删除', 'FlowDeleteRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_3', '节点启动删除按钮的操作员', 'FlowDeleteRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_0', '手工启动', 'FlowRunWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_1', '指定人员按时启动', 'FlowRunWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_2', '数据集按时启动', 'FlowRunWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_3', '触发式启动', 'FlowRunWay', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('FLRole_CH_0', '按接受人', 'FLRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FLRole_CH_1', '按部门', 'FLRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FLRole_CH_2', '按岗位', 'FLRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmShowType_CH_0', '普通方式', 'FrmShowType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmShowType_CH_1', '页签方式', 'FrmShowType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmSln_CH_0', '默认方案', 'FrmSln', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmSln_CH_1', '只读方案', 'FrmSln', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmSln_CH_2', '自定义方案', 'FrmSln', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmThreadSta_CH_0', '禁用', 'FrmThreadSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmThreadSta_CH_1', '启用', 'FrmThreadSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmTrackSta_CH_0', '禁用', 'FrmTrackSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmTrackSta_CH_1', '显示轨迹图', 'FrmTrackSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmTrackSta_CH_2', '显示轨迹表', 'FrmTrackSta', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_0', '傻瓜表单', 'FrmType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_1', '自由表单', 'FrmType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_11', '累加表单', 'FrmType', 11, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_3', '嵌入式表单', 'FrmType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_4', 'Word表单', 'FrmType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_5', '在线编辑模式Excel表单', 'FrmType', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_6', 'VSTO模式Excel表单', 'FrmType', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_7', '实体类组件', 'FrmType', 7, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_0', '不显示', 'FrmUrlShowWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_1', '自动大小', 'FrmUrlShowWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_2', '指定大小', 'FrmUrlShowWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_3', '新窗口', 'FrmUrlShowWay', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('FTCSta_CH_0', '禁用', 'FTCSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FTCSta_CH_1', '只读', 'FTCSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FTCSta_CH_2', '可设置人员', 'FTCSta', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FTCWorkModel_CH_0', '简洁模式', 'FTCWorkModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FTCWorkModel_CH_1', '高级模式', 'FTCWorkModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_0', '不启用', 'FWCAth', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_1', '多附件', 'FWCAth', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_2', '单附件(暂不支持)', 'FWCAth', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_3', '图片附件(暂不支持)', 'FWCAth', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCMsgShow_CH_0', '都显示', 'FWCMsgShow', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCMsgShow_CH_1', '仅显示自己的意见', 'FWCMsgShow', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCOrderModel_CH_0', '按审批时间先后排序', 'FWCOrderModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCOrderModel_CH_1', '按照接受人员列表先后顺序(官职大小)', 'FWCOrderModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCShowModel_CH_0', '表格方式', 'FWCShowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCShowModel_CH_1', '自由模式', 'FWCShowModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCSta_CH_0', '禁用', 'FWCSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCSta_CH_1', '启用', 'FWCSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCSta_CH_2', '只读', 'FWCSta', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCType_CH_0', '审核组件', 'FWCType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCType_CH_1', '日志组件', 'FWCType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCType_CH_2', '周报组件', 'FWCType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCType_CH_3', '月报组件', 'FWCType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCVer_CH_0', '2018', 'FWCVer', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCVer_CH_1', '2019', 'FWCVer', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('HelpRole_CH_0', '禁用', 'HelpRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('HelpRole_CH_1', '启用', 'HelpRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('HelpRole_CH_2', '强制提示', 'HelpRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('HelpRole_CH_3', '选择性提示', 'HelpRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('HuiQianLeaderRole_CH_0', '只有一个组长', 'HuiQianLeaderRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('HuiQianLeaderRole_CH_1', '最后一个组长发送', 'HuiQianLeaderRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('HuiQianLeaderRole_CH_2', '任意组长可以发送', 'HuiQianLeaderRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('HuiQianRole_CH_0', '不启用', 'HuiQianRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('HuiQianRole_CH_1', '协作(同事)模式', 'HuiQianRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('HuiQianRole_CH_4', '组长(领导)模式', 'HuiQianRole', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('HungUpWay_CH_0', '无限挂起', 'HungUpWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('HungUpWay_CH_1', '按指定的时间解除挂起并通知我自己', 'HungUpWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('HungUpWay_CH_2', '按指定的时间解除挂起并通知所有人', 'HungUpWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ImpModel_CH_0', '不导入', 'ImpModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ImpModel_CH_1', '按配置模式导入', 'ImpModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ImpModel_CH_2', '按照xls文件模版导入', 'ImpModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('InvokeTime_CH_0', '发送时', 'InvokeTime', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('InvokeTime_CH_1', '工作到达时', 'InvokeTime', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSLSubFlowOver_CH_0', '不处理', 'IsAutoSendSLSubFlowOver', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSLSubFlowOver_CH_1', '让同级子流程自动运行下一步', 'IsAutoSendSLSubFlowOver', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSLSubFlowOver_CH_2', '结束同级子流程', 'IsAutoSendSLSubFlowOver', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSubFlowOver_CH_0', '不处理', 'IsAutoSendSubFlowOver', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSubFlowOver_CH_1', '让父流程自动运行下一步', 'IsAutoSendSubFlowOver', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSubFlowOver_CH_2', '结束父流程', 'IsAutoSendSubFlowOver', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_0', '无', 'IsSigan', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_1', '图片签名', 'IsSigan', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_2', '山东CA', 'IsSigan', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_3', '广东CA', 'IsSigan', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_4', '图片盖章', 'IsSigan', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_0', 'yyyy-MM-dd', 'IsSupperText', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_1', 'yyyy-MM-dd HH:mm', 'IsSupperText', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_2', 'yyyy-MM-dd HH:mm:ss', 'IsSupperText', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_3', 'yyyy-MM', 'IsSupperText', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_4', 'HH:mm', 'IsSupperText', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_5', 'HH:mm:ss', 'IsSupperText', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_6', 'MM-dd', 'IsSupperText', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_0', '一般', 'JMCD', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_1', '保密', 'JMCD', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_2', '秘密', 'JMCD', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_3', '机密', 'JMCD', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_0', '不能跳转', 'JumpWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_1', '只能向后跳转', 'JumpWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_2', '只能向前跳转', 'JumpWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_3', '任意节点跳转', 'JumpWay', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_4', '按指定规则跳转', 'JumpWay', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('LGType_CH_0', '普通', 'LGType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('LGType_CH_1', '枚举', 'LGType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('LGType_CH_2', '外键', 'LGType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('LGType_CH_3', '打开系统页面', 'LGType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ListShowModel_CH_0', '表格', 'ListShowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ListShowModel_CH_1', '卡片', 'ListShowModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuCtrlWay_CH_0', '按照设置的控制', 'MenuCtrlWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuCtrlWay_CH_1', '任何人都可以使用', 'MenuCtrlWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuCtrlWay_CH_2', 'Admin用户可以使用', 'MenuCtrlWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_0', '系统根目录', 'MenuType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_1', '系统类别', 'MenuType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_2', '系统', 'MenuType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_3', '目录', 'MenuType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_4', '功能/界面', 'MenuType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_5', '功能控制点', 'MenuType', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('MethodDocTypeOfFunc_CH_0', 'SQL', 'MethodDocTypeOfFunc', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('MethodDocTypeOfFunc_CH_1', 'URL', 'MethodDocTypeOfFunc', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MethodDocTypeOfFunc_CH_2', 'JavaScript', 'MethodDocTypeOfFunc', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('MethodDocTypeOfFunc_CH_3', '业务单元', 'MethodDocTypeOfFunc', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('MobileShowModel_CH_0', '新页面显示模式', 'MobileShowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('MobileShowModel_CH_1', '列表模式', 'MobileShowModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('Model_CH_0', '普通', 'Model', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('Model_CH_1', '固定行', 'Model', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MoveToShowWay_CH_0', '不显示', 'MoveToShowWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('MoveToShowWay_CH_1', '下拉列表0', 'MoveToShowWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MoveToShowWay_CH_2', '平铺', 'MoveToShowWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_0', '不发送', 'MsgCtrl', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_1', '按设置的下一步接受人自动发送（默认）', 'MsgCtrl', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_2', '由本节点表单系统字段(IsSendEmail,IsSendSMS)来决定', 'MsgCtrl', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_3', '由SDK开发者参数(IsSendEmail,IsSendSMS)来决定', 'MsgCtrl', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_1', '字符串String', 'MyDataType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_2', '整数类型Int', 'MyDataType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_3', '浮点类型AppFloat', 'MyDataType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_4', '判断类型Boolean', 'MyDataType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_5', '双精度类型Double', 'MyDataType', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_6', '日期型Date', 'MyDataType', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_7', '时间类型Datetime', 'MyDataType', 7, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_8', '金额类型AppMoney', 'MyDataType', 8, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDeptRole_CH_0', '仅部门领导可以查看', 'MyDeptRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDeptRole_CH_1', '部门下所有的人都可以查看', 'MyDeptRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDeptRole_CH_2', '本部门里指定岗位的人可以查看', 'MyDeptRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('NoteEnable_CH_0', '禁用', 'NoteEnable', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('NoteEnable_CH_1', '启用', 'NoteEnable', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('NoteEnable_CH_2', '只读', 'NoteEnable', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('OpenWay_CH_0', '新窗口', 'OpenWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('OpenWay_CH_1', '本窗口', 'OpenWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('OpenWay_CH_2', '覆盖新窗口', 'OpenWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('PopValFormat_CH_0', 'No(仅编号)', 'PopValFormat', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('PopValFormat_CH_1', 'Name(仅名称)', 'PopValFormat', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('PopValFormat_CH_2', 'No,Name(编号与名称,比如zhangsan,张三;lisi,李四;)', 'PopValFormat', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('PowerCtrlType_CH_0', '岗位', 'PowerCtrlType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('PowerCtrlType_CH_1', '人员', 'PowerCtrlType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('PrintPDFModle_CH_0', '全部打印', 'PrintPDFModle', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('PrintPDFModle_CH_1', '单个表单打印(针对树形表单)', 'PrintPDFModle', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('PRI_CH_0', '低', 'PRI', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('PRI_CH_1', '中', 'PRI', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('PRI_CH_2', '高', 'PRI', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_0', '按照指定节点的工作人员', 'PushWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_1', '按照指定的工作人员', 'PushWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_2', '按照指定的工作岗位', 'PushWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_3', '按照指定的部门', 'PushWay', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_4', '按照指定的SQL', 'PushWay', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_5', '按照系统指定的字段', 'PushWay', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('QRModel_CH_0', '不生成', 'QRModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('QRModel_CH_1', '生成二维码', 'QRModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('RBShowModel_CH_0', '竖向', 'RBShowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('RBShowModel_CH_3', '横向', 'RBShowModel', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_0', '不回执', 'ReadReceipts', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_1', '自动回执', 'ReadReceipts', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_2', '由上一节点表单字段决定', 'ReadReceipts', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_3', '由SDK开发者参数决定', 'ReadReceipts', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ReadRole_CH_0', '不控制', 'ReadRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ReadRole_CH_1', '未阅读阻止发送', 'ReadRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ReadRole_CH_2', '未阅读做记录', 'ReadRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('RefBillRole_CH_0', '不启用', 'RefBillRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('RefBillRole_CH_1', '非必须选择关联单据', 'RefBillRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('RefBillRole_CH_2', '必须选择关联单据', 'RefBillRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('RefMethodType_CH_1', '模态窗口打开', 'RefMethodType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('RefMethodType_CH_2', '新窗口打开', 'RefMethodType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('RefMethodType_CH_3', '右侧窗口打开', 'RefMethodType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnOneNodeRole_CH_0', '不启用', 'ReturnOneNodeRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnOneNodeRole_CH_1', '按照[退回信息填写字段]作为退回意见直接退回', 'ReturnOneNodeRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnOneNodeRole_CH_2', '按照[审核组件]填写的信息作为退回意见直接退回', 'ReturnOneNodeRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_0', '不能退回', 'ReturnRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_1', '只能退回上一个节点', 'ReturnRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_2', '可退回以前任意节点', 'ReturnRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_3', '可退回指定的节点', 'ReturnRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_4', '由流程图设计的退回路线决定', 'ReturnRole', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnSendModel_CH_0', '从退回节点正常执行', 'ReturnSendModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnSendModel_CH_1', '直接发送到当前节点', 'ReturnSendModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnSendModel_CH_2', '直接发送到当前节点的下一个节点', 'ReturnSendModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('RowOpenModel_CH_0', '新窗口打开', 'RowOpenModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('RowOpenModel_CH_1', '在本窗口打开', 'RowOpenModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('RowOpenModel_CH_2', '弹出窗口打开,关闭后不刷新列表', 'RowOpenModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('RowOpenModel_CH_3', '弹出窗口打开,关闭后刷新列表', 'RowOpenModel', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('RunModel_CH_0', '普通', 'RunModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('RunModel_CH_1', '合流', 'RunModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('RunModel_CH_2', '分流', 'RunModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('RunModel_CH_3', '分合流', 'RunModel', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('RunModel_CH_4', '子线程', 'RunModel', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('SaveModel_CH_0', '仅节点表', 'SaveModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SaveModel_CH_1', '节点表与Rpt表', 'SaveModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SearchUrlOpenType_CH_0', 'En.htm', 'SearchUrlOpenType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SearchUrlOpenType_CH_1', 'EnOnly.htm', 'SearchUrlOpenType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SearchUrlOpenType_CH_2', '自定义url', 'SearchUrlOpenType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectAccepterEnable_CH_0', '不启用', 'SelectAccepterEnable', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectAccepterEnable_CH_1', '单独启用', 'SelectAccepterEnable', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectAccepterEnable_CH_2', '在发送前打开', 'SelectAccepterEnable', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectAccepterEnable_CH_3', '转入新页面', 'SelectAccepterEnable', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_0', '按岗位', 'SelectorModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_1', '按部门', 'SelectorModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_2', '按人员', 'SelectorModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_3', '按SQL', 'SelectorModel', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_4', '按SQL模版计算', 'SelectorModel', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_5', '使用通用人员选择器', 'SelectorModel', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_6', '部门与岗位的交集', 'SelectorModel', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_7', '自定义Url', 'SelectorModel', 7, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_8', '使用通用部门岗位人员选择器', 'SelectorModel', 8, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_9', '按岗位智能计算(操作员所在部门)', 'SelectorModel', 9, 'CH');
INSERT INTO `sys_enum` VALUES ('SendModel_CH_0', '给当前人员设置开始节点待办', 'SendModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SendModel_CH_1', '发送到下一个节点', 'SendModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SFOpenType_CH_0', '工作查看器', 'SFOpenType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SFOpenType_CH_1', '傻瓜表单轨迹查看器', 'SFOpenType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SFShowCtrl_CH_0', '可以看所有的子流程', 'SFShowCtrl', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SFShowCtrl_CH_1', '仅仅可以看自己发起的子流程', 'SFShowCtrl', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SFShowModel_CH_0', '表格方式', 'SFShowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SFShowModel_CH_1', '自由模式', 'SFShowModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SFSta_CH_0', '禁用', 'SFSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SFSta_CH_1', '启用', 'SFSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SFSta_CH_2', '只读', 'SFSta', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SharingType_CH_0', '共享', 'SharingType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SharingType_CH_1', '私有', 'SharingType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ShowModel_CH_0', '按钮', 'ShowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ShowModel_CH_1', '超链接', 'ShowModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ShowWhere_CH_0', '树形表单', 'ShowWhere', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ShowWhere_CH_1', '工具栏', 'ShowWhere', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SignType_CH_0', '不签名', 'SignType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SignType_CH_1', '图片签名', 'SignType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SignType_CH_2', '电子签名', 'SignType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_0', '方向条件', 'SQLType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_1', '接受人规则', 'SQLType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_2', '下拉框数据过滤', 'SQLType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_3', '级联下拉框', 'SQLType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_4', 'PopVal开窗返回值', 'SQLType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_5', '人员选择器人员选择范围', 'SQLType', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_0', '本地的类', 'SrcType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_1', '创建表', 'SrcType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_2', '表或视图', 'SrcType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_3', 'SQL查询表', 'SrcType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_4', 'WebServices', 'SrcType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_5', '微服务Handler外部数据源', 'SrcType', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_6', 'JavaScript外部数据源', 'SrcType', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_7', '动态Json', 'SrcType', 7, 'CH');
INSERT INTO `sys_enum` VALUES ('SSOType_CH_0', 'SID验证', 'SSOType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SSOType_CH_1', '连接', 'SSOType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SSOType_CH_2', '表单提交', 'SSOType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SSOType_CH_3', '不传值', 'SSOType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowModel_CH_0', '下级子流程', 'SubFlowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowModel_CH_1', '同级子流程', 'SubFlowModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowStartWay_CH_0', '不启动', 'SubFlowStartWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowStartWay_CH_1', '指定的字段启动', 'SubFlowStartWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowStartWay_CH_2', '按明细表启动', 'SubFlowStartWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowType_CH_0', '手动启动子流程', 'SubFlowType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowType_CH_1', '触发启动子流程', 'SubFlowType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowType_CH_2', '延续子流程', 'SubFlowType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SubThreadType_CH_0', '同表单', 'SubThreadType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SubThreadType_CH_1', '异表单', 'SubThreadType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('TableCol_CH_0', '4列', 'TableCol', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('TableCol_CH_1', '6列', 'TableCol', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('TableCol_CH_2', '上下模式3列', 'TableCol', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('TabType_CH_0', '本地表或视图', 'TabType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('TabType_CH_1', '通过一个SQL确定的一个外部数据源', 'TabType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('TabType_CH_2', '通过WebServices获得的一个数据源', 'TabType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('Target_CH_0', '新窗口', 'Target', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('Target_CH_1', '本窗口', 'Target', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('Target_CH_2', '父窗口', 'Target', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_0', '未开始', 'TaskSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_1', '进行中', 'TaskSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_2', '完成', 'TaskSta', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_3', '推迟', 'TaskSta', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('TemplateFileModel_CH_0', 'rtf模版', 'TemplateFileModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('TemplateFileModel_CH_1', 'vsto模式的word模版', 'TemplateFileModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('TemplateFileModel_CH_2', 'vsto模式的excel模版', 'TemplateFileModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('TextColSpan_CH_1', '跨1个单元格', 'TextColSpan', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('TextColSpan_CH_2', '跨2个单元格', 'TextColSpan', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('TextColSpan_CH_3', '跨3个单元格', 'TextColSpan', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('TextColSpan_CH_4', '跨4个单元格', 'TextColSpan', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('ThreadKillRole_CH_0', '不能删除', 'ThreadKillRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ThreadKillRole_CH_1', '手工删除', 'ThreadKillRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ThreadKillRole_CH_2', '自动删除', 'ThreadKillRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ToobarExcType_CH_0', '超链接', 'ToobarExcType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ToobarExcType_CH_1', '函数', 'ToobarExcType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_0', '本周', 'TSpan', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_1', '上周', 'TSpan', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_2', '两周以前', 'TSpan', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_3', '三周以前', 'TSpan', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_4', '更早', 'TSpan', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('UIContralType_CH_1', '下拉框', 'UIContralType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('UIContralType_CH_3', '单选按钮', 'UIContralType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('UIRowStyleGlo_CH_0', '无风格', 'UIRowStyleGlo', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('UIRowStyleGlo_CH_1', '交替风格', 'UIRowStyleGlo', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('UIRowStyleGlo_CH_2', '鼠标移动', 'UIRowStyleGlo', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('UIRowStyleGlo_CH_3', '交替并鼠标移动', 'UIRowStyleGlo', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadFileCheck_CH_0', '不控制', 'UploadFileCheck', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadFileCheck_CH_1', '上传附件个数不能为0', 'UploadFileCheck', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadFileCheck_CH_2', '每个类别下面的个数不能为0', 'UploadFileCheck', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadFileNumCheck_CH_0', '不用校验', 'UploadFileNumCheck', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadFileNumCheck_CH_1', '不能为空', 'UploadFileNumCheck', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadFileNumCheck_CH_2', '每个类别下不能为空', 'UploadFileNumCheck', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadType_CH_0', '单个', 'UploadType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadType_CH_1', '多个', 'UploadType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadType_CH_2', '指定', 'UploadType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('UrlSrcType_CH_0', '自定义', 'UrlSrcType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('UrlSrcType_CH_1', '地图', 'UrlSrcType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('UrlSrcType_CH_2', '流程轨迹表', 'UrlSrcType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('UrlSrcType_CH_3', '流程轨迹图', 'UrlSrcType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('UserType_CH_0', '普通用户', 'UserType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('UserType_CH_1', '管理员用户', 'UserType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('UseSta_CH_0', '禁用', 'UseSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('UseSta_CH_1', '启用', 'UseSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('WebOfficeEnable_CH_0', '不启用', 'WebOfficeEnable', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('WebOfficeEnable_CH_1', '按钮方式', 'WebOfficeEnable', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('WebOfficeEnable_CH_2', '标签页置后方式', 'WebOfficeEnable', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('WebOfficeEnable_CH_3', '标签页置前方式', 'WebOfficeEnable', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_10', '批处理', 'WFStateApp', 10, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_2', '运行中', 'WFStateApp', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_3', '已完成', 'WFStateApp', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_4', '挂起', 'WFStateApp', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_5', '退回', 'WFStateApp', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_6', '转发', 'WFStateApp', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_7', '删除', 'WFStateApp', 7, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_8', '加签', 'WFStateApp', 8, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_9', '冻结', 'WFStateApp', 9, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_0', '空白', 'WFState', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_1', '草稿', 'WFState', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_10', '批处理', 'WFState', 10, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_11', '加签回复状态', 'WFState', 11, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_2', '运行中', 'WFState', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_3', '已完成', 'WFState', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_4', '挂起', 'WFState', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_5', '退回', 'WFState', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_6', '转发', 'WFState', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_7', '删除', 'WFState', 7, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_8', '加签', 'WFState', 8, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_9', '冻结', 'WFState', 9, 'CH');
INSERT INTO `sys_enum` VALUES ('WFSta_CH_0', '运行中', 'WFSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('WFSta_CH_1', '已完成', 'WFSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('WFSta_CH_2', '其他', 'WFSta', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('WhatAreYouTodo_CH_0', '关闭提示窗口', 'WhatAreYouTodo', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('WhatAreYouTodo_CH_1', '关闭提示窗口并刷新', 'WhatAreYouTodo', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('WhatAreYouTodo_CH_2', '转入到Search.htm页面上去', 'WhatAreYouTodo', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('WhenOverSize_CH_0', '不处理', 'WhenOverSize', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('WhenOverSize_CH_1', '向下顺增行', 'WhenOverSize', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('WhenOverSize_CH_2', '次页显示', 'WhenOverSize', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoExeIt_CH_0', '操作员执行', 'WhoExeIt', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoExeIt_CH_1', '机器执行', 'WhoExeIt', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoExeIt_CH_2', '混合执行', 'WhoExeIt', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_0', 'WorkID是主键', 'WhoIsPK', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_1', 'FID是主键(干流程的WorkID)', 'WhoIsPK', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_2', '父流程ID是主键', 'WhoIsPK', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_3', '延续流程ID是主键', 'WhoIsPK', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_4', '爷爷流程ID是主键', 'WhoIsPK', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_5', '太爷爷流程ID是主键', 'WhoIsPK', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_0', '不能退回', 'YBFlowReturnRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_1', '退回到父流程的开始节点', 'YBFlowReturnRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_2', '退回到父流程的任何节点', 'YBFlowReturnRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_3', '退回父流程的启动节点', 'YBFlowReturnRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_4', '可退回到指定的节点', 'YBFlowReturnRole', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('显示方式_CH_0', '4列', '显示方式', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('显示方式_CH_1', '6列', '显示方式', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('显示方式_CH_2', '上下模式3列', '显示方式', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('表单展示方式_CH_0', '普通方式', '表单展示方式', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('表单展示方式_CH_1', '页签方式', '表单展示方式', 1, 'CH');

-- ----------------------------
-- Table structure for sys_enummain
-- ----------------------------
DROP TABLE IF EXISTS `sys_enummain`;
CREATE TABLE `sys_enummain`  (
  `No` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `CfgVal` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置信息',
  `Lang` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '语言',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '枚举' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_enummain
-- ----------------------------

-- ----------------------------
-- Table structure for sys_enver
-- ----------------------------
DROP TABLE IF EXISTS `sys_enver`;
CREATE TABLE `sys_enver`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体类',
  `Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体名',
  `PKValue` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主键值',
  `EVer` int(11) NULL DEFAULT NULL COMMENT '版本号',
  `Rec` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '实体版本号' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_enver
-- ----------------------------

-- ----------------------------
-- Table structure for sys_enverdtl
-- ----------------------------
DROP TABLE IF EXISTS `sys_enverdtl`;
CREATE TABLE `sys_enverdtl`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `EnName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体名',
  `EnVerPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本主表PK',
  `AttrKey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段',
  `AttrName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名',
  `OldVal` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '旧值',
  `NewVal` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '新值',
  `EnVer` int(11) NULL DEFAULT NULL COMMENT '版本号(日期)',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日期',
  `Rec` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '实体修改明细' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_enverdtl
-- ----------------------------

-- ----------------------------
-- Table structure for sys_excelfield
-- ----------------------------
DROP TABLE IF EXISTS `sys_excelfield`;
CREATE TABLE `sys_excelfield`  (
  `No` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `CellName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单元格名称',
  `CellRow` int(11) NULL DEFAULT NULL COMMENT '行号',
  `CellColumn` int(11) NULL DEFAULT NULL COMMENT '列号',
  `FK_ExcelSheet` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属ExcelSheet表,外键:对应物理表:Sys_ExcelSheet,表描述:ExcelSheet',
  `Field` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '存储字段名',
  `FK_ExcelTable` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '存储数据表,外键:对应物理表:Sys_ExcelTable,表描述:Excel数据表',
  `DataType` int(11) NULL DEFAULT NULL COMMENT '值类型,枚举类型:0 字符串;1 整数;2 浮点数;3 日期;4 日期时间;5 外键;6 枚举;',
  `UIBindKey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外键表/枚举',
  `UIRefKey` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外键表No',
  `UIRefKeyText` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外键表Name',
  `Validators` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '校验器',
  `FK_ExcelFile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属Excel模板,外键:对应物理表:Sys_ExcelFile,表描述:Excel模板',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '参数',
  `ConfirmKind` int(11) NULL DEFAULT NULL COMMENT '单元格确认方式,枚举类型:0 当前单元格;1 左方单元格;2 上方单元格;3 右方单元格;4 下方单元格;',
  `ConfirmCellCount` int(11) NULL DEFAULT NULL COMMENT '单元格确认方向移动量',
  `ConfirmCellValue` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应单元格值',
  `ConfirmRepeatIndex` int(11) NULL DEFAULT NULL COMMENT '对应单元格值重复选定次序',
  `SkipIsNull` int(11) NULL DEFAULT NULL COMMENT '不计非空',
  `SyncToField` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '同步到字段',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Excel字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_excelfield
-- ----------------------------

-- ----------------------------
-- Table structure for sys_excelfile
-- ----------------------------
DROP TABLE IF EXISTS `sys_excelfile`;
CREATE TABLE `sys_excelfile`  (
  `No` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Mark` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标识',
  `ExcelType` int(11) NULL DEFAULT NULL COMMENT '类型,枚举类型:0 普通文件数据提取;1 流程附件数据提取;',
  `Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '上传说明',
  `MyFileName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板文件',
  `MyFilePath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) NULL DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) NULL DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float NULL DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Excel模板' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_excelfile
-- ----------------------------

-- ----------------------------
-- Table structure for sys_excelsheet
-- ----------------------------
DROP TABLE IF EXISTS `sys_excelsheet`;
CREATE TABLE `sys_excelsheet`  (
  `No` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'No - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Sheet名称',
  `FK_ExcelFile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Excel模板,外键:对应物理表:Sys_ExcelFile,表描述:Excel模板',
  `SheetIndex` int(11) NULL DEFAULT NULL COMMENT 'Sheet索引',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'ExcelSheet' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_excelsheet
-- ----------------------------

-- ----------------------------
-- Table structure for sys_exceltable
-- ----------------------------
DROP TABLE IF EXISTS `sys_exceltable`;
CREATE TABLE `sys_exceltable`  (
  `No` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据表名',
  `FK_ExcelFile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Excel模板,外键:对应物理表:Sys_ExcelFile,表描述:Excel模板',
  `IsDtl` int(11) NULL DEFAULT NULL COMMENT '是否明细表',
  `Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '数据表说明',
  `SyncToTable` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '同步到表',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Excel数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_exceltable
-- ----------------------------

-- ----------------------------
-- Table structure for sys_filemanager
-- ----------------------------
DROP TABLE IF EXISTS `sys_filemanager`;
CREATE TABLE `sys_filemanager`  (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `AttrFileName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指定名称',
  `AttrFileNo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指定编号',
  `EnName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联的表',
  `RefVal` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主键值',
  `WebPath` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Web路径',
  `MyFileName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `MyFilePath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFileExt',
  `MyFileH` int(11) NULL DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) NULL DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float NULL DEFAULT NULL COMMENT 'MyFileSize',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上传时间',
  `Rec` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上传人',
  `Doc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文件管理者' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_filemanager
-- ----------------------------

-- ----------------------------
-- Table structure for sys_formtree
-- ----------------------------
DROP TABLE IF EXISTS `sys_formtree`;
CREATE TABLE `sys_formtree`  (
  `No` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父节点No',
  `OrgNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织编号',
  `Idx` int(11) NULL DEFAULT NULL COMMENT 'Idx',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '表单树' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_formtree
-- ----------------------------
INSERT INTO `sys_formtree` VALUES ('1', '表单树', '0', '', 0);
INSERT INTO `sys_formtree` VALUES ('100', '流程独立表单', '1', '', 0);
INSERT INTO `sys_formtree` VALUES ('101', '常用信息管理', '1', '', 0);
INSERT INTO `sys_formtree` VALUES ('102', '常用单据', '1', '', 0);

-- ----------------------------
-- Table structure for sys_frmattachment
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmattachment`;
CREATE TABLE `sys_frmattachment`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `NoOfObj` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件标识',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点控制(对sln有效)',
  `AthRunModel` int(11) NULL DEFAULT NULL COMMENT '运行模式,枚举类型:0 流水模式;1 固定模式;2 自定义页面;',
  `AthSaveWay` int(11) NULL DEFAULT NULL COMMENT '保存方式,枚举类型:0 保存到web服务器;1 保存到数据库;2 ftp服务器;',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件名称',
  `Exts` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件格式',
  `NumOfUpload` int(11) NULL DEFAULT NULL COMMENT '最小上传数量',
  `TopNumOfUpload` int(11) NULL DEFAULT NULL COMMENT '最大上传数量',
  `FileMaxSize` int(11) NULL DEFAULT NULL COMMENT '附件最大限制(KB)',
  `UploadFileNumCheck` int(11) NULL DEFAULT NULL COMMENT '上传校验方式,枚举类型:0 不用校验;1 不能为空;2 每个类别下不能为空;',
  `SaveTo` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存到',
  `Sort` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `W` float NULL DEFAULT NULL COMMENT '宽度',
  `H` float NULL DEFAULT NULL COMMENT '高度',
  `IsUpload` int(11) NULL DEFAULT NULL COMMENT '是否可以上传',
  `IsVisable` int(11) NULL DEFAULT NULL COMMENT '是否显示附件分组',
  `FileType` int(11) NULL DEFAULT NULL COMMENT '附件类型,枚举类型:0 普通附件;1 图片文件;',
  `DeleteWay` int(11) NULL DEFAULT NULL COMMENT '附件删除规则,枚举类型:0 不能删除;1 删除所有;2 只能删除自己上传的;',
  `IsDownload` int(11) NULL DEFAULT NULL COMMENT '是否可以下载',
  `IsOrder` int(11) NULL DEFAULT NULL COMMENT '是否可以排序',
  `IsAutoSize` int(11) NULL DEFAULT NULL COMMENT '自动控制大小',
  `IsNote` int(11) NULL DEFAULT NULL COMMENT '是否增加备注',
  `IsExpCol` int(11) NULL DEFAULT NULL COMMENT '是否启用扩展列',
  `IsShowTitle` int(11) NULL DEFAULT NULL COMMENT '是否显示标题列',
  `UploadType` int(11) NULL DEFAULT NULL COMMENT '上传类型,枚举类型:0 单个;1 多个;2 指定;',
  `CtrlWay` int(11) NULL DEFAULT NULL COMMENT '控制呈现控制方式,枚举类型:0 按岗位;1 按部门;2 按人员;3 按SQL;',
  `AthUploadWay` int(11) NULL DEFAULT NULL COMMENT '控制上传控制方式,枚举类型:0 继承模式;1 协作模式;',
  `ReadRole` int(11) NULL DEFAULT NULL COMMENT '阅读规则,枚举类型:0 不控制;1 未阅读阻止发送;2 未阅读做记录;',
  `DataRefNoOfObj` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应附件标识',
  `IsWoEnableWF` int(11) NULL DEFAULT NULL COMMENT '是否启用weboffice',
  `IsWoEnableSave` int(11) NULL DEFAULT NULL COMMENT '是否启用保存',
  `IsWoEnableReadonly` int(11) NULL DEFAULT NULL COMMENT '是否只读',
  `IsWoEnableRevise` int(11) NULL DEFAULT NULL COMMENT '是否启用修订',
  `IsWoEnableViewKeepMark` int(11) NULL DEFAULT NULL COMMENT '是否查看用户留痕',
  `IsWoEnablePrint` int(11) NULL DEFAULT NULL COMMENT '是否打印',
  `IsWoEnableOver` int(11) NULL DEFAULT NULL COMMENT '是否启用套红',
  `IsWoEnableSeal` int(11) NULL DEFAULT NULL COMMENT '是否启用签章',
  `IsWoEnableTemplete` int(11) NULL DEFAULT NULL COMMENT '是否启用公文模板',
  `IsWoEnableCheck` int(11) NULL DEFAULT NULL COMMENT '是否自动写入审核信息',
  `IsWoEnableInsertFlow` int(11) NULL DEFAULT NULL COMMENT '是否插入流程',
  `IsWoEnableInsertFengXian` int(11) NULL DEFAULT NULL COMMENT '是否插入风险点',
  `IsWoEnableMarks` int(11) NULL DEFAULT NULL COMMENT '是否启用留痕模式',
  `IsWoEnableDown` int(11) NULL DEFAULT NULL COMMENT '是否启用下载',
  `AtPara` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  `GroupID` int(11) NULL DEFAULT NULL COMMENT 'GroupID',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `IsTurn2Html` int(11) NULL DEFAULT NULL COMMENT '是否转换成html(方便手机浏览)',
  `IsRowLock` int(11) NULL DEFAULT NULL COMMENT '是否启用锁定行',
  `IsToHeLiuHZ` int(11) NULL DEFAULT NULL COMMENT '该附件是否要汇总到合流节点上去？(对子线程节点有效)',
  `IsHeLiuHuiZong` int(11) NULL DEFAULT NULL COMMENT '是否是合流节点的汇总附件组件？(对合流节点有效)',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmattachment
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmattachmentdb
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmattachmentdb`;
CREATE TABLE `sys_frmattachmentdb`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_MapData',
  `FK_FrmAttachment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件主键',
  `NoOfObj` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件标识',
  `RefPKVal` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体主键',
  `FID` int(11) NULL DEFAULT NULL COMMENT 'FID',
  `NodeID` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点ID',
  `Sort` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别',
  `FileFullName` varchar(700) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件路径',
  `FileName` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `FileExts` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展',
  `FileSize` float NULL DEFAULT NULL COMMENT '文件大小',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  `Rec` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录人',
  `RecName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录人名字',
  `MyNote` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `IsRowLock` int(11) NULL DEFAULT NULL COMMENT '是否锁定行',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '排序',
  `UploadGUID` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上传GUID',
  `AtPara` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件数据存储' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmattachmentdb
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmbtn
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmbtn`;
CREATE TABLE `sys_frmbtn`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `Text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '标签',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `IsView` int(11) NULL DEFAULT NULL COMMENT '是否可见',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否起用',
  `UAC` int(11) NULL DEFAULT NULL COMMENT '控制类型',
  `UACContext` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '控制内容',
  `EventType` int(11) NULL DEFAULT NULL COMMENT '事件类型,枚举类型:0 禁用;1 执行URL;2 执行CCFromRef.js;',
  `EventContext` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '事件内容',
  `MsgOK` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运行成功提示',
  `MsgErr` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运行失败提示',
  `BtnID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按钮ID',
  `GroupID` int(11) NULL DEFAULT NULL COMMENT '所在分组',
  `GroupIDText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所在分组',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '按钮' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmbtn
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmele
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmele`;
CREATE TABLE `sys_frmele`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `EleType` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `EleID` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控件ID值(对部分控件有效)',
  `EleName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控件名称(对部分控件有效)',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `H` float NULL DEFAULT NULL COMMENT 'H',
  `W` float NULL DEFAULT NULL COMMENT 'W',
  `Tag1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '链接URL',
  `Tag2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag4',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '表单元素扩展' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmele
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmeledb
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmeledb`;
CREATE TABLE `sys_frmeledb`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_MapData',
  `EleID` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'EleID',
  `RefPKVal` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'RefPKVal',
  `FID` int(11) NULL DEFAULT NULL COMMENT 'FID',
  `Tag1` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag5',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '表单元素扩展DB' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmeledb
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmevent
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmevent`;
CREATE TABLE `sys_frmevent`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Event` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '事件名称',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点ID',
  `EventDoType` int(11) NULL DEFAULT NULL COMMENT '事件类型',
  `DoDoc` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '执行内容',
  `MsgOK` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '成功执行提示',
  `MsgError` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '异常信息提示',
  `MsgCtrl` int(11) NULL DEFAULT NULL COMMENT '消息发送控制,枚举类型:0 不发送;1 按设置的下一步接受人自动发送（默认）;2 由本节点表单系统字段(IsSendEmail,IsSendSMS)来决定;3 由SDK开发者参数(IsSendEmail,IsSendSMS)来决定;',
  `MailEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用邮件发送？(如果启用就要设置邮件模版，支持ccflow表达式。)',
  `MailTitle` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮件标题模版',
  `MailDoc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '邮件内容模版',
  `SMSEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用短信发送？(如果启用就要设置短信模版，支持ccflow表达式。)',
  `SMSDoc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '短信内容模版',
  `MobilePushEnable` int(11) NULL DEFAULT NULL COMMENT '是否推送到手机、pad端。',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '事件' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmevent
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmimg
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmimg`;
CREATE TABLE `sys_frmimg`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_MapData',
  `KeyOfEn` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应字段',
  `ImgAppType` int(11) NULL DEFAULT NULL COMMENT '应用类型',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `H` float NULL DEFAULT NULL COMMENT 'H',
  `W` float NULL DEFAULT NULL COMMENT 'W',
  `ImgURL` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ImgURL',
  `ImgPath` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ImgPath',
  `LinkURL` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'LinkURL',
  `LinkTarget` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'LinkTarget',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `Tag0` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `ImgSrcType` int(11) NULL DEFAULT NULL COMMENT '图片来源0=本地,1=URL',
  `IsEdit` int(11) NULL DEFAULT NULL COMMENT '是否可以编辑',
  `Name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '中文名称',
  `EnPK` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '英文名称',
  `ColSpan` int(11) NULL DEFAULT NULL COMMENT '单元格数量',
  `TextColSpan` int(11) NULL DEFAULT NULL COMMENT '文本单元格数量',
  `RowSpan` int(11) NULL DEFAULT NULL COMMENT '行数',
  `GroupID` int(11) NULL DEFAULT NULL COMMENT '显示的分组',
  `GroupIDText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示的分组',
  `UIWidth` int(11) NULL DEFAULT 0,
  `UIHeight` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图片' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmimg
-- ----------------------------
INSERT INTO `sys_frmimg` VALUES ('8580a5ce40fa4b35a2b36cc70814d95d', 'ND301', '', 0, 579.26, -2.55, 40, 137, '/ccform;component/Img/LogoBig.png', '/CCFormDesigner;component/Img/Logo//LogoBig.png', 'http://ccflow.org', '_blank', '', '', 0, 1, '', '', 0, 1, 1, 0, '0', 0, 0);
INSERT INTO `sys_frmimg` VALUES ('c7eec8f13ab04fb788d16ca8202511c7', 'ND201', '', 0, 579.26, -2.55, 40, 137, '/ccform;component/Img/LogoBig.png', '/CCFormDesigner;component/Img/Logo//LogoBig.png', 'http://ccflow.org', '_blank', '', '', 0, 1, '', '', 0, 1, 1, 0, '0', 0, 0);

-- ----------------------------
-- Table structure for sys_frmimgath
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmimgath`;
CREATE TABLE `sys_frmimgath`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `CtrlID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控件ID',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '中文名称',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `H` float NULL DEFAULT NULL COMMENT 'H',
  `W` float NULL DEFAULT NULL COMMENT 'W',
  `IsEdit` int(11) NULL DEFAULT NULL COMMENT '是否可编辑',
  `IsRequired` int(11) NULL DEFAULT NULL COMMENT '是否必填项',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图片附件' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmimgath
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmimgathdb
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmimgathdb`;
CREATE TABLE `sys_frmimgathdb`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `FK_FrmImgAth` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片附件编号',
  `RefPKVal` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体主键',
  `FileFullName` varchar(700) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件全路径',
  `FileName` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `FileExts` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展名',
  `FileSize` float NULL DEFAULT NULL COMMENT '文件大小',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  `Rec` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录人',
  `RecName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录人名字',
  `MyNote` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '剪切图片附件数据存储' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmimgathdb
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmlab
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmlab`;
CREATE TABLE `sys_frmlab`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_MapData',
  `Text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Label',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `FontSize` int(11) NULL DEFAULT NULL COMMENT '字体大小',
  `FontColor` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '颜色',
  `FontName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字体名称',
  `FontStyle` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字体风格',
  `FontWeight` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字体宽度',
  `IsBold` int(11) NULL DEFAULT NULL COMMENT '是否粗体',
  `IsItalic` int(11) NULL DEFAULT NULL COMMENT '是否斜体',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标签' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmlab
-- ----------------------------
INSERT INTO `sys_frmlab` VALUES ('0060c9b1a1044440b6d089760bd4ff34', 'ND101', '填写xxxx申请单', 79.67, 4.27, 23, '#FF000000', 'Portable User Interface', 'Normal', 'normal', 0, 0, '');
INSERT INTO `sys_frmlab` VALUES ('2531b36929264905bd7cbcacf0959edd', 'ND301', '填写xxxx申请单', 79.67, 4.27, 23, '#FF000000', 'Portable User Interface', 'Normal', 'normal', 0, 0, '');
INSERT INTO `sys_frmlab` VALUES ('3325cdd63fc949cfa650eaa5396670f0', 'ND101', 'Made&nbsp;by&nbsp;ccform', 605, 490, 14, '#FF000000', 'Portable User Interface', 'Normal', '', 0, 0, '');
INSERT INTO `sys_frmlab` VALUES ('9b6b8aca2af5482598404e4ffd7130de', 'ND301', 'Made&nbsp;by&nbsp;ccform', 605, 490, 14, '#FF000000', 'Portable User Interface', 'Normal', '', 0, 0, '');
INSERT INTO `sys_frmlab` VALUES ('cacd8fdf07af40df88e6c25c44e430cc', 'ND201', '填写xxxx申请单', 79.67, 4.27, 23, '#FF000000', 'Portable User Interface', 'Normal', 'normal', 0, 0, '');
INSERT INTO `sys_frmlab` VALUES ('d457775f513b4a5480401a6b26876d24', 'ND201', 'Made&nbsp;by&nbsp;ccform', 605, 490, 14, '#FF000000', 'Portable User Interface', 'Normal', '', 0, 0, '');

-- ----------------------------
-- Table structure for sys_frmline
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmline`;
CREATE TABLE `sys_frmline`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主表',
  `X1` float NULL DEFAULT NULL COMMENT 'X1',
  `Y1` float NULL DEFAULT NULL COMMENT 'Y1',
  `X2` float NULL DEFAULT NULL COMMENT 'X2',
  `Y2` float NULL DEFAULT NULL COMMENT 'Y2',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `BorderWidth` float NULL DEFAULT NULL COMMENT '宽度',
  `BorderColor` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '颜色',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '初始的GUID',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '线' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmline
-- ----------------------------
INSERT INTO `sys_frmline` VALUES ('1494be01ca5f4f8baf8628e711c09e66', 'ND201', 719.09, 40, 719.09, 482.73, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('1df6cc7dc32c425d84266762aaa2b41d', 'ND101', 719.09, 40, 719.09, 482.73, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('33ec90010b454a419970a658ff8ecc58', 'ND301', 83.36, 120.91, 717.91, 120.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('350a927b85234ba589a9479db9b0949f', 'ND201', 83.36, 120.91, 717.91, 120.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('37458f6dfb624ae2831249195cfae59e', 'ND201', 81.55, 80, 718.82, 80, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('3b28a2bb63f9439e83b4b76af3bcfb42', 'ND101', 83.36, 120.91, 717.91, 120.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('4e1dbb196d36461c8af8d3b136e1ed9d', 'ND201', 83.36, 40.91, 717.91, 40.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('4f4d6a077bf94099bfd673f08346dc39', 'ND301', 81.82, 40, 81.82, 480.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('5b3f9786084f41f1aae8307f83aae58c', 'ND101', 83.36, 40.91, 717.91, 40.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('70517b263a874a34807c4e0593b1b97c', 'ND201', 81.82, 481.82, 720, 481.82, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('73a522bfa17d4f15a5afbb866c650059', 'ND201', 81.82, 40, 81.82, 480.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('80d2a7ea63be417592051bc4472aec1d', 'ND301', 81.55, 80, 718.82, 80, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('84b64c3e496f43e18fbe00dc4296abc9', 'ND101', 81.82, 481.82, 720, 481.82, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('8eed52f7e65844a7bac4c448e953c5f4', 'ND101', 81.82, 40, 81.82, 480.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('b2c24c56f96f419b8c24298c44e0c398', 'ND301', 81.82, 481.82, 720, 481.82, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('bfc9538ca3d144e7b56cd32a341089a0', 'ND301', 83.36, 40.91, 717.91, 40.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('c42f477e1bb448d483926650aadaafbe', 'ND101', 81.55, 80, 718.82, 80, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('cd6abf3cc374419ebc2be99681c4f899', 'ND301', 719.09, 40, 719.09, 482.73, 9, 9, 2, 'Black', '');

-- ----------------------------
-- Table structure for sys_frmlink
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmlink`;
CREATE TABLE `sys_frmlink`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `Text` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签',
  `URL` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'URL',
  `Target` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '连接目标(_blank,_parent,_self)',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `FontSize` int(11) NULL DEFAULT NULL COMMENT 'FontSize',
  `FontColor` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FontColor',
  `FontName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FontName',
  `FontStyle` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FontStyle',
  `IsBold` int(11) NULL DEFAULT NULL COMMENT 'IsBold',
  `IsItalic` int(11) NULL DEFAULT NULL COMMENT 'IsItalic',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `GroupID` int(11) NULL DEFAULT NULL COMMENT '显示的分组',
  `GroupIDText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示的分组',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '超连接' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmlink
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmrb
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmrb`;
CREATE TABLE `sys_frmrb`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `KeyOfEn` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段',
  `EnumKey` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '枚举值',
  `Lab` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签',
  `IntKey` int(11) NULL DEFAULT NULL COMMENT 'IntKey',
  `UIIsEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `Script` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '要执行的脚本',
  `FieldsCfg` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '配置信息@FieldName=Sta',
  `SetVal` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设置的值',
  `Tip` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '选择后提示的信息',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `AtPara` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单选框' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmrb
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmreportfield
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmreportfield`;
CREATE TABLE `sys_frmreportfield`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单编号',
  `KeyOfEn` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示中文名',
  `UIWidth` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '宽度',
  `UIVisible` int(11) NULL DEFAULT NULL COMMENT '是否显示',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '表单报表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmreportfield
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmrpt
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmrpt`;
CREATE TABLE `sys_frmrpt`  (
  `No` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主表',
  `PTable` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物理表',
  `SQLOfColumn` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列的数据源',
  `SQLOfRow` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '行数据源',
  `RowIdx` int(11) NULL DEFAULT NULL COMMENT '位置',
  `GroupID` int(11) NULL DEFAULT NULL COMMENT 'GroupID',
  `IsShowSum` int(11) NULL DEFAULT NULL COMMENT 'IsShowSum',
  `IsShowIdx` int(11) NULL DEFAULT NULL COMMENT 'IsShowIdx',
  `IsCopyNDData` int(11) NULL DEFAULT NULL COMMENT 'IsCopyNDData',
  `IsHLDtl` int(11) NULL DEFAULT NULL COMMENT '是否是合流汇总',
  `IsReadonly` int(11) NULL DEFAULT NULL COMMENT 'IsReadonly',
  `IsShowTitle` int(11) NULL DEFAULT NULL COMMENT 'IsShowTitle',
  `IsView` int(11) NULL DEFAULT NULL COMMENT '是否可见',
  `IsExp` int(11) NULL DEFAULT NULL COMMENT 'IsExp',
  `IsImp` int(11) NULL DEFAULT NULL COMMENT 'IsImp',
  `IsInsert` int(11) NULL DEFAULT NULL COMMENT 'IsInsert',
  `IsDelete` int(11) NULL DEFAULT NULL COMMENT 'IsDelete',
  `IsUpdate` int(11) NULL DEFAULT NULL COMMENT 'IsUpdate',
  `IsEnablePass` int(11) NULL DEFAULT NULL COMMENT '是否启用通过审核功能?',
  `IsEnableAthM` int(11) NULL DEFAULT NULL COMMENT '是否启用多附件',
  `IsEnableM2M` int(11) NULL DEFAULT NULL COMMENT '是否启用M2M',
  `IsEnableM2MM` int(11) NULL DEFAULT NULL COMMENT '是否启用M2M',
  `WhenOverSize` int(11) NULL DEFAULT NULL COMMENT 'WhenOverSize,枚举类型:0 不处理;1 向下顺增行;2 次页显示;',
  `DtlOpenType` int(11) NULL DEFAULT NULL COMMENT '数据开放类型,枚举类型:0 操作员;1 工作ID;2 流程ID;',
  `EditModel` int(11) NULL DEFAULT NULL COMMENT '显示格式,枚举类型:0 表格;1 卡片;',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `H` float NULL DEFAULT NULL COMMENT 'H',
  `W` float NULL DEFAULT NULL COMMENT 'W',
  `FrmW` float NULL DEFAULT NULL COMMENT 'FrmW',
  `FrmH` float NULL DEFAULT NULL COMMENT 'FrmH',
  `MTR` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '多表头列',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '纬度报表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmrpt
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmsln
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmsln`;
CREATE TABLE `sys_frmsln`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Flow` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `KeyOfEn` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段',
  `Name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名',
  `EleType` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `UIIsEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用',
  `UIVisible` int(11) NULL DEFAULT NULL COMMENT '是否可见',
  `IsSigan` int(11) NULL DEFAULT NULL COMMENT '是否签名',
  `IsNotNull` int(11) NULL DEFAULT NULL COMMENT '是否为空',
  `RegularExp` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '正则表达式',
  `IsWriteToFlowTable` int(11) NULL DEFAULT NULL COMMENT '是否写入流程表',
  `IsWriteToGenerWorkFlow` int(11) NULL DEFAULT NULL COMMENT '是否写入流程注册表',
  `DefVal` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认值',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '表单字段方案' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frmsln
-- ----------------------------

-- ----------------------------
-- Table structure for sys_glovar
-- ----------------------------
DROP TABLE IF EXISTS `sys_glovar`;
CREATE TABLE `sys_glovar`  (
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '键 - 主键',
  `Name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Val` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '值',
  `GroupKey` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组值',
  `Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '说明',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '全局变量' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_glovar
-- ----------------------------
INSERT INTO `sys_glovar` VALUES ('0', '选择系统约定默认值', NULL, 'DefVal', NULL, NULL);
INSERT INTO `sys_glovar` VALUES ('@CurrWorker', '当前工作可处理人员', NULL, 'DefVal', NULL, NULL);
INSERT INTO `sys_glovar` VALUES ('@FK_ND', '当前年度', NULL, 'DefVal', NULL, NULL);
INSERT INTO `sys_glovar` VALUES ('@FK_YF', '当前月份', NULL, 'DefVal', NULL, NULL);
INSERT INTO `sys_glovar` VALUES ('@WebUser.FK_Dept', '登陆人员部门编号', NULL, 'DefVal', NULL, NULL);
INSERT INTO `sys_glovar` VALUES ('@WebUser.FK_DeptFullName', '登陆人员部门全称', NULL, 'DefVal', NULL, NULL);
INSERT INTO `sys_glovar` VALUES ('@WebUser.FK_DeptName', '登陆人员部门名称', NULL, 'DefVal', NULL, NULL);
INSERT INTO `sys_glovar` VALUES ('@WebUser.Name', '登陆人员名称', NULL, 'DefVal', NULL, NULL);
INSERT INTO `sys_glovar` VALUES ('@WebUser.No', '登陆人员账号', NULL, 'DefVal', NULL, NULL);
INSERT INTO `sys_glovar` VALUES ('@yyyy年MM月dd日', '当前日期(yyyy年MM月dd日)', NULL, 'DefVal', NULL, NULL);
INSERT INTO `sys_glovar` VALUES ('@yyyy年MM月dd日HH时mm分', '当前日期(yyyy年MM月dd日HH时mm分)', NULL, 'DefVal', NULL, NULL);
INSERT INTO `sys_glovar` VALUES ('@yy年MM月dd日', '当前日期(yy年MM月dd日)', NULL, 'DefVal', NULL, NULL);
INSERT INTO `sys_glovar` VALUES ('@yy年MM月dd日HH时mm分', '当前日期(yy年MM月dd日HH时mm分)', NULL, 'DefVal', NULL, NULL);

-- ----------------------------
-- Table structure for sys_groupenstemplate
-- ----------------------------
DROP TABLE IF EXISTS `sys_groupenstemplate`;
CREATE TABLE `sys_groupenstemplate`  (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `EnName` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表称',
  `Name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报表名',
  `EnsName` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报表类名',
  `OperateCol` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作属性',
  `Attrs` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运算属性',
  `Rec` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录人',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '报表模板' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_groupenstemplate
-- ----------------------------

-- ----------------------------
-- Table structure for sys_groupfield
-- ----------------------------
DROP TABLE IF EXISTS `sys_groupfield`;
CREATE TABLE `sys_groupfield`  (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Lab` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签',
  `FrmID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `CtrlType` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控件类型',
  `CtrlID` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控件ID',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `AtPara` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '傻瓜表单分组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_groupfield
-- ----------------------------
INSERT INTO `sys_groupfield` VALUES (112, '申请', 'ND102', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (113, '开始', 'ND101', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (114, '基础信息', 'ND1Rpt', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (115, '流程信息', 'ND1Rpt', '', '', 2, '', '');
INSERT INTO `sys_groupfield` VALUES (116, '科长审批', 'ND103', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (117, '科长审批意见', 'ND103', '', '', 2, '', '');
INSERT INTO `sys_groupfield` VALUES (118, '部长审批', 'ND104', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (119, '部长审批意见', 'ND104', '', '', 2, '', '');
INSERT INTO `sys_groupfield` VALUES (120, '董事长审批', 'ND105', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (122, '测试', 'ND101', '', '', 2, '', '');
INSERT INTO `sys_groupfield` VALUES (126, '基础信息', 'ND202', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (129, 'Start Node', 'ND201', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (131, '基础信息', 'ND2Rpt', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (133, '流程信息', 'ND2Rpt', '', '', 2, '', '');
INSERT INTO `sys_groupfield` VALUES (137, '基础信息', 'ND302', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (138, '开始节点', '', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (140, 'Start Node', 'ND301', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (142, '基础信息', 'ND3Rpt', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (144, '流程信息', 'ND3Rpt', '', '', 2, '', '');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_langue
-- ----------------------------
DROP TABLE IF EXISTS `sys_langue`;
CREATE TABLE `sys_langue`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Langue` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '语言ID',
  `Model` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块',
  `ModelKey` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块实例',
  `Sort` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别',
  `SortKey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别PK',
  `Val` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '语言值',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '语言定义' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_langue
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (1, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-02-14 15:14:54');
INSERT INTO `sys_logininfor` VALUES (2, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-02-14 15:14:57');
INSERT INTO `sys_logininfor` VALUES (3, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-02-14 15:24:57');
INSERT INTO `sys_logininfor` VALUES (4, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-02-14 15:36:20');
INSERT INTO `sys_logininfor` VALUES (5, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-02-14 15:36:24');
INSERT INTO `sys_logininfor` VALUES (6, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-02-14 15:36:27');
INSERT INTO `sys_logininfor` VALUES (7, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '密码输入错误1次', '2020-02-14 15:36:33');
INSERT INTO `sys_logininfor` VALUES (8, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-02-14 15:37:11');
INSERT INTO `sys_logininfor` VALUES (9, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '密码输入错误1次', '2020-02-14 15:37:16');
INSERT INTO `sys_logininfor` VALUES (10, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-02-14 15:40:22');

-- ----------------------------
-- Table structure for sys_mapattr
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapattr`;
CREATE TABLE `sys_mapattr`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `KeyOfEn` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段',
  `Name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `DefVal` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '默认值',
  `DefValType` int(11) NULL DEFAULT NULL COMMENT '默认值类型',
  `UIContralType` int(11) NULL DEFAULT NULL COMMENT '控件',
  `MyDataType` int(11) NULL DEFAULT NULL COMMENT '数据类型',
  `LGType` int(11) NULL DEFAULT NULL COMMENT '逻辑类型,枚举类型:0 普通;1 枚举;2 外键;3 打开系统页面;',
  `UIWidth` float NULL DEFAULT NULL COMMENT '宽度',
  `UIHeight` float NULL DEFAULT NULL COMMENT '高度',
  `MinLen` int(11) NULL DEFAULT NULL COMMENT '最小长度',
  `MaxLen` int(11) NULL DEFAULT NULL COMMENT '最大长度',
  `UIBindKey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绑定的信息',
  `UIRefKey` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绑定的Key',
  `UIRefKeyText` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绑定的Text',
  `ExtIsSum` int(11) NULL DEFAULT NULL COMMENT '是否显示合计(对从表有效)',
  `UIVisible` int(11) NULL DEFAULT NULL COMMENT '是否可见',
  `UIIsEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `UIIsLine` int(11) NULL DEFAULT NULL COMMENT '是否单独栏显示',
  `UIIsInput` int(11) NULL DEFAULT NULL COMMENT '是否必填字段',
  `IsRichText` int(11) NULL DEFAULT NULL COMMENT '富文本',
  `IsSupperText` int(11) NULL DEFAULT NULL COMMENT '富文本',
  `FontSize` int(11) NULL DEFAULT NULL COMMENT '富文本',
  `IsSigan` int(11) NULL DEFAULT NULL COMMENT '签字？',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `EditType` int(11) NULL DEFAULT NULL COMMENT '编辑类型',
  `Tag` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标识',
  `Tag1` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标识1',
  `Tag2` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标识2',
  `Tag3` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标识3',
  `Tip` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '激活提示',
  `ColSpan` int(11) NULL DEFAULT NULL COMMENT '单元格数量',
  `TextColSpan` int(11) NULL DEFAULT NULL COMMENT '文本单元格数量',
  `RowSpan` int(11) NULL DEFAULT NULL COMMENT '行数',
  `CSS` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义样式',
  `GroupID` int(11) NULL DEFAULT NULL COMMENT '显示的分组',
  `IsEnableInAPP` int(11) NULL DEFAULT NULL COMMENT '是否在移动端中显示',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '序号',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AtPara',
  `GroupIDText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示的分组',
  `CSSText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义样式',
  `DefValText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认值（选中）',
  `RBShowModel` int(11) NULL DEFAULT NULL COMMENT '单选按钮的展现方式,枚举类型:0 竖向;3 横向;',
  `IsEnableJS` int(11) NULL DEFAULT NULL COMMENT '是否启用JS高级设置？',
  `ExtDefVal` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统默认值',
  `ExtDefValText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统默认值',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '流程进度图' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_mapattr
-- ----------------------------
INSERT INTO `sys_mapattr` VALUES ('ND101_CDT', 'ND101', 'CDT', '发起时间', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 113, 1, 1001, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND101_Emps', 'ND101', 'Emps', 'Emps', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 113, 1, 1003, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND101_FID', 'ND101', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 113, 1, 1000, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND101_FK_Dept', 'ND101', 'FK_Dept', '操作员部门', '', 0, 1, 1, 2, 100, 23, 0, 50, 'BP.Port.Depts', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 113, 1, 1004, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND101_FK_NY', 'ND101', 'FK_NY', '年月', '', 0, 0, 1, 0, 100, 23, 0, 7, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 113, 1, 1005, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND101_MyNum', 'ND101', 'MyNum', '个数', '1', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 113, 1, 1006, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND101_OID', 'ND101', 'OID', 'OID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 113, 1, 999, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND101_RDT', 'ND101', 'RDT', '更新时间', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 113, 1, 999, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND101_Rec', 'ND101', 'Rec', '发起人', '@WebUser.No', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 113, 1, 1002, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND101_Title', 'ND101', 'Title', '标题', '', 0, 0, 1, 0, 251, 23, 0, 200, '', '', '', 0, 0, 1, 1, 0, 0, 0, 0, 0, 171.2, 68.4, '', 1, '', '', '', '', '', 1, 1, 1, '0', 113, 1, -1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND101_XingMing', 'ND101', 'XingMing', '姓名', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 122, 1, 2, '@FontSize=12', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND102_CDT', 'ND102', 'CDT', '完成时间', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 112, 1, 4, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND102_Emps', 'ND102', 'Emps', 'Emps', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 6, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND102_FID', 'ND102', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND102_FK_Dept', 'ND102', 'FK_Dept', '操作员部门', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 7, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND102_JSSJ', 'ND102', 'JSSJ', '结束时间', '@RDT', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 10, '@FontSize=12', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND102_KSSJ', 'ND102', 'KSSJ', '开始时间', '@RDT', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 9, '@FontSize=12', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND102_OID', 'ND102', 'OID', 'WorkID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND102_QJR', 'ND102', 'QJR', '请假人', '@WebUser.Name', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 8, '@FontSize=12@IsRichText=0@IsSupperText=0', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND102_QJYY', 'ND102', 'QJYY', '请假原因', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 11, '@FontSize=12', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND102_RDT', 'ND102', 'RDT', '接受时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 112, 1, 3, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND102_Rec', 'ND102', 'Rec', '记录人', '@WebUser.No', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 5, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND103_CDT', 'ND103', 'CDT', '完成时间', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 116, 1, 4, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND103_Emps', 'ND103', 'Emps', 'Emps', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 116, 1, 6, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND103_FID', 'ND103', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 116, 1, 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND103_FK_Dept', 'ND103', 'FK_Dept', '操作员部门', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 116, 1, 7, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND103_KZSPYJ_Checker', 'ND103', 'KZSPYJ_Checker', '审核人', '@WebUser.No', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 1, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND103_KZSPYJ_Note', 'ND103', 'KZSPYJ_Note', '审核意见', '', 0, 0, 1, 0, 100, 69, 0, 4000, '', '', '', 0, 1, 1, 1, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 117, 1, 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND103_KZSPYJ_RDT', 'ND103', 'KZSPYJ_RDT', '审核日期', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 3, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND103_OID', 'ND103', 'OID', 'WorkID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 116, 1, 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND103_RDT', 'ND103', 'RDT', '接受时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 116, 1, 3, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND103_Rec', 'ND103', 'Rec', '记录人', '@WebUser.No', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 116, 1, 5, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND104_BZSPYJ_Checker', 'ND104', 'BZSPYJ_Checker', '审核人', '@WebUser.No', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 1, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 119, 1, 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND104_BZSPYJ_Note', 'ND104', 'BZSPYJ_Note', '审核意见', '', 0, 0, 1, 0, 100, 69, 0, 4000, '', '', '', 0, 1, 1, 1, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 119, 1, 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND104_BZSPYJ_RDT', 'ND104', 'BZSPYJ_RDT', '审核日期', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 119, 1, 3, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND104_CDT', 'ND104', 'CDT', '完成时间', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 118, 1, 4, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND104_Emps', 'ND104', 'Emps', 'Emps', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 118, 1, 6, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND104_FID', 'ND104', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 118, 1, 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND104_FK_Dept', 'ND104', 'FK_Dept', '操作员部门', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 118, 1, 7, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND104_OID', 'ND104', 'OID', 'WorkID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 118, 1, 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND104_RDT', 'ND104', 'RDT', '接受时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 118, 1, 3, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND104_Rec', 'ND104', 'Rec', '记录人', '@WebUser.No', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 118, 1, 5, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND105_CDT', 'ND105', 'CDT', '完成时间', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 120, 1, 4, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND105_Emps', 'ND105', 'Emps', 'Emps', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 120, 1, 6, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND105_FID', 'ND105', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 120, 1, 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND105_FK_Dept', 'ND105', 'FK_Dept', '操作员部门', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 120, 1, 7, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND105_OID', 'ND105', 'OID', 'WorkID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 120, 1, 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND105_RDT', 'ND105', 'RDT', '接受时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 120, 1, 3, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND105_Rec', 'ND105', 'Rec', '记录人', '@WebUser.No', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 120, 1, 5, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_AtPara', 'ND1Rpt', 'AtPara', '参数', '', 0, 0, 1, 0, 100, 23, 0, 4000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_BillNo', 'ND1Rpt', 'BillNo', '单据编号', '', 0, 0, 1, 0, 100, 23, 0, 100, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_BZSPYJ_Checker', 'ND1Rpt', 'BZSPYJ_Checker', '审核人', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 1, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 119, 1, 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_BZSPYJ_Note', 'ND1Rpt', 'BZSPYJ_Note', '审核意见', '', 0, 0, 1, 0, 100, 69, 0, 4000, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 119, 1, 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_BZSPYJ_RDT', 'ND1Rpt', 'BZSPYJ_RDT', '审核日期', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 119, 1, 3, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_CDT', 'ND1Rpt', 'CDT', '活动时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 104, 1, 1001, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_Emps', 'ND1Rpt', 'Emps', '参与者', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 104, 1, 1003, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FID', 'ND1Rpt', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 104, 1, 1000, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FK_Dept', 'ND1Rpt', 'FK_Dept', '操作员部门', '', 0, 0, 1, 0, 100, 23, 0, 100, 'BP.Port.Depts', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 115, 1, 1004, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FK_NY', 'ND1Rpt', 'FK_NY', '年月', '', 0, 0, 1, 0, 100, 23, 0, 7, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 115, 1, 1005, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowDaySpan', 'ND1Rpt', 'FlowDaySpan', '跨度(天)', '', 0, 0, 8, 0, 100, 23, 0, 300, '', '', '', 0, 1, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 115, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowEmps', 'ND1Rpt', 'FlowEmps', '参与人', '', 0, 0, 1, 0, 100, 23, 0, 1000, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 115, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowEnder', 'ND1Rpt', 'FlowEnder', '结束人', '', 0, 1, 1, 2, 100, 23, 0, 20, 'BP.Port.Emps', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 115, 1, -1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowEnderRDT', 'ND1Rpt', 'FlowEnderRDT', '结束时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 115, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowEndNode', 'ND1Rpt', 'FlowEndNode', '结束节点', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 115, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowNote', 'ND1Rpt', 'FlowNote', '流程信息', '', 0, 0, 1, 0, 100, 23, 0, 500, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowStarter', 'ND1Rpt', 'FlowStarter', '发起人', '', 0, 1, 1, 2, 100, 23, 0, 20, 'BP.Port.Emps', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 115, 1, -1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowStartRDT', 'ND1Rpt', 'FlowStartRDT', '发起时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 115, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_GUID', 'ND1Rpt', 'GUID', 'GUID', '', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_JSSJ', 'ND1Rpt', 'JSSJ', '结束时间', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 10, '@FontSize=12', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_KSSJ', 'ND1Rpt', 'KSSJ', '开始时间', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 9, '@FontSize=12', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_KZSPYJ_Checker', 'ND1Rpt', 'KZSPYJ_Checker', '审核人', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 1, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_KZSPYJ_Note', 'ND1Rpt', 'KZSPYJ_Note', '审核意见', '', 0, 0, 1, 0, 100, 69, 0, 4000, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 117, 1, 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_KZSPYJ_RDT', 'ND1Rpt', 'KZSPYJ_RDT', '审核日期', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 3, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_MyNum', 'ND1Rpt', 'MyNum', '个数', '1', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 115, 1, 1006, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_OID', 'ND1Rpt', 'OID', 'OID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 0, 1, 999, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_PEmp', 'ND1Rpt', 'PEmp', '调起子流程的人员', '', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_PFlowNo', 'ND1Rpt', 'PFlowNo', '父流程流程编号', '', 0, 0, 1, 0, 100, 23, 0, 3, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 115, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_PNodeID', 'ND1Rpt', 'PNodeID', '父流程启动的节点', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_PrjName', 'ND1Rpt', 'PrjName', '项目名称', '', 0, 0, 1, 0, 100, 23, 0, 100, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_PrjNo', 'ND1Rpt', 'PrjNo', '项目编号', '', 0, 0, 1, 0, 100, 23, 0, 100, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_PWorkID', 'ND1Rpt', 'PWorkID', '父流程WorkID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 115, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_QJR', 'ND1Rpt', 'QJR', '请假人', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 8, '@FontSize=12@IsRichText=0@IsSupperText=0', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_QJYY', 'ND1Rpt', 'QJYY', '请假原因', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 11, '@FontSize=12', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_RDT', 'ND1Rpt', 'RDT', '更新时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 0, 1, 999, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_Rec', 'ND1Rpt', 'Rec', '发起人', '', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 104, 1, 1002, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_Title', 'ND1Rpt', 'Title', '标题', '', 0, 0, 1, 0, 251, 23, 0, 200, '', '', '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 171.2, 68.4, '', 1, '', '', '', '', '', 1, 1, 1, '0', 104, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_WFSta', 'ND1Rpt', 'WFSta', '状态', '', 0, 1, 2, 1, 100, 23, 0, 1000, 'WFSta', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, -1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_WFState', 'ND1Rpt', 'WFState', '流程状态', '', 0, 1, 2, 1, 100, 23, 0, 1000, 'WFState', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 115, 1, -1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND201_CDT', 'ND201', 'CDT', '发起时间', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 129, 1, 1001, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND201_Emps', 'ND201', 'Emps', 'Emps', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 129, 1, 1003, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND201_FID', 'ND201', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 129, 1, 1000, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND201_FK_Dept', 'ND201', 'FK_Dept', '操作员部门', '', 0, 1, 1, 2, 100, 23, 0, 50, 'BP.Port.Depts', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 129, 1, 1004, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND201_FK_NY', 'ND201', 'FK_NY', '年月', '', 0, 0, 1, 0, 100, 23, 0, 7, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 129, 1, 1005, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND201_MyNum', 'ND201', 'MyNum', '个数', '1', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 129, 1, 1006, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND201_OID', 'ND201', 'OID', 'OID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 0, 1, 999, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND201_RDT', 'ND201', 'RDT', '更新时间', '@RDT', 0, 0, 7, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 0, 1, 999, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND201_Rec', 'ND201', 'Rec', '发起人', '@WebUser.No', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 129, 1, 1002, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND201_Title', 'ND201', 'Title', '标题', '', 0, 0, 1, 0, 251, 23, 0, 200, '', '', '', 0, 0, 1, 1, 0, 0, 0, 0, 0, 171.2, 68.4, '', 1, '', '', '', '', '', 1, 1, 1, '0', 129, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND202_CDT', 'ND202', 'CDT', '完成时间', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 126, 1, 4, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND202_Emps', 'ND202', 'Emps', 'Emps', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 126, 1, 6, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND202_FID', 'ND202', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 126, 1, 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND202_FK_Dept', 'ND202', 'FK_Dept', '操作员部门', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 126, 1, 7, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND202_OID', 'ND202', 'OID', 'WorkID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 126, 1, 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND202_RDT', 'ND202', 'RDT', '接受时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 126, 1, 3, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND202_Rec', 'ND202', 'Rec', '记录人', '@WebUser.No', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 126, 1, 5, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_AtPara', 'ND2Rpt', 'AtPara', '参数', '', 0, 0, 1, 0, 100, 23, 0, 4000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 131, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_BillNo', 'ND2Rpt', 'BillNo', '单据编号', '', 0, 0, 1, 0, 100, 23, 0, 100, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 131, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_CDT', 'ND2Rpt', 'CDT', '活动时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 129, 1, 1001, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_Emps', 'ND2Rpt', 'Emps', '参与者', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 129, 1, 1003, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_FID', 'ND2Rpt', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 129, 1, 1000, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_FK_Dept', 'ND2Rpt', 'FK_Dept', '操作员部门', '', 0, 0, 1, 0, 100, 23, 0, 100, 'BP.Port.Depts', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 133, 1, 1004, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_FK_NY', 'ND2Rpt', 'FK_NY', '年月', '', 0, 0, 1, 0, 100, 23, 0, 7, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 133, 1, 1005, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_FlowDaySpan', 'ND2Rpt', 'FlowDaySpan', '跨度(天)', '', 0, 0, 8, 0, 100, 23, 0, 300, '', '', '', 0, 1, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 133, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_FlowEmps', 'ND2Rpt', 'FlowEmps', '参与人', '', 0, 0, 1, 0, 100, 23, 0, 1000, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 133, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_FlowEnder', 'ND2Rpt', 'FlowEnder', '结束人', '', 0, 1, 1, 2, 100, 23, 0, 20, 'BP.Port.Emps', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 133, 1, -1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_FlowEnderRDT', 'ND2Rpt', 'FlowEnderRDT', '结束时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 133, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_FlowEndNode', 'ND2Rpt', 'FlowEndNode', '结束节点', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 133, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_FlowNote', 'ND2Rpt', 'FlowNote', '流程信息', '', 0, 0, 1, 0, 100, 23, 0, 500, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 131, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_FlowStarter', 'ND2Rpt', 'FlowStarter', '发起人', '', 0, 1, 1, 2, 100, 23, 0, 20, 'BP.Port.Emps', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 133, 1, -1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_FlowStartRDT', 'ND2Rpt', 'FlowStartRDT', '发起时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 133, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_GUID', 'ND2Rpt', 'GUID', 'GUID', '', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 131, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_MyNum', 'ND2Rpt', 'MyNum', '个数', '1', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 133, 1, 1006, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_OID', 'ND2Rpt', 'OID', 'OID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 0, 1, 999, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_PEmp', 'ND2Rpt', 'PEmp', '调起子流程的人员', '', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 131, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_PFlowNo', 'ND2Rpt', 'PFlowNo', '父流程流程编号', '', 0, 0, 1, 0, 100, 23, 0, 3, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 133, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_PNodeID', 'ND2Rpt', 'PNodeID', '父流程启动的节点', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 131, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_PrjName', 'ND2Rpt', 'PrjName', '项目名称', '', 0, 0, 1, 0, 100, 23, 0, 100, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 131, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_PrjNo', 'ND2Rpt', 'PrjNo', '项目编号', '', 0, 0, 1, 0, 100, 23, 0, 100, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 131, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_PWorkID', 'ND2Rpt', 'PWorkID', '父流程WorkID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 133, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_RDT', 'ND2Rpt', 'RDT', '更新时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 0, 1, 999, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_Rec', 'ND2Rpt', 'Rec', '发起人', '', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 129, 1, 1002, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_Title', 'ND2Rpt', 'Title', '标题', '', 0, 0, 1, 0, 251, 23, 0, 200, '', '', '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 171.2, 68.4, '', 1, '', '', '', '', '', 1, 1, 1, '0', 129, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_WFSta', 'ND2Rpt', 'WFSta', '状态', '', 0, 1, 2, 1, 100, 23, 0, 1000, 'WFSta', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 131, 1, -1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND2Rpt_WFState', 'ND2Rpt', 'WFState', '流程状态', '', 0, 1, 2, 1, 100, 23, 0, 1000, 'WFState', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 133, 1, -1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND301_CDT', 'ND301', 'CDT', '发起时间', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 140, 1, 1001, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND301_Emps', 'ND301', 'Emps', 'Emps', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 140, 1, 1003, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND301_FID', 'ND301', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 140, 1, 1000, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND301_FK_Dept', 'ND301', 'FK_Dept', '操作员部门', '', 0, 1, 1, 2, 100, 23, 0, 50, 'BP.Port.Depts', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 140, 1, 1004, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND301_FK_NY', 'ND301', 'FK_NY', '年月', '', 0, 0, 1, 0, 100, 23, 0, 7, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 140, 1, 1005, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND301_MyNum', 'ND301', 'MyNum', '个数', '1', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 140, 1, 1006, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND301_OID', 'ND301', 'OID', 'OID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 0, 1, 999, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND301_RDT', 'ND301', 'RDT', '更新时间', '@RDT', 0, 0, 7, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 0, 1, 999, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND301_Rec', 'ND301', 'Rec', '发起人', '@WebUser.No', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 140, 1, 1002, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND301_Title', 'ND301', 'Title', '标题', '', 0, 0, 1, 0, 251, 23, 0, 200, '', '', '', 0, 0, 1, 1, 0, 0, 0, 0, 0, 171.2, 68.4, '', 1, '', '', '', '', '', 1, 1, 1, '0', 140, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND302_CDT', 'ND302', 'CDT', '完成时间', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 137, 1, 4, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND302_Emps', 'ND302', 'Emps', 'Emps', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 137, 1, 6, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND302_FID', 'ND302', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 137, 1, 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND302_FK_Dept', 'ND302', 'FK_Dept', '操作员部门', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 137, 1, 7, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND302_OID', 'ND302', 'OID', 'WorkID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 137, 1, 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND302_RDT', 'ND302', 'RDT', '接受时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 137, 1, 3, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND302_Rec', 'ND302', 'Rec', '记录人', '@WebUser.No', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 137, 1, 5, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_AtPara', 'ND3Rpt', 'AtPara', '参数', '', 0, 0, 1, 0, 100, 23, 0, 4000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 142, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_BillNo', 'ND3Rpt', 'BillNo', '单据编号', '', 0, 0, 1, 0, 100, 23, 0, 100, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 142, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_CDT', 'ND3Rpt', 'CDT', '活动时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 140, 1, 1001, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_Emps', 'ND3Rpt', 'Emps', '参与者', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 140, 1, 1003, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_FID', 'ND3Rpt', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 140, 1, 1000, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_FK_Dept', 'ND3Rpt', 'FK_Dept', '操作员部门', '', 0, 0, 1, 0, 100, 23, 0, 100, 'BP.Port.Depts', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 144, 1, 1004, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_FK_NY', 'ND3Rpt', 'FK_NY', '年月', '', 0, 0, 1, 0, 100, 23, 0, 7, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 144, 1, 1005, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_FlowDaySpan', 'ND3Rpt', 'FlowDaySpan', '跨度(天)', '', 0, 0, 8, 0, 100, 23, 0, 300, '', '', '', 0, 1, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 144, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_FlowEmps', 'ND3Rpt', 'FlowEmps', '参与人', '', 0, 0, 1, 0, 100, 23, 0, 1000, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 144, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_FlowEnder', 'ND3Rpt', 'FlowEnder', '结束人', '', 0, 1, 1, 2, 100, 23, 0, 20, 'BP.Port.Emps', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 144, 1, -1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_FlowEnderRDT', 'ND3Rpt', 'FlowEnderRDT', '结束时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 144, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_FlowEndNode', 'ND3Rpt', 'FlowEndNode', '结束节点', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 144, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_FlowNote', 'ND3Rpt', 'FlowNote', '流程信息', '', 0, 0, 1, 0, 100, 23, 0, 500, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 142, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_FlowStarter', 'ND3Rpt', 'FlowStarter', '发起人', '', 0, 1, 1, 2, 100, 23, 0, 20, 'BP.Port.Emps', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 144, 1, -1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_FlowStartRDT', 'ND3Rpt', 'FlowStartRDT', '发起时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 144, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_GUID', 'ND3Rpt', 'GUID', 'GUID', '', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 142, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_MyNum', 'ND3Rpt', 'MyNum', '个数', '1', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 144, 1, 1006, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_OID', 'ND3Rpt', 'OID', 'OID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 0, 1, 999, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_PEmp', 'ND3Rpt', 'PEmp', '调起子流程的人员', '', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 142, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_PFlowNo', 'ND3Rpt', 'PFlowNo', '父流程流程编号', '', 0, 0, 1, 0, 100, 23, 0, 3, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 144, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_PNodeID', 'ND3Rpt', 'PNodeID', '父流程启动的节点', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 142, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_PrjName', 'ND3Rpt', 'PrjName', '项目名称', '', 0, 0, 1, 0, 100, 23, 0, 100, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 142, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_PrjNo', 'ND3Rpt', 'PrjNo', '项目编号', '', 0, 0, 1, 0, 100, 23, 0, 100, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 142, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_PWorkID', 'ND3Rpt', 'PWorkID', '父流程WorkID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 144, 1, -101, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_RDT', 'ND3Rpt', 'RDT', '更新时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 0, 1, 999, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_Rec', 'ND3Rpt', 'Rec', '发起人', '', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 140, 1, 1002, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_Title', 'ND3Rpt', 'Title', '标题', '', 0, 0, 1, 0, 251, 23, 0, 200, '', '', '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 171.2, 68.4, '', 1, '', '', '', '', '', 1, 1, 1, '0', 140, 1, -100, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_WFSta', 'ND3Rpt', 'WFSta', '状态', '', 0, 1, 2, 1, 100, 23, 0, 1000, 'WFSta', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 142, 1, -1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapattr` VALUES ('ND3Rpt_WFState', 'ND3Rpt', 'WFState', '流程状态', '', 0, 1, 2, 1, 100, 23, 0, 1000, 'WFState', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 144, 1, -1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_mapdata
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapdata`;
CREATE TABLE `sys_mapdata`  (
  `No` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'null - 主键',
  `FrmType` int(11) NULL DEFAULT NULL COMMENT '表单类型',
  `PTable` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物理表',
  `Name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'null',
  `FK_FormTree` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单树',
  `TableCol` int(11) NULL DEFAULT NULL COMMENT '表单显示列数,枚举类型:0 4列;1 6列;2 上下模式3列;',
  `RowOpenModel` int(11) NULL DEFAULT NULL COMMENT '行记录打开模式,枚举类型:0 新窗口打开;1 在本窗口打开;2 弹出窗口打开,关闭后不刷新列表;3 弹出窗口打开,关闭后刷新列表;',
  `EntityType` int(11) NULL DEFAULT NULL COMMENT '业务类型,枚举类型:0 独立表单;1 单据;2 编号名称实体;3 树结构实体;',
  `BillNoFormat` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体编号规则',
  `TitleRole` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题生成规则',
  `SortColumns` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '排序字段',
  `BtnNewLable` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '新建',
  `BtnNewModel` int(11) NULL DEFAULT NULL COMMENT '新建模式,枚举类型:0 表格模式;1 卡片模式;2 不可用;',
  `BtnSaveLable` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存',
  `BtnSubmitLable` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '提交',
  `BtnDelLable` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除',
  `BtnSearchLabel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列表',
  `BtnGroupLabel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分析',
  `BtnGroupEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `BtnPrintHtml` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印Html',
  `BtnPrintHtmlEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `BtnPrintPDF` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印PDF',
  `BtnPrintPDFEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `BtnPrintRTF` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印RTF',
  `BtnPrintRTFEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `BtnPrintCCWord` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印CCWord',
  `BtnPrintCCWordEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `BtnExpZip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '导出zip文件',
  `BtnExpZipEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `BtnRefBill` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联单据',
  `RefBillRole` int(11) NULL DEFAULT NULL COMMENT '关联单据工作模式,枚举类型:0 不启用;1 非必须选择关联单据;2 必须选择关联单据;',
  `RefBill` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联单据ID',
  `BtnImpExcel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '导入Excel文件',
  `BtnImpExcelEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `BtnExpExcel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '导出Excel文件',
  `BtnExpExcelEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `Designer` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设计者',
  `DesignerContact` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `DesignerUnit` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `Ver` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本号',
  `Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `Tag0` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag0',
  `Tag1` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Tag1',
  `Tag2` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag2',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AtPara',
  `PopHeight` int(11) NULL DEFAULT NULL COMMENT '弹窗高度',
  `PopWidth` int(11) NULL DEFAULT NULL COMMENT '弹窗宽度',
  `EntityEditModel` int(11) NULL DEFAULT NULL COMMENT '编辑模式',
  `EntityShowModel` int(11) NULL DEFAULT NULL COMMENT '展示模式,枚举类型:0 表格;1 树干模式;',
  `FormEventEntity` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '事件实体',
  `EnPK` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体主键',
  `PTableModel` int(11) NULL DEFAULT NULL COMMENT '表存储模式',
  `URL` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Url',
  `Dtls` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '从表',
  `FrmW` int(11) NULL DEFAULT NULL COMMENT '系统表单宽度',
  `FrmH` int(11) NULL DEFAULT NULL COMMENT '系统表单高度',
  `Tag` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag',
  `FK_FrmSort` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单类别',
  `FrmShowType` int(11) NULL DEFAULT NULL COMMENT '表单展示方式,枚举类型:0 普通方式;1 页签方式;',
  `AppType` int(11) NULL DEFAULT NULL COMMENT '应用类型',
  `DBSrc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源,外键:对应物理表:Sys_SFDBSrc,表描述:数据源',
  `BodyAttr` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单Body属性',
  `FlowCtrls` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程控件',
  `IsTemplate` int(11) NULL DEFAULT NULL COMMENT '是否是表单模版',
  `OfficeOpenLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存标签',
  `OfficeSaveEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeOverLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套红按钮标签',
  `OfficeOverEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeMarksEnable` int(11) NULL DEFAULT NULL COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印按钮标签',
  `OfficePrintEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeSealLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签章按钮标签',
  `OfficeSealEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeNodeInfo` int(11) NULL DEFAULT NULL COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) NULL DEFAULT NULL COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeIsMarks` int(11) NULL DEFAULT NULL COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指定文档模板',
  `OfficeIsParent` int(11) NULL DEFAULT NULL COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) NULL DEFAULT NULL COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自动套红模板',
  `FK_Flow` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '独立表单属性:FK_Flow',
  `RightViewWay` int(11) NULL DEFAULT NULL COMMENT '报表查看权限控制方式',
  `RightViewTag` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '报表查看权限控制Tag',
  `RightDeptWay` int(11) NULL DEFAULT NULL COMMENT '部门数据查看控制方式',
  `RightDeptTag` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '部门数据查看控制Tag',
  `DBURL` int(11) NULL DEFAULT NULL COMMENT 'DBURL',
  `TemplaterVer` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模版编号',
  `DBSave` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Word数据文件存储',
  `MyFileName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单模版',
  `MyFilePath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) NULL DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) NULL DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float(11, 2) NULL DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统表单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_mapdata
-- ----------------------------
INSERT INTO `sys_mapdata` VALUES ('ND101', 0, 'ND1Rpt', 'Start Node', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '2020-02-12 14:31:01', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', NULL, NULL, NULL, NULL, '', '', 0, '', '', 900, 1200, '', '', 0, 0, 'local', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapdata` VALUES ('ND102', 0, 'ND1Rpt', 'Node 2', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '2020-02-12 14:31:01', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', NULL, NULL, NULL, NULL, '', '', 0, '', '', 900, 1200, '', '', 0, 0, 'local', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapdata` VALUES ('ND103', 0, 'ND1Rpt', 'New Node 3', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '2020-02-12 14:31:01', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', NULL, NULL, NULL, NULL, '', '', 0, '', '', 900, 1200, '', '', 0, 0, 'local', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapdata` VALUES ('ND104', 0, 'ND1Rpt', 'New Node 4', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '2020-02-12 14:31:01', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', NULL, NULL, NULL, NULL, '', '', 0, '', '', 900, 1200, '', '', 0, 0, 'local', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapdata` VALUES ('ND105', 0, 'ND1Rpt', 'New Node 5', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '2020-02-12 14:31:01', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', NULL, NULL, NULL, NULL, '', '', 0, '', '', 900, 1200, '', '', 0, 0, 'local', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapdata` VALUES ('ND1Rpt', 0, 'ND1Rpt', '请假流程 可用', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '2020-02-13 18:43:49', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', NULL, NULL, NULL, NULL, '', '', 0, '', '', 900, 1200, '', '', 0, 0, 'local', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapdata` VALUES ('ND201', 0, 'ND2Rpt', 'Start Node', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '2020-02-14 11:00:51', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', NULL, NULL, NULL, NULL, '', '', 0, '', '', 900, 1200, '', '', 0, 0, 'local', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapdata` VALUES ('ND202', 0, 'ND2Rpt', 'Node 2', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '2020-02-14 11:00:50', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', NULL, NULL, NULL, NULL, '', '', 0, '', '', 900, 1200, '', '', 0, 0, 'local', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapdata` VALUES ('ND2Rpt', 1, 'ND2Rpt', '打架流程', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '2020-02-14 11:01:14', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', NULL, NULL, NULL, NULL, '', '', 0, '', '', 900, 1200, '', '', 0, 0, 'local', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapdata` VALUES ('ND301', 0, 'ND3Rpt', 'Start Node', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '2020-02-14 12:28:30', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', NULL, NULL, NULL, NULL, '', '', 0, '', '', 900, 1200, '', '', 0, 0, 'local', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapdata` VALUES ('ND302', 0, 'ND3Rpt', 'Node 2', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '2020-02-14 12:28:30', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', NULL, NULL, NULL, NULL, '', '', 0, '', '', 900, 1200, '', '', 0, 0, 'local', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_mapdata` VALUES ('ND3Rpt', 1, 'ND3Rpt', '流程1', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '2020-02-14 12:28:31', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', NULL, NULL, NULL, NULL, '', '', 0, '', '', 900, 1200, '', '', 0, 0, 'local', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_mapdtl
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapdtl`;
CREATE TABLE `sys_mapdtl`  (
  `No` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Alias` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '别名',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `PTable` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '存储表',
  `GroupField` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组字段',
  `RefPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联的主键',
  `FEBD` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '事件类实体类',
  `Model` int(11) NULL DEFAULT NULL COMMENT '工作模式,枚举类型:0 普通;1 固定行;',
  `DtlVer` int(11) NULL DEFAULT NULL COMMENT '使用版本,枚举类型:0 2017传统版;1 2019EasyUI版本;',
  `RowsOfList` int(11) NULL DEFAULT NULL COMMENT '初始化行数',
  `IsEnableGroupField` int(11) NULL DEFAULT NULL COMMENT '是否启用分组字段',
  `IsShowSum` int(11) NULL DEFAULT NULL COMMENT '是否显示合计？',
  `IsShowIdx` int(11) NULL DEFAULT NULL COMMENT '是否显示序号？',
  `IsCopyNDData` int(11) NULL DEFAULT NULL COMMENT '是否允许copy节点数据',
  `IsHLDtl` int(11) NULL DEFAULT NULL COMMENT '是否是合流汇总',
  `IsReadonly` int(11) NULL DEFAULT NULL COMMENT '是否只读？',
  `IsShowTitle` int(11) NULL DEFAULT NULL COMMENT '是否显示标题？',
  `IsView` int(11) NULL DEFAULT NULL COMMENT '是否可见？',
  `IsInsert` int(11) NULL DEFAULT NULL COMMENT '是否可以插入行？',
  `IsDelete` int(11) NULL DEFAULT NULL COMMENT '是否可以删除行？',
  `IsUpdate` int(11) NULL DEFAULT NULL COMMENT '是否可以更新？',
  `IsEnablePass` int(11) NULL DEFAULT NULL COMMENT '是否启用通过审核功能?',
  `IsEnableAthM` int(11) NULL DEFAULT NULL COMMENT '是否启用多附件',
  `IsEnableM2M` int(11) NULL DEFAULT NULL COMMENT '是否启用M2M',
  `IsEnableM2MM` int(11) NULL DEFAULT NULL COMMENT '是否启用M2M',
  `WhenOverSize` int(11) NULL DEFAULT NULL COMMENT '超出行数,枚举类型:0 不处理;1 向下顺增行;2 次页显示;',
  `DtlOpenType` int(11) NULL DEFAULT NULL COMMENT '数据开放类型,枚举类型:0 操作员;1 工作ID;2 流程ID;',
  `ListShowModel` int(11) NULL DEFAULT NULL COMMENT '列表数据显示格式,枚举类型:0 表格;1 卡片;',
  `EditModel` int(11) NULL DEFAULT NULL COMMENT '编辑数据方式,枚举类型:0 表格;1 卡片;',
  `MobileShowModel` int(11) NULL DEFAULT NULL COMMENT '移动端数据显示方式,枚举类型:0 新页面显示模式;1 列表模式;',
  `MobileShowField` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移动端列表显示字段',
  `X` float NULL DEFAULT NULL COMMENT '距左',
  `Y` float NULL DEFAULT NULL COMMENT '距上',
  `H` float NULL DEFAULT NULL COMMENT '高度',
  `W` float NULL DEFAULT NULL COMMENT '宽度',
  `FrmW` float NULL DEFAULT NULL COMMENT '表单宽度',
  `FrmH` float NULL DEFAULT NULL COMMENT '表单高度',
  `MTR` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请书写html标记,以《TR》开头，以《/TR》结尾。',
  `IsEnableLink` int(11) NULL DEFAULT NULL COMMENT '是否启用超链接',
  `LinkLabel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '超连接标签',
  `LinkTarget` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '连接目标',
  `LinkUrl` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '连接URL',
  `FilterSQLExp` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '过滤数据SQL表达式',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点(用户独立表单权限控制)',
  `ShowCols` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示的列',
  `IsExp` int(11) NULL DEFAULT NULL COMMENT '是否可以导出？(导出到Excel,Txt,html类型文件.)',
  `ImpModel` int(11) NULL DEFAULT NULL COMMENT '导入方式,枚举类型:0 不导入;1 按配置模式导入;2 按照xls文件模版导入;',
  `ImpSQLSearch` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '查询SQL(SQL里必须包含@Key关键字.)',
  `ImpSQLInit` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '初始化SQL(初始化表格的时候的SQL数据,可以为空)',
  `ImpSQLFullOneRow` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '数据填充一行数据的SQL(必须包含@Key关键字,为选择的主键)',
  `ImpSQLNames` varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列的中文名称',
  `ColAutoExp` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列自动计算表达式',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `AtPara` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  `SubThreadWorker` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子线程处理人字段',
  `SubThreadWorkerText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子线程处理人字段',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '明细' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_mapdtl
-- ----------------------------

-- ----------------------------
-- Table structure for sys_mapext
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapext`;
CREATE TABLE `sys_mapext`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主表',
  `ExtType` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `DoWay` int(11) NULL DEFAULT NULL COMMENT '执行方式',
  `AttrOfOper` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作的Attr',
  `AttrsOfActive` varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '激活的字段',
  `Doc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `Tag` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag',
  `Tag1` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag5',
  `H` int(11) NULL DEFAULT NULL COMMENT '高度',
  `W` int(11) NULL DEFAULT NULL COMMENT '宽度',
  `DBType` int(11) NULL DEFAULT NULL COMMENT '数据类型',
  `FK_DBSrc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源',
  `PRI` int(11) NULL DEFAULT NULL COMMENT 'PRI/顺序号',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '参数',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '业务逻辑' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_mapext
-- ----------------------------

-- ----------------------------
-- Table structure for sys_mapframe
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapframe`;
CREATE TABLE `sys_mapframe`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `UrlSrcType` int(11) NULL DEFAULT NULL COMMENT 'URL来源',
  `FrameURL` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FrameURL',
  `URL` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'URL',
  `Y` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Y',
  `X` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'x',
  `W` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '宽度',
  `H` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '高度',
  `IsAutoSize` int(11) NULL DEFAULT NULL COMMENT '是否自动设置大小',
  `EleType` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '框架' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_mapframe
-- ----------------------------
INSERT INTO `sys_mapframe` VALUES ('ND101_Frame1', 'ND101', '我的框架Frame1', 0, 'MapFrameDefPage.htm', 'MapFrameDefPage.htm', '100', '100', '200', '200', 1, '', '', 0);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2023 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, '#', '', 'M', '0', '', 'fa fa-gear', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, '#', '', 'M', '0', '', 'fa fa-video-camera', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统监控目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, '/system/user', '', 'C', '0', 'system:user:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, '/system/role', '', 'C', '0', 'system:role:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, '/system/menu', '', 'C', '0', 'system:menu:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, '/system/dept', '', 'C', '0', 'system:dept:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, '/system/post', '', 'C', '0', 'system:post:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, '/system/dict', '', 'C', '0', 'system:dict:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, '/system/config', '', 'C', '0', 'system:config:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, '/system/notice', '', 'C', '0', 'system:notice:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, '#', '', 'M', '0', '', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, '/monitor/online', '', 'C', '0', 'monitor:online:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, '/monitor/job', '', 'C', '0', 'monitor:job:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, '/monitor/data', '', 'C', '0', 'monitor:data:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 3, '/monitor/server', '', 'C', '0', 'monitor:server:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '服务监控菜单');
INSERT INTO `sys_menu` VALUES (115, '系统接口', 2, 3, '/tool/swagger', 'menuItem', 'C', '0', 'tool:swagger:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2020-02-13 16:37:13', '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, '/monitor/operlog', '', 'C', '0', 'monitor:operlog:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, '/monitor/logininfor', '', 'C', '0', 'monitor:logininfor:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '#', '', 'F', '0', 'system:user:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '#', '', 'F', '0', 'system:user:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '#', '', 'F', '0', 'system:user:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '#', '', 'F', '0', 'system:user:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '#', '', 'F', '0', 'system:user:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '#', '', 'F', '0', 'system:user:import', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '#', '', 'F', '0', 'system:user:resetPwd', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '#', '', 'F', '0', 'system:role:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '#', '', 'F', '0', 'system:role:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '#', '', 'F', '0', 'system:role:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '#', '', 'F', '0', 'system:role:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '#', '', 'F', '0', 'system:role:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '#', '', 'F', '0', 'system:menu:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '#', '', 'F', '0', 'system:menu:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '#', '', 'F', '0', 'system:menu:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '#', '', 'F', '0', 'system:menu:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '#', '', 'F', '0', 'system:dept:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '#', '', 'F', '0', 'system:dept:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '#', '', 'F', '0', 'system:dept:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '#', '', 'F', '0', 'system:dept:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '#', '', 'F', '0', 'system:post:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '#', '', 'F', '0', 'system:post:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '#', '', 'F', '0', 'system:post:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '#', '', 'F', '0', 'system:post:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '#', '', 'F', '0', 'system:post:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', 'F', '0', 'system:dict:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', 'F', '0', 'system:dict:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', 'F', '0', 'system:dict:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', 'F', '0', 'system:dict:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', 'F', '0', 'system:dict:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', 'F', '0', 'system:config:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', 'F', '0', 'system:config:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', 'F', '0', 'system:config:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', 'F', '0', 'system:config:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', 'F', '0', 'system:config:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', 'F', '0', 'system:notice:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', 'F', '0', 'system:notice:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', 'F', '0', 'system:notice:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', 'F', '0', 'system:notice:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', 'F', '0', 'monitor:operlog:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', 'F', '0', 'monitor:operlog:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1041, '详细信息', 500, 3, '#', '', 'F', '0', 'monitor:operlog:detail', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 'F', '0', 'monitor:operlog:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 'F', '0', 'monitor:logininfor:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 'F', '0', 'monitor:logininfor:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 'F', '0', 'monitor:logininfor:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1046, '账户解锁', 501, 4, '#', '', 'F', '0', 'monitor:logininfor:unlock', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1047, '在线查询', 109, 1, '#', '', 'F', '0', 'monitor:online:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1048, '批量强退', 109, 2, '#', '', 'F', '0', 'monitor:online:batchForceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1049, '单条强退', 109, 3, '#', '', 'F', '0', 'monitor:online:forceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1050, '任务查询', 110, 1, '#', '', 'F', '0', 'monitor:job:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1051, '任务新增', 110, 2, '#', '', 'F', '0', 'monitor:job:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1052, '任务修改', 110, 3, '#', '', 'F', '0', 'monitor:job:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1053, '任务删除', 110, 4, '#', '', 'F', '0', 'monitor:job:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1054, '状态修改', 110, 5, '#', '', 'F', '0', 'monitor:job:changeStatus', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1055, '任务详细', 110, 6, '#', '', 'F', '0', 'monitor:job:detail', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1056, '任务导出', 110, 7, '#', '', 'F', '0', 'monitor:job:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (2000, '工作流', 0, 4, '#', 'menuItem', 'M', '0', NULL, 'fa fa-american-sign-language-interpreting', 'admin', '2020-02-13 11:37:55', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2001, '流程设计器', 2000, 1, '/WF/Admin/Portal/Default.htm', 'menuItem', 'C', '0', '', '#', 'admin', '2020-02-13 14:33:45', 'admin', '2020-02-14 12:54:09', '');
INSERT INTO `sys_menu` VALUES (2002, '流程办理', 2000, 2, '#', 'menuItem', 'M', '0', '', '#', 'admin', '2020-02-13 14:37:43', 'admin', '2020-02-14 12:53:49', '');
INSERT INTO `sys_menu` VALUES (2003, '发起', 2002, 1, '/WF/Start.htm', 'menuItem', 'C', '0', '', '#', 'admin', '2020-02-13 14:38:44', 'admin', '2020-02-13 14:39:24', '');
INSERT INTO `sys_menu` VALUES (2004, '待办', 2002, 2, '/WF/Todolist.htm', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-13 14:39:14', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2005, '会签', 2002, 3, '/WF/HuiQianList.htm', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-13 14:40:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2006, '在途', 2002, 4, '/WF/Runing.htm', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-13 14:40:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2007, '共享任务', 2002, 5, '/WF/TaskPoolSharing.htm', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-13 14:40:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2008, '申请下来的任务', 2002, 6, '/WF/TaskPoolApply.htm', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-13 14:41:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2009, '流程查询', 2000, 3, '#', 'menuItem', 'M', '0', NULL, '#', 'admin', '2020-02-14 12:47:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2011, '我发起的', 2009, 1, '/WF/Comm/Search.htm?EnsName=BP.WF.Data.MyStartFlows', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-14 12:49:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2012, '我审批的', 2009, 2, '/WF/Comm/Search.htm?EnsName=BP.WF.Data.MyJoinFlows', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-14 12:49:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2013, '流程分布', 2009, 3, '/WF/RptSearch/DistributedOfMy.htm', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-14 12:50:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2014, '我的流程', 2009, 4, '/WF/Search.htm', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-14 12:51:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2015, '单流程查询', 2009, 5, '/WF/RptDfine/Flowlist.htm', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-14 12:51:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2016, '综合查询', 2009, 6, '/WF/RptSearch/Default.htm', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-14 12:52:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2017, '高级功能', 2000, 4, '#', 'menuItem', 'M', '0', NULL, '#', 'admin', '2020-02-14 12:53:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2018, '我的草稿', 2017, 1, '/WF/Draft.htm', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-14 12:55:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2019, '抄送', 2017, 2, '/WF/CC.htm', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-14 12:55:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2020, '我的关注', 2017, 3, '/WF/Focus.htm', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-14 12:56:11', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2021, '授权待办', 2017, 4, '/WF/TodolistOfAuth.htm', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-14 12:56:40', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2022, '挂起的工作', 2017, 5, '/WF/HungUpList.htm', 'menuItem', 'C', '0', NULL, '#', 'admin', '2020-02-14 12:57:07', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', '新版本内容', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', '维护内容', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 167 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (100, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"工作流\" ],\r\n  \"url\" : [ \"\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"4\" ],\r\n  \"icon\" : [ \"fa fa-american-sign-language-interpreting\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 11:37:55');
INSERT INTO `sys_oper_log` VALUES (101, '重置密码', 2, 'cn.risesoft.web.controller.system.SysProfileController.resetPwd()', 'POST', 1, 'admin', '研发部门', '/ys/system/user/profile/resetPwd', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"1\" ],\r\n  \"loginName\" : [ \"admin\" ],\r\n  \"oldPassword\" : [ \"admin123\" ],\r\n  \"newPassword\" : [ \"666666\" ],\r\n  \"confirm\" : [ \"666666\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 14:27:38');
INSERT INTO `sys_oper_log` VALUES (102, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"流程设计器\" ],\r\n  \"url\" : [ \"//WF/Admin/CCBPMDesigner/Default.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"fa fa-american-sign-language-interpreting\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 14:33:45');
INSERT INTO `sys_oper_log` VALUES (103, '菜单管理', 2, 'cn.risesoft.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2001\" ],\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"流程设计器\" ],\r\n  \"url\" : [ \"/WF/Admin/CCBPMDesigner/Default.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"fa fa-american-sign-language-interpreting\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 14:34:44');
INSERT INTO `sys_oper_log` VALUES (104, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"流程办理\" ],\r\n  \"url\" : [ \"\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"icon\" : [ \"fa fa-american-sign-language-interpreting\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 14:37:43');
INSERT INTO `sys_oper_log` VALUES (105, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2002\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"发起流程\" ],\r\n  \"url\" : [ \"/WF/Start.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 14:38:44');
INSERT INTO `sys_oper_log` VALUES (106, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2002\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"待办\" ],\r\n  \"url\" : [ \"/WF/Todolist.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 14:39:14');
INSERT INTO `sys_oper_log` VALUES (107, '菜单管理', 2, 'cn.risesoft.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2003\" ],\r\n  \"parentId\" : [ \"2002\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"发起\" ],\r\n  \"url\" : [ \"/WF/Start.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"#\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 14:39:24');
INSERT INTO `sys_oper_log` VALUES (108, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2002\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"会签\" ],\r\n  \"url\" : [ \"/WF/HuiQianList.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 14:40:04');
INSERT INTO `sys_oper_log` VALUES (109, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2002\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"在途\" ],\r\n  \"url\" : [ \"/WF/Runing.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"4\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 14:40:26');
INSERT INTO `sys_oper_log` VALUES (110, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2002\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"共享任务\" ],\r\n  \"url\" : [ \"/WF/TaskPoolSharing.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"5\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 14:40:58');
INSERT INTO `sys_oper_log` VALUES (111, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2002\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"申请下来的任务\" ],\r\n  \"url\" : [ \"/WF/TaskPoolApply.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"6\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 14:41:26');
INSERT INTO `sys_oper_log` VALUES (112, '部门管理', 2, 'cn.risesoft.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"100\" ],\r\n  \"parentId\" : [ \"0\" ],\r\n  \"parentName\" : [ \"无\" ],\r\n  \"deptName\" : [ \"组织架构\" ],\r\n  \"orderNum\" : [ \"0\" ],\r\n  \"leader\" : [ \"admin\" ],\r\n  \"phone\" : [ \"18888888888\" ],\r\n  \"email\" : [ \"888888@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:06:35');
INSERT INTO `sys_oper_log` VALUES (113, '部门管理', 2, 'cn.risesoft.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"101\" ],\r\n  \"parentId\" : [ \"100\" ],\r\n  \"parentName\" : [ \"组织架构\" ],\r\n  \"deptName\" : [ \"绵阳市人大常委会\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"leader\" : [ \"\" ],\r\n  \"phone\" : [ \"\" ],\r\n  \"email\" : [ \"\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:06:52');
INSERT INTO `sys_oper_log` VALUES (114, '部门管理', 2, 'cn.risesoft.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"101\" ],\r\n  \"parentId\" : [ \"100\" ],\r\n  \"parentName\" : [ \"组织架构\" ],\r\n  \"deptName\" : [ \"绵阳投资集团\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"leader\" : [ \"\" ],\r\n  \"phone\" : [ \"\" ],\r\n  \"email\" : [ \"\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:07:22');
INSERT INTO `sys_oper_log` VALUES (115, '部门管理', 3, 'cn.risesoft.web.controller.system.SysDeptController.remove()', 'GET', 1, 'admin', '研发部门', '/ys/system/dept/remove/108', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:07:27');
INSERT INTO `sys_oper_log` VALUES (116, '部门管理', 3, 'cn.risesoft.web.controller.system.SysDeptController.remove()', 'GET', 1, 'admin', '研发部门', '/ys/system/dept/remove/109', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:07:29');
INSERT INTO `sys_oper_log` VALUES (117, '部门管理', 3, 'cn.risesoft.web.controller.system.SysDeptController.remove()', 'GET', 1, 'admin', '研发部门', '/ys/system/dept/remove/102', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:07:31');
INSERT INTO `sys_oper_log` VALUES (118, '岗位管理', 1, 'cn.risesoft.web.controller.system.SysPostController.addSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/post/add', '127.0.0.1', '内网IP', '{\r\n  \"postName\" : [ \"总经理\" ],\r\n  \"postCode\" : [ \"总经理\" ],\r\n  \"postSort\" : [ \"5\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:08:28');
INSERT INTO `sys_oper_log` VALUES (119, '岗位管理', 1, 'cn.risesoft.web.controller.system.SysPostController.addSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/post/add', '127.0.0.1', '内网IP', '{\r\n  \"postName\" : [ \"市场部经理\" ],\r\n  \"postCode\" : [ \"市场部经理\" ],\r\n  \"postSort\" : [ \"6\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:09:27');
INSERT INTO `sys_oper_log` VALUES (120, '角色管理', 2, 'cn.risesoft.web.controller.system.SysRoleController.authDataScopeSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/role/authDataScope', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"1\" ],\r\n  \"roleName\" : [ \"管理员\" ],\r\n  \"roleKey\" : [ \"admin\" ],\r\n  \"dataScope\" : [ \"1\" ],\r\n  \"deptIds\" : [ \"\" ]\r\n}', 'null', 1, '不允许操作超级管理员角色', '2020-02-13 16:10:30');
INSERT INTO `sys_oper_log` VALUES (121, '角色管理', 2, 'cn.risesoft.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"1\" ],\r\n  \"roleName\" : [ \"管理员\" ],\r\n  \"roleKey\" : [ \"admin\" ],\r\n  \"roleSort\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"menuIds\" : [ \"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,1046,2,109,1047,1048,1049,110,1050,1051,1052,1053,1054,1055,1056,111,112,3,113,114,1057,1058,1059,1060,1061,115,2000,2001,2002,2003,2004,2005,2006,2007,2008\" ]\r\n}', 'null', 1, '不允许操作超级管理员角色', '2020-02-13 16:10:41');
INSERT INTO `sys_oper_log` VALUES (122, '菜单管理', 2, 'cn.risesoft.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"115\" ],\r\n  \"parentId\" : [ \"2\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"系统接口\" ],\r\n  \"url\" : [ \"/tool/swagger\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"tool:swagger:view\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"icon\" : [ \"#\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:37:13');
INSERT INTO `sys_oper_log` VALUES (123, '菜单管理', 3, 'cn.risesoft.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '研发部门', '/ys/system/menu/remove/113', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"菜单已分配,不允许删除\",\r\n  \"code\" : 301\r\n}', 0, NULL, '2020-02-13 16:37:17');
INSERT INTO `sys_oper_log` VALUES (124, '角色管理', 2, 'cn.risesoft.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"roleName\" : [ \"普通角色\" ],\r\n  \"roleKey\" : [ \"common\" ],\r\n  \"roleSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"普通角色\" ],\r\n  \"menuIds\" : [ \"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,1046,2,109,1047,1048,1049,110,1050,1051,1052,1053,1054,1055,1056,111,112,115,2000,2001,2002,2003,2004,2005,2006,2007,2008\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:37:28');
INSERT INTO `sys_oper_log` VALUES (125, '菜单管理', 3, 'cn.risesoft.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '研发部门', '/ys/system/menu/remove/113', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:37:41');
INSERT INTO `sys_oper_log` VALUES (126, '菜单管理', 3, 'cn.risesoft.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '研发部门', '/ys/system/menu/remove/1061', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:37:47');
INSERT INTO `sys_oper_log` VALUES (127, '菜单管理', 3, 'cn.risesoft.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '研发部门', '/ys/system/menu/remove/1060', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:37:52');
INSERT INTO `sys_oper_log` VALUES (128, '菜单管理', 3, 'cn.risesoft.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '研发部门', '/ys/system/menu/remove/1059', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:37:57');
INSERT INTO `sys_oper_log` VALUES (129, '菜单管理', 3, 'cn.risesoft.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '研发部门', '/ys/system/menu/remove/1058', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:38:02');
INSERT INTO `sys_oper_log` VALUES (130, '菜单管理', 3, 'cn.risesoft.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '研发部门', '/ys/system/menu/remove/1057', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:38:07');
INSERT INTO `sys_oper_log` VALUES (131, '菜单管理', 3, 'cn.risesoft.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '研发部门', '/ys/system/menu/remove/114', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:38:10');
INSERT INTO `sys_oper_log` VALUES (132, '菜单管理', 3, 'cn.risesoft.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '研发部门', '/ys/system/menu/remove/3', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:38:14');
INSERT INTO `sys_oper_log` VALUES (133, '重置密码', 2, 'cn.risesoft.web.controller.system.SysUserController.resetPwd()', 'GET', 1, 'admin', '研发部门', '/ys/system/user/resetPwd/2', '127.0.0.1', '内网IP', '{ }', '\"system/user/resetPwd\"', 0, NULL, '2020-02-13 16:53:16');
INSERT INTO `sys_oper_log` VALUES (134, '重置密码', 2, 'cn.risesoft.web.controller.system.SysUserController.resetPwdSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/user/resetPwd', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"2\" ],\r\n  \"loginName\" : [ \"zaoyun\" ],\r\n  \"password\" : [ \"666666\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:53:22');
INSERT INTO `sys_oper_log` VALUES (135, '用户管理', 2, 'cn.risesoft.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"1\" ],\r\n  \"deptId\" : [ \"100\" ],\r\n  \"userName\" : [ \"超级管理员\" ],\r\n  \"dept.deptName\" : [ \"组织架构\" ],\r\n  \"phonenumber\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@163.com\" ],\r\n  \"loginName\" : [ \"admin\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"role\" : [ \"1\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"1,3,4,6\" ]\r\n}', 'null', 1, '不允许操作超级管理员用户', '2020-02-13 16:53:39');
INSERT INTO `sys_oper_log` VALUES (136, '用户管理', 2, 'cn.risesoft.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"2\" ],\r\n  \"deptId\" : [ \"105\" ],\r\n  \"userName\" : [ \"赵云\" ],\r\n  \"dept.deptName\" : [ \"测试部门\" ],\r\n  \"phonenumber\" : [ \"15666666666\" ],\r\n  \"email\" : [ \"2434234234@qq.com\" ],\r\n  \"loginName\" : [ \"zaoyun\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"role\" : [ \"1\", \"2\" ],\r\n  \"remark\" : [ \"测试员\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1,2\" ],\r\n  \"postIds\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:53:54');
INSERT INTO `sys_oper_log` VALUES (137, '部门管理', 1, 'cn.risesoft.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/dept/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"100\" ],\r\n  \"deptName\" : [ \"绵阳卷烟厂\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"leader\" : [ \"无\" ],\r\n  \"phone\" : [ \"13600000000\" ],\r\n  \"email\" : [ \"\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:56:07');
INSERT INTO `sys_oper_log` VALUES (138, '用户管理', 2, 'cn.risesoft.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '研发部门', '/ys/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"2\" ],\r\n  \"deptId\" : [ \"107\" ],\r\n  \"userName\" : [ \"赵云\" ],\r\n  \"dept.deptName\" : [ \"运维部门\" ],\r\n  \"phonenumber\" : [ \"15666666666\" ],\r\n  \"email\" : [ \"2434234234@qq.com\" ],\r\n  \"loginName\" : [ \"zaoyun\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"role\" : [ \"1\", \"2\" ],\r\n  \"remark\" : [ \"测试员\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1,2\" ],\r\n  \"postIds\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-13 16:58:21');
INSERT INTO `sys_oper_log` VALUES (139, '角色管理', 1, 'cn.risesoft.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/role/add', '127.0.0.1', '内网IP', '{\r\n  \"roleName\" : [ \"操作员\" ],\r\n  \"roleKey\" : [ \"caozuoyuan\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"无\" ],\r\n  \"menuIds\" : [ \"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,1046,2,109,1047,1048,1049,110,1050,1051,1052,1053,1054,1055,1056,111,112,115,2000,2001,2002,2003,2004,2005,2006,2007,2008\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 10:13:34');
INSERT INTO `sys_oper_log` VALUES (140, '个人信息', 2, 'cn.risesoft.web.controller.system.SysProfileController.updateAvatar()', 'POST', 1, 'admin', '组织架构', '/ys/system/user/profile/updateAvatar', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 11:13:03');
INSERT INTO `sys_oper_log` VALUES (141, '个人信息', 2, 'cn.risesoft.web.controller.system.SysProfileController.update()', 'POST', 1, 'admin', '组织架构', '/ys/system/user/profile/update', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"\" ],\r\n  \"userName\" : [ \"超级管理员\" ],\r\n  \"phonenumber\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@163.com\" ],\r\n  \"sex\" : [ \"1\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 11:13:07');
INSERT INTO `sys_oper_log` VALUES (142, '菜单管理', 2, 'cn.risesoft.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2001\" ],\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"流程设计器\" ],\r\n  \"url\" : [ \"/WF/Admin/Portal/Default.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"fa fa-american-sign-language-interpreting\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:28:16');
INSERT INTO `sys_oper_log` VALUES (143, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"流程查询\" ],\r\n  \"url\" : [ \"\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:47:46');
INSERT INTO `sys_oper_log` VALUES (144, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"高级功能\" ],\r\n  \"url\" : [ \"\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"4\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:48:18');
INSERT INTO `sys_oper_log` VALUES (145, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2009\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"我发起的\" ],\r\n  \"url\" : [ \"/WF/Comm/Search.htm?EnsName=BP.WF.Data.MyStartFlows\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:49:06');
INSERT INTO `sys_oper_log` VALUES (146, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2009\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"我审批的\" ],\r\n  \"url\" : [ \"/WF/Comm/Search.htm?EnsName=BP.WF.Data.MyJoinFlows\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:49:48');
INSERT INTO `sys_oper_log` VALUES (147, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2009\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"流程分布\" ],\r\n  \"url\" : [ \"/WF/RptSearch/DistributedOfMy.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:50:28');
INSERT INTO `sys_oper_log` VALUES (148, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2009\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"我的流程\" ],\r\n  \"url\" : [ \"/WF/Search.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"4\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:51:04');
INSERT INTO `sys_oper_log` VALUES (149, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2009\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"单流程查询\" ],\r\n  \"url\" : [ \"/WF/RptDfine/Flowlist.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"5\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:51:42');
INSERT INTO `sys_oper_log` VALUES (150, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2009\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"综合查询\" ],\r\n  \"url\" : [ \"/WF/RptSearch/Default.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"6\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:52:20');
INSERT INTO `sys_oper_log` VALUES (151, '菜单管理', 3, 'cn.risesoft.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '组织架构', '/ys/system/menu/remove/2010', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:53:01');
INSERT INTO `sys_oper_log` VALUES (152, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"高级功能\" ],\r\n  \"url\" : [ \"\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"4\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:53:20');
INSERT INTO `sys_oper_log` VALUES (153, '菜单管理', 2, 'cn.risesoft.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2001\" ],\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"流程设计器\" ],\r\n  \"url\" : [ \"/WF/Admin/Portal/Default.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:53:33');
INSERT INTO `sys_oper_log` VALUES (154, '菜单管理', 2, 'cn.risesoft.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2002\" ],\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"流程办理\" ],\r\n  \"url\" : [ \"#\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:53:49');
INSERT INTO `sys_oper_log` VALUES (155, '菜单管理', 2, 'cn.risesoft.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2001\" ],\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"流程设计器\" ],\r\n  \"url\" : [ \"/WF/Admin/Portal/Default.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \" \" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:54:09');
INSERT INTO `sys_oper_log` VALUES (156, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2017\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"我的草稿\" ],\r\n  \"url\" : [ \"/WF/Draft.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:55:29');
INSERT INTO `sys_oper_log` VALUES (157, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2017\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"抄送\" ],\r\n  \"url\" : [ \"/WF/CC.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:55:49');
INSERT INTO `sys_oper_log` VALUES (158, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2017\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"我的关注\" ],\r\n  \"url\" : [ \"/WF/Focus.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:56:11');
INSERT INTO `sys_oper_log` VALUES (159, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2017\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"授权待办\" ],\r\n  \"url\" : [ \"/WF/TodolistOfAuth.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"4\" ],\r\n  \"icon\" : [ \"#\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:56:40');
INSERT INTO `sys_oper_log` VALUES (160, '菜单管理', 1, 'cn.risesoft.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2017\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"挂起的工作\" ],\r\n  \"url\" : [ \"/WF/HungUpList.htm\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"5\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 12:57:07');
INSERT INTO `sys_oper_log` VALUES (161, '用户管理', 1, 'cn.risesoft.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', '组织架构', '/ys/system/user/add', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"107\" ],\r\n  \"userName\" : [ \"李儒\" ],\r\n  \"deptName\" : [ \"运维部门\" ],\r\n  \"phonenumber\" : [ \"18009093726\" ],\r\n  \"email\" : [ \"kikock@qq.com\" ],\r\n  \"loginName\" : [ \"liru\" ],\r\n  \"password\" : [ \"666666\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"1\", \"2\", \"100\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1,2,100\" ],\r\n  \"postIds\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 13:44:09');
INSERT INTO `sys_oper_log` VALUES (162, '重置密码', 2, 'cn.risesoft.web.controller.system.SysUserController.resetPwd()', 'GET', 1, 'admin', '组织架构', '/ys/system/user/resetPwd/100', '127.0.0.1', '内网IP', '{ }', '\"system/user/resetPwd\"', 0, NULL, '2020-02-14 14:43:20');
INSERT INTO `sys_oper_log` VALUES (163, '角色管理', 4, 'cn.risesoft.web.controller.system.SysRoleController.cancelAuthUser()', 'POST', 1, 'admin', '组织架构', '/ys/system/role/authUser/cancel', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"1\" ],\r\n  \"userId\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 14:47:07');
INSERT INTO `sys_oper_log` VALUES (164, '角色管理', 4, 'cn.risesoft.web.controller.system.SysRoleController.cancelAuthUser()', 'POST', 1, 'admin', '组织架构', '/ys/system/role/authUser/cancel', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"1\" ],\r\n  \"userId\" : [ \"100\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 14:47:09');
INSERT INTO `sys_oper_log` VALUES (165, '角色管理', 4, 'cn.risesoft.web.controller.system.SysRoleController.selectAuthUserAll()', 'POST', 1, 'admin', '组织架构', '/ys/system/role/authUser/selectAll', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"1\" ],\r\n  \"userIds\" : [ \"100,2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 14:47:14');
INSERT INTO `sys_oper_log` VALUES (166, '登陆日志', 9, 'cn.risesoft.web.controller.monitor.SysLogininforController.clean()', 'POST', 1, 'admin', '组织架构', '/ys/monitor/logininfor/clean', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-02-14 15:10:20');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES (5, '总经理', '总经理', 5, '0', 'admin', '2020-02-13 16:08:28', '', NULL, NULL);
INSERT INTO `sys_post` VALUES (6, '市场部经理', '市场部经理', 6, '0', 'admin', '2020-02-13 16:09:27', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', 'admin', 1, '1', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-02-13 16:37:28', '普通角色');
INSERT INTO `sys_role` VALUES (100, '操作员', 'caozuoyuan', 3, '1', '0', '0', 'admin', '2020-02-14 10:13:34', '', NULL, '无');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 2000);
INSERT INTO `sys_role_menu` VALUES (2, 2001);
INSERT INTO `sys_role_menu` VALUES (2, 2002);
INSERT INTO `sys_role_menu` VALUES (2, 2003);
INSERT INTO `sys_role_menu` VALUES (2, 2004);
INSERT INTO `sys_role_menu` VALUES (2, 2005);
INSERT INTO `sys_role_menu` VALUES (2, 2006);
INSERT INTO `sys_role_menu` VALUES (2, 2007);
INSERT INTO `sys_role_menu` VALUES (2, 2008);
INSERT INTO `sys_role_menu` VALUES (100, 1);
INSERT INTO `sys_role_menu` VALUES (100, 2);
INSERT INTO `sys_role_menu` VALUES (100, 100);
INSERT INTO `sys_role_menu` VALUES (100, 101);
INSERT INTO `sys_role_menu` VALUES (100, 102);
INSERT INTO `sys_role_menu` VALUES (100, 103);
INSERT INTO `sys_role_menu` VALUES (100, 104);
INSERT INTO `sys_role_menu` VALUES (100, 105);
INSERT INTO `sys_role_menu` VALUES (100, 106);
INSERT INTO `sys_role_menu` VALUES (100, 107);
INSERT INTO `sys_role_menu` VALUES (100, 108);
INSERT INTO `sys_role_menu` VALUES (100, 109);
INSERT INTO `sys_role_menu` VALUES (100, 110);
INSERT INTO `sys_role_menu` VALUES (100, 111);
INSERT INTO `sys_role_menu` VALUES (100, 112);
INSERT INTO `sys_role_menu` VALUES (100, 115);
INSERT INTO `sys_role_menu` VALUES (100, 500);
INSERT INTO `sys_role_menu` VALUES (100, 501);
INSERT INTO `sys_role_menu` VALUES (100, 1000);
INSERT INTO `sys_role_menu` VALUES (100, 1001);
INSERT INTO `sys_role_menu` VALUES (100, 1002);
INSERT INTO `sys_role_menu` VALUES (100, 1003);
INSERT INTO `sys_role_menu` VALUES (100, 1004);
INSERT INTO `sys_role_menu` VALUES (100, 1005);
INSERT INTO `sys_role_menu` VALUES (100, 1006);
INSERT INTO `sys_role_menu` VALUES (100, 1007);
INSERT INTO `sys_role_menu` VALUES (100, 1008);
INSERT INTO `sys_role_menu` VALUES (100, 1009);
INSERT INTO `sys_role_menu` VALUES (100, 1010);
INSERT INTO `sys_role_menu` VALUES (100, 1011);
INSERT INTO `sys_role_menu` VALUES (100, 1012);
INSERT INTO `sys_role_menu` VALUES (100, 1013);
INSERT INTO `sys_role_menu` VALUES (100, 1014);
INSERT INTO `sys_role_menu` VALUES (100, 1015);
INSERT INTO `sys_role_menu` VALUES (100, 1016);
INSERT INTO `sys_role_menu` VALUES (100, 1017);
INSERT INTO `sys_role_menu` VALUES (100, 1018);
INSERT INTO `sys_role_menu` VALUES (100, 1019);
INSERT INTO `sys_role_menu` VALUES (100, 1020);
INSERT INTO `sys_role_menu` VALUES (100, 1021);
INSERT INTO `sys_role_menu` VALUES (100, 1022);
INSERT INTO `sys_role_menu` VALUES (100, 1023);
INSERT INTO `sys_role_menu` VALUES (100, 1024);
INSERT INTO `sys_role_menu` VALUES (100, 1025);
INSERT INTO `sys_role_menu` VALUES (100, 1026);
INSERT INTO `sys_role_menu` VALUES (100, 1027);
INSERT INTO `sys_role_menu` VALUES (100, 1028);
INSERT INTO `sys_role_menu` VALUES (100, 1029);
INSERT INTO `sys_role_menu` VALUES (100, 1030);
INSERT INTO `sys_role_menu` VALUES (100, 1031);
INSERT INTO `sys_role_menu` VALUES (100, 1032);
INSERT INTO `sys_role_menu` VALUES (100, 1033);
INSERT INTO `sys_role_menu` VALUES (100, 1034);
INSERT INTO `sys_role_menu` VALUES (100, 1035);
INSERT INTO `sys_role_menu` VALUES (100, 1036);
INSERT INTO `sys_role_menu` VALUES (100, 1037);
INSERT INTO `sys_role_menu` VALUES (100, 1038);
INSERT INTO `sys_role_menu` VALUES (100, 1039);
INSERT INTO `sys_role_menu` VALUES (100, 1040);
INSERT INTO `sys_role_menu` VALUES (100, 1041);
INSERT INTO `sys_role_menu` VALUES (100, 1042);
INSERT INTO `sys_role_menu` VALUES (100, 1043);
INSERT INTO `sys_role_menu` VALUES (100, 1044);
INSERT INTO `sys_role_menu` VALUES (100, 1045);
INSERT INTO `sys_role_menu` VALUES (100, 1046);
INSERT INTO `sys_role_menu` VALUES (100, 1047);
INSERT INTO `sys_role_menu` VALUES (100, 1048);
INSERT INTO `sys_role_menu` VALUES (100, 1049);
INSERT INTO `sys_role_menu` VALUES (100, 1050);
INSERT INTO `sys_role_menu` VALUES (100, 1051);
INSERT INTO `sys_role_menu` VALUES (100, 1052);
INSERT INTO `sys_role_menu` VALUES (100, 1053);
INSERT INTO `sys_role_menu` VALUES (100, 1054);
INSERT INTO `sys_role_menu` VALUES (100, 1055);
INSERT INTO `sys_role_menu` VALUES (100, 1056);
INSERT INTO `sys_role_menu` VALUES (100, 2000);
INSERT INTO `sys_role_menu` VALUES (100, 2001);
INSERT INTO `sys_role_menu` VALUES (100, 2002);
INSERT INTO `sys_role_menu` VALUES (100, 2003);
INSERT INTO `sys_role_menu` VALUES (100, 2004);
INSERT INTO `sys_role_menu` VALUES (100, 2005);
INSERT INTO `sys_role_menu` VALUES (100, 2006);
INSERT INTO `sys_role_menu` VALUES (100, 2007);
INSERT INTO `sys_role_menu` VALUES (100, 2008);

-- ----------------------------
-- Table structure for sys_rptdept
-- ----------------------------
DROP TABLE IF EXISTS `sys_rptdept`;
CREATE TABLE `sys_rptdept`  (
  `FK_Rpt` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '报表 - 主键',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Rpt`, `FK_Dept`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '报表部门对应信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_rptdept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_rptemp
-- ----------------------------
DROP TABLE IF EXISTS `sys_rptemp`;
CREATE TABLE `sys_rptemp`  (
  `FK_Rpt` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '报表 - 主键',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Rpt`, `FK_Emp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '报表人员对应信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_rptemp
-- ----------------------------

-- ----------------------------
-- Table structure for sys_rptstation
-- ----------------------------
DROP TABLE IF EXISTS `sys_rptstation`;
CREATE TABLE `sys_rptstation`  (
  `FK_Rpt` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '报表 - 主键',
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Rpt`, `FK_Station`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '报表岗位对应信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_rptstation
-- ----------------------------

-- ----------------------------
-- Table structure for sys_rpttemplate
-- ----------------------------
DROP TABLE IF EXISTS `sys_rpttemplate`;
CREATE TABLE `sys_rpttemplate`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `EnsName` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类名',
  `FK_Emp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作员',
  `D1` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'D1',
  `D2` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'D2',
  `D3` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'D3',
  `AlObjs` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '要分析的对象',
  `Height` int(11) NULL DEFAULT NULL COMMENT 'Height',
  `Width` int(11) NULL DEFAULT NULL COMMENT 'Width',
  `IsSumBig` int(11) NULL DEFAULT NULL COMMENT '是否显示大合计',
  `IsSumLittle` int(11) NULL DEFAULT NULL COMMENT '是否显示小合计',
  `IsSumRight` int(11) NULL DEFAULT NULL COMMENT '是否显示右合计',
  `PercentModel` int(11) NULL DEFAULT NULL COMMENT '比率显示方式',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '报表模板' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_rpttemplate
-- ----------------------------

-- ----------------------------
-- Table structure for sys_serial
-- ----------------------------
DROP TABLE IF EXISTS `sys_serial`;
CREATE TABLE `sys_serial`  (
  `CfgKey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'CfgKey - 主键',
  `IntVal` int(11) NULL DEFAULT NULL COMMENT '属性',
  PRIMARY KEY (`CfgKey`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '序列号' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_serial
-- ----------------------------
INSERT INTO `sys_serial` VALUES ('BP.WF.Template.FlowSort', 102);
INSERT INTO `sys_serial` VALUES ('OID', 144);
INSERT INTO `sys_serial` VALUES ('UpdataCCFlowVer', 214102206);
INSERT INTO `sys_serial` VALUES ('Ver', 20191015);
INSERT INTO `sys_serial` VALUES ('WorkID', 104);

-- ----------------------------
-- Table structure for sys_sfdbsrc
-- ----------------------------
DROP TABLE IF EXISTS `sys_sfdbsrc`;
CREATE TABLE `sys_sfdbsrc`  (
  `No` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据源编号(必须是英文) - 主键',
  `Name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源名称',
  `DBSrcType` int(11) NULL DEFAULT NULL COMMENT '数据源类型,枚举类型:0 应用系统主数据库(默认);1 SQLServer数据库;2 Oracle数据库;3 MySQL数据库;4 Informix数据库;50 Dubbo服务;100 WebService数据源;',
  `UserID` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据库登录用户ID',
  `Password` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据库登录用户密码',
  `IP` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'IP地址/数据库实例名',
  `DBName` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据库名称/Oracle保持为空',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据源' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_sfdbsrc
-- ----------------------------
INSERT INTO `sys_sfdbsrc` VALUES ('local', '本机数据源(默认)', 0, '', '', '', '');

-- ----------------------------
-- Table structure for sys_sftable
-- ----------------------------
DROP TABLE IF EXISTS `sys_sftable`;
CREATE TABLE `sys_sftable`  (
  `No` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表英文名称 - 主键',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表中文名称',
  `SrcType` int(11) NULL DEFAULT NULL COMMENT '数据表类型,枚举类型:0 本地的类;1 创建表;2 表或视图;3 SQL查询表;4 WebServices;5 微服务Handler外部数据源;6 JavaScript外部数据源;7 动态Json;',
  `CodeStruct` int(11) NULL DEFAULT NULL COMMENT '字典表类型,枚举类型:0 普通的编码表(具有No,Name);1 树结构(具有No,Name,ParentNo);2 行政机构编码表(编码以两位编号标识级次树形关系);',
  `FK_Val` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认创建的字段名',
  `TableDesc` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表描述',
  `DefVal` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认值',
  `FK_SFDBSrc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源,外键:对应物理表:Sys_SFDBSrc,表描述:数据源',
  `SrcTable` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源表',
  `ColumnValue` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示的值(编号列)',
  `ColumnText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示的文字(名称列)',
  `ParentValue` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父级值(父级列)',
  `SelectStatement` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '查询语句',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '加入日期',
  `RootVal` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '根节点值',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_sftable
-- ----------------------------

-- ----------------------------
-- Table structure for sys_sms
-- ----------------------------
DROP TABLE IF EXISTS `sys_sms`;
CREATE TABLE `sys_sms`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Sender` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送人(可以为空)',
  `SendTo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送给(可以为空)',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '写入时间',
  `Mobile` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号(可以为空)',
  `MobileSta` int(11) NULL DEFAULT NULL COMMENT '消息状态',
  `MobileInfo` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '短信信息',
  `Email` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Email(可以为空)',
  `EmailSta` int(11) NULL DEFAULT NULL COMMENT 'EmaiSta消息状态',
  `EmailTitle` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `EmailDoc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `SendDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送时间',
  `IsRead` int(11) NULL DEFAULT NULL COMMENT '是否读取?',
  `IsAlert` int(11) NULL DEFAULT NULL COMMENT '是否提示?',
  `MsgFlag` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息标记(用于防止发送重复)',
  `MsgType` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息类型(CC抄送,Todolist待办,Return退回,Etc其他消息...)',
  `AtPara` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_sms
-- ----------------------------
INSERT INTO `sys_sms` VALUES ('58e68023c24c47249f97065199bba02e', 'admin', 'admin', '2020-02-13 18:39', '', 0, '工作{研发部门-admin,若依在2020-02-13 16:04发起.}被退回,退回人:admin, 超级管理员,打开http://localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID={EmpStr}_101_102_2020-02-13 18:39 .', 'zhoupeng@ccflow.org', 0, '被退回来{研发部门-admin,若依在2020-02-13 16:04发起.},退回人admin,超级管理员', '工作{研发部门-admin,若依在2020-02-13 16:04发起.}被退回,退回人:admin, 超级管理员,打开http://localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID={EmpStr}_101_102_2020-02-13 18:39 .', '', 0, 0, 'WKAlt102_101', 'ReturnAfter', '@OpenUrl=http://localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID=admin_101_102_2020-02-13 18:39@PushModel=');
INSERT INTO `sys_sms` VALUES ('@FK_Flow=001&FK_Node=102@WorkID=102', 'admin', 'zaoyun', '2020-02-13 18:44', '15666666666', 0, '有新工作{组织架构-admin,超级管理员在2020-02-13 18:44发起.}需要您处理, 发送人:admin, 超级管理员,打开http://localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID={EmpStr}_102_102_2020-02-13 18:44 .', '', 0, '新工作{组织架构-admin,超级管理员在2020-02-13 18:44发起.},发送人admin,超级管理员', '有新工作{组织架构-admin,超级管理员在2020-02-13 18:44发起.}需要您处理, 发送人:admin, 超级管理员,打开http://localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID={EmpStr}_102_102_2020-02-13 18:44 .', '', 0, 0, 'WKAlt102_102', 'SendSuccess', '@OpenUrl=http://localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID=zaoyun_102_102_2020-02-13 18:44@PushModel=');
INSERT INTO `sys_sms` VALUES ('@FK_Flow=001&FK_Node=102@WorkID=103', 'admin', 'zaoyun', '2020-02-13 18:50', '15666666666', 0, '有新工作{组织架构-admin,超级管理员在2020-02-13 18:49发起.}需要您处理, 发送人:admin, 超级管理员,打开http://localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID={EmpStr}_103_102_2020-02-13 18:50 .', '', 0, '新工作{组织架构-admin,超级管理员在2020-02-13 18:49发起.},发送人admin,超级管理员', '有新工作{组织架构-admin,超级管理员在2020-02-13 18:49发起.}需要您处理, 发送人:admin, 超级管理员,打开http://localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID={EmpStr}_103_102_2020-02-13 18:50 .', '', 0, 0, 'WKAlt102_103', 'SendSuccess', '@OpenUrl=http://localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID=zaoyun_103_102_2020-02-13 18:50@PushModel=');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '盐加密',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '最后登陆IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登陆时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 100, 'admin', '超级管理员', '00', 'ry@163.com', '15888888888', '1', '/profile/avatar/2020/02/14/b62b5330c737bd79f27093366938f256.png', 'cbbadf9a4db9841f0a28a8bdfbd7b1aa', '41ad30', '0', '0', '127.0.0.1', '2020-02-14 15:40:22', 'admin', '2018-03-16 11:33:00', 'ry', '2020-02-14 15:40:22', '管理员');
INSERT INTO `sys_user` VALUES (2, 107, 'zaoyun', '赵云', '00', '2434234234@qq.com', '15666666666', '1', '', '38d52304f13990bfddb944e2625679ed', '625b12', '0', '0', '127.0.0.1', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', 'admin', '2020-02-13 16:58:21', '测试员');
INSERT INTO `sys_user` VALUES (100, 107, 'liru', '李儒', '00', 'kikock@qq.com', '18009093726', '0', '', '55f7e8eb55a61eea5d0f157ba951935d', 'f2c4d6', '0', '0', '', NULL, 'admin', '2020-02-14 13:44:09', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online`  (
  `sessionId` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime(0) NULL DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime(0) NULL DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) NULL DEFAULT 0 COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '在线用户记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------
INSERT INTO `sys_user_online` VALUES ('3d34af33-d427-4a1b-90ea-3f5ec561cfbb', 'admin', '组织架构', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', 'on_line', '2020-02-14 15:36:12', '2020-02-14 15:40:20', 1800000);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);
INSERT INTO `sys_user_post` VALUES (100, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (100, 1);
INSERT INTO `sys_user_role` VALUES (100, 2);
INSERT INTO `sys_user_role` VALUES (100, 100);

-- ----------------------------
-- Table structure for sys_userlogt
-- ----------------------------
DROP TABLE IF EXISTS `sys_userlogt`;
CREATE TABLE `sys_userlogt`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Emp` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户',
  `IP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'IP',
  `LogFlag` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标识',
  `Docs` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '说明',
  `RDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_userlogt
-- ----------------------------

-- ----------------------------
-- Table structure for sys_userregedit
-- ----------------------------
DROP TABLE IF EXISTS `sys_userregedit`;
CREATE TABLE `sys_userregedit`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `EnsName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体类名称',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户',
  `Attrs` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '属性s',
  `ContrastKey` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对比项目',
  `KeyVal1` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'KeyVal1',
  `KeyVal2` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'KeyVal2',
  `SortBy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortBy',
  `KeyOfNum` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'KeyOfNum',
  `GroupWay` int(11) NULL DEFAULT NULL COMMENT '求什么?SumAvg',
  `OrderWay` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'OrderWay',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体',
  `AttrKey` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点对应字段',
  `LB` int(11) NULL DEFAULT NULL COMMENT '类别',
  `CurValue` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '文本',
  `CfgKey` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '键',
  `Vals` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '值',
  `GenerSQL` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GenerSQL',
  `Paras` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Paras',
  `NumKey` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分析的Key',
  `OrderBy` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'OrderBy',
  `SearchKey` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SearchKey',
  `MVals` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MVals',
  `IsPic` int(11) NULL DEFAULT NULL COMMENT '是否图片',
  `DTFrom` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询时间从',
  `DTTo` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户注册表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_userregedit
-- ----------------------------
INSERT INTO `sys_userregedit` VALUES ('adminBP.WF.Data.MyStartFlow_SearchAttrs', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 'BP.WF.Data.MyStartFlow_SearchAttrs', '', '', '', '', '', '', '', 0, '', '', '');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.GPM.Apps_SearchAttrs', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, '', '', '@RecCount=0');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.GPM.Depts_SearchAttrs', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, '', '', '@RecCount=11');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.GPM.Emps_SearchAttrs', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, '', '', '@RecCount=2');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.GPM.GPMEmps_SearchAttrs', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, '', '', '@RecCount=2');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.GPM.Groups_SearchAttrs', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, '', '', '@RecCount=0');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.GPM.StationExts_SearchAttrs', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, '', '', '@RecCount=4');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.GPM.Stations_SearchAttrs', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, '', '', '@RecCount=6');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.GPM.StationTypes_SearchAttrs', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, '', '', '@RecCount=0');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.WF.Data.MyDeptFlows_SearchAttrs', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, '', '', '@RecCount=3');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.WF.Data.MyDeptTodolists_SearchAttrs', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, '', '', '@RecCount=0');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.WF.Data.MyJoinFlows_SearchAttrs', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, '', '', '@RecCount=3');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.WF.Data.MyStartFlows_SearchAttrs', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, '', '', '@RecCount=4');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.WF.Port.AdminEmps_SearchAttrs', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 'SearchAttrs', '@UseSta=all@UserType=all@OrgNo=all', '', '', '', '', '', '', 0, '', '', '@RecCount=2');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.WF.Port.Incs_SearchAttrs', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, '', '', '@RecCount=1');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.WF.Template.SQLTemplates_SearchAttrs', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, '', '', '@RecCount=0');

-- ----------------------------
-- Table structure for sys_wfsealdata
-- ----------------------------
DROP TABLE IF EXISTS `sys_wfsealdata`;
CREATE TABLE `sys_wfsealdata`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `OID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'OID',
  `FK_Node` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_Node',
  `FK_MapData` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_MapData',
  `SealData` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'SealData',
  `RDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '签名信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_wfsealdata
-- ----------------------------

-- ----------------------------
-- Table structure for test
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test`  (
  `OID` int(11) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of test
-- ----------------------------

-- ----------------------------
-- Table structure for wf_accepterrole
-- ----------------------------
DROP TABLE IF EXISTS `wf_accepterrole`;
CREATE TABLE `wf_accepterrole`  (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'null',
  `FK_Node` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点',
  `FK_Mode` int(11) NULL DEFAULT NULL COMMENT '模式类型',
  `Tag0` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag0',
  `Tag1` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag5',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '接受人规则' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_accepterrole
-- ----------------------------

-- ----------------------------
-- Table structure for wf_athunreadlog
-- ----------------------------
DROP TABLE IF EXISTS `wf_athunreadlog`;
CREATE TABLE `wf_athunreadlog`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门,外键:对应物理表:Port_Dept,表描述:部门',
  `Title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT 'WorkID',
  `FlowStarter` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起人',
  `FlowStartRDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起时间',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程,外键:对应物理表:WF_Flow,表描述:流程',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点ID',
  `NodeName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `FK_Emp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员',
  `FK_EmpDept` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员部门',
  `FK_EmpDeptName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员名称',
  `BeiZhu` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `SendDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日期',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件未读日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_athunreadlog
-- ----------------------------

-- ----------------------------
-- Table structure for wf_auth
-- ----------------------------
DROP TABLE IF EXISTS `wf_auth`;
CREATE TABLE `wf_auth`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Auther` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权人',
  `AuthType` int(11) NULL DEFAULT NULL COMMENT '类型(0=全部流程1=指定流程)',
  `EmpNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '委托给人员编号',
  `EmpName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '委托给人员名称',
  `FlowNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程名称',
  `TakeBackDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '取回日期',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '授权' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_auth
-- ----------------------------

-- ----------------------------
-- Table structure for wf_bill
-- ----------------------------
DROP TABLE IF EXISTS `wf_bill`;
CREATE TABLE `wf_bill`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT '工作ID',
  `FID` int(11) NULL DEFAULT NULL COMMENT 'FID',
  `FK_Flow` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `FK_BillType` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据类型',
  `Title` varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `FK_Starter` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起人',
  `StartDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起时间',
  `Url` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Url',
  `FullPath` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FullPath',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印人,外键:对应物理表:Port_Emp,表描述:用户',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印时间',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隶属部门,外键:对应物理表:Port_Dept,表描述:部门',
  `FK_NY` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隶属年月',
  `Emps` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Emps',
  `FK_Node` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点',
  `FK_Bill` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_Bill',
  `MyNum` int(11) NULL DEFAULT NULL COMMENT '个数',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_bill
-- ----------------------------

-- ----------------------------
-- Table structure for wf_billtemplate
-- ----------------------------
DROP TABLE IF EXISTS `wf_billtemplate`;
CREATE TABLE `wf_billtemplate`  (
  `No` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'No - 主键',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `TempFilePath` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板路径',
  `NodeID` int(11) NULL DEFAULT NULL COMMENT 'NodeID',
  `FK_MapData` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单编号',
  `BillFileType` int(11) NULL DEFAULT NULL COMMENT '生成的文件类型,枚举类型:0 Word;1 PDF;2 Excel(未完成);3 Html(未完成);',
  `BillOpenModel` int(11) NULL DEFAULT NULL COMMENT '生成的文件打开方式,枚举类型:0 下载本地;1 在线WebOffice打开;',
  `QRModel` int(11) NULL DEFAULT NULL COMMENT '二维码生成方式,枚举类型:0 不生成;1 生成二维码;',
  `TemplateFileModel` int(11) NULL DEFAULT NULL COMMENT '模版模式,枚举类型:0 rtf模版;1 vsto模式的word模版;2 vsto模式的excel模版;',
  `Idx` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Idx',
  `MyFrmID` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单编号',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据模板' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_billtemplate
-- ----------------------------

-- ----------------------------
-- Table structure for wf_ccdept
-- ----------------------------
DROP TABLE IF EXISTS `wf_ccdept`;
CREATE TABLE `wf_ccdept`  (
  `FK_Node` int(11) NOT NULL COMMENT '节点,主外键:对应物理表:WF_Node,表描述:节点',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Node`, `FK_Dept`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '抄送部门' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_ccdept
-- ----------------------------

-- ----------------------------
-- Table structure for wf_ccemp
-- ----------------------------
DROP TABLE IF EXISTS `wf_ccemp`;
CREATE TABLE `wf_ccemp`  (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Node`, `FK_Emp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '抄送人员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_ccemp
-- ----------------------------

-- ----------------------------
-- Table structure for wf_cclist
-- ----------------------------
DROP TABLE IF EXISTS `wf_cclist`;
CREATE TABLE `wf_cclist`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `Sta` int(11) NULL DEFAULT NULL COMMENT '状态',
  `FK_Flow` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程名称',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点',
  `NodeName` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT '工作ID',
  `FID` int(11) NULL DEFAULT NULL COMMENT 'FID',
  `Doc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `Rec` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送人员',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  `CCTo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送给',
  `CCToName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送给(人员名称)',
  `CCToDept` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送到部门',
  `CCToDeptName` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送给部门名称',
  `CDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开时间',
  `PFlowNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父流程编号',
  `PWorkID` int(11) NULL DEFAULT NULL COMMENT '父流程WorkID',
  `InEmpWorks` int(11) NULL DEFAULT NULL COMMENT '是否加入待办列表',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '抄送列表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_cclist
-- ----------------------------

-- ----------------------------
-- Table structure for wf_ccstation
-- ----------------------------
DROP TABLE IF EXISTS `wf_ccstation`;
CREATE TABLE `wf_ccstation`  (
  `FK_Node` int(11) NOT NULL COMMENT '节点,主外键:对应物理表:WF_Node,表描述:节点',
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '工作岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Node`, `FK_Station`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '抄送岗位' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_ccstation
-- ----------------------------

-- ----------------------------
-- Table structure for wf_ch
-- ----------------------------
DROP TABLE IF EXISTS `wf_ch`;
CREATE TABLE `wf_ch`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT '工作ID',
  `FID` int(11) NULL DEFAULT NULL COMMENT 'FID',
  `Title` varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `FK_FlowT` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程名称',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点',
  `FK_NodeT` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `Sender` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送人',
  `SenderT` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送人名称',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当事人',
  `FK_EmpT` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当事人名称',
  `GroupEmps` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关当事人',
  `GroupEmpsNames` varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关当事人名称',
  `GroupEmpsNum` int(11) NULL DEFAULT NULL COMMENT '相关当事人数量',
  `DTFrom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务下达时间',
  `DTTo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务处理时间',
  `SDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应完成日期',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隶属部门',
  `FK_DeptT` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `FK_NY` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隶属月份',
  `DTSWay` int(11) NULL DEFAULT NULL COMMENT '考核方式,枚举类型:0 不考核;1 按照时效考核;2 按照工作量考核;',
  `TimeLimit` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规定限期',
  `OverMinutes` float NULL DEFAULT NULL COMMENT '逾期分钟',
  `UseDays` float NULL DEFAULT NULL COMMENT '实际使用天',
  `OverDays` float NULL DEFAULT NULL COMMENT '逾期天',
  `CHSta` int(11) NULL DEFAULT NULL COMMENT '状态',
  `WeekNum` int(11) NULL DEFAULT NULL COMMENT '第几周',
  `Points` float NULL DEFAULT NULL COMMENT '总扣分',
  `MyNum` int(11) NULL DEFAULT NULL COMMENT '个数',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '时效考核' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_ch
-- ----------------------------
INSERT INTO `wf_ch` VALUES ('102_102_0_admin', 102, 0, '组织架构-admin,超级管理员在2020-02-13 18:44发起.', '001', '请假流程 可用', 102, '申请', 'admin', '超级管理员', 'admin', '超级管理员', ',admin,', 'admin,超级管理员;', 1, '2020-02-13 18:44', '2020-02-13 18:44:22', '2020-02-14 18:44:13', '100', '组织架构', '2020-02', 1, '1', -1439, 0, 0, 0, 6, 0, 1);
INSERT INTO `wf_ch` VALUES ('102_103_0_admin', 103, 0, '组织架构-admin,超级管理员在2020-02-13 18:49发起.', '001', '请假流程 可用', 102, '申请', 'admin', '超级管理员', 'admin', '超级管理员', ',admin,', 'admin,超级管理员;', 1, '2020-02-13 18:49', '2020-02-13 18:50:16', '2020-02-14 18:49:52', '100', '组织架构', '2020-02', 1, '1', -1439, 0, 0, 0, 6, 0, 1);

-- ----------------------------
-- Table structure for wf_cheval
-- ----------------------------
DROP TABLE IF EXISTS `wf_cheval`;
CREATE TABLE `wf_cheval`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `FK_Flow` varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程名称',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT '工作ID',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '评价节点',
  `NodeName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '停留节点',
  `Rec` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价人',
  `RecName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价人名称',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价日期',
  `EvalEmpNo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '被考核的人员编号',
  `EvalEmpName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '被考核的人员名称',
  `EvalCent` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价分值',
  `EvalNote` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价内容',
  `FK_Dept` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门',
  `DeptName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `FK_NY` varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年月',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工作质量评价' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_cheval
-- ----------------------------

-- ----------------------------
-- Table structure for wf_cond
-- ----------------------------
DROP TABLE IF EXISTS `wf_cond`;
CREATE TABLE `wf_cond`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `CondType` int(11) NULL DEFAULT NULL COMMENT '条件类型',
  `DataFrom` int(11) NULL DEFAULT NULL COMMENT '条件数据来源0表单,1岗位(对方向条件有效)',
  `FK_Flow` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `NodeID` int(11) NULL DEFAULT NULL COMMENT '发生的事件MainNode',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点ID',
  `FK_Attr` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性',
  `AttrKey` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性键',
  `AttrName` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '中文名称',
  `FK_Operator` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运算符号',
  `OperatorValue` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '要运算的值',
  `OperatorValueT` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '要运算的值T',
  `ToNodeID` int(11) NULL DEFAULT NULL COMMENT 'ToNodeID（对方向条件有效）',
  `ConnJudgeWay` int(11) NULL DEFAULT NULL COMMENT '条件关系,枚举类型:0 or;1 and;',
  `MyPOID` int(11) NULL DEFAULT NULL COMMENT 'MyPOID',
  `PRI` int(11) NULL DEFAULT NULL COMMENT '计算优先级',
  `CondOrAnd` int(11) NULL DEFAULT NULL COMMENT '方向条件类型',
  `Note` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `AtPara` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '条件' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_cond
-- ----------------------------

-- ----------------------------
-- Table structure for wf_deptflowsearch
-- ----------------------------
DROP TABLE IF EXISTS `wf_deptflowsearch`;
CREATE TABLE `wf_deptflowsearch`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Emp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作员',
  `FK_Flow` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门编号',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '流程部门数据查询权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_deptflowsearch
-- ----------------------------

-- ----------------------------
-- Table structure for wf_direction
-- ----------------------------
DROP TABLE IF EXISTS `wf_direction`;
CREATE TABLE `wf_direction`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Flow` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `Node` int(11) NULL DEFAULT NULL COMMENT '从节点',
  `ToNode` int(11) NULL DEFAULT NULL COMMENT '到节点',
  `IsCanBack` int(11) NULL DEFAULT NULL COMMENT '是否可以原路返回(对后退线有效)',
  `Dots` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '轨迹信息',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '节点方向信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_direction
-- ----------------------------
INSERT INTO `wf_direction` VALUES ('001_101_102', '001', 101, 102, 0, '');
INSERT INTO `wf_direction` VALUES ('001_102_103', '001', 102, 103, 0, '');
INSERT INTO `wf_direction` VALUES ('001_103_104', '001', 103, 104, 0, '');
INSERT INTO `wf_direction` VALUES ('001_104_105', '001', 104, 105, 0, '');
INSERT INTO `wf_direction` VALUES ('002_201_202', '002', 201, 202, 0, NULL);
INSERT INTO `wf_direction` VALUES ('003_301_302', '003', 301, 302, 0, '');

-- ----------------------------
-- Table structure for wf_directionstation
-- ----------------------------
DROP TABLE IF EXISTS `wf_directionstation`;
CREATE TABLE `wf_directionstation`  (
  `FK_Direction` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '工作岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Direction`, `FK_Station`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '节点岗位' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_directionstation
-- ----------------------------

-- ----------------------------
-- Table structure for wf_emp
-- ----------------------------
DROP TABLE IF EXISTS `wf_emp`;
CREATE TABLE `wf_emp`  (
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'No - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Name',
  `UseSta` int(11) NULL DEFAULT NULL COMMENT '用户状态0禁用,1正常.',
  `Tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tel',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_Dept',
  `Email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Email',
  `AlertWay` int(11) NULL DEFAULT NULL COMMENT '收听方式,枚举类型:0 不接收;1 短信;2 邮件;3 内部消息;4 QQ消息;5 RTX消息;6 MSN消息;',
  `Author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权人',
  `AuthorDate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权日期',
  `AuthorWay` int(11) NULL DEFAULT NULL COMMENT '授权方式',
  `AuthorToDate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权到日期',
  `AuthorFlows` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '可以执行的授权流程',
  `Stas` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位s',
  `Depts` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Deptss',
  `Msg` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Msg',
  `Style` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Style',
  `SPass` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片签名密码',
  `Idx` int(11) NULL DEFAULT NULL COMMENT 'Idx',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AtPara',
  `StartFlows` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OrgNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织,外键:对应物理表:Port_Inc,表描述:独立组织',
  `UserType` int(11) NULL DEFAULT NULL COMMENT '用户状态,枚举类型:0 普通用户;1 管理员用户;',
  `RootOfFlow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程权限节点,外键:对应物理表:WF_FlowSort,表描述:流程类别',
  `RootOfForm` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单权限节点,外键:对应物理表:Sys_FormTree,表描述:表单树',
  `RootOfDept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织结构权限节点,外键:对应物理表:Port_Inc,表描述:独立组织',
  `MyNum` int(11) NULL DEFAULT NULL COMMENT '个数',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_emp
-- ----------------------------
INSERT INTO `wf_emp` VALUES ('admin', '超级管理员', 1, '', '100', 'zhoupeng@ccflow.org', 3, '', '', 0, '', '', '', '', '@当前工作[申请]已经完成@发送给如下1位处理人,(zaoyun,赵云).@<a href=\'./WorkOpt/UnSend.htm?DoType=UnSend&UserNo=admin&SID=09bf650e-0136-4507-8eb1-f54d043f4320&WorkID=103&FK_Flow=001\' ><img src=\'./Img/Action/UnSend.png\' border=0 />撤销本次发送</a> @下一步<font color=blue>[科长审批]</font>工作成功启动.@null@已向:{zaoyun}发送提醒信息.', '', '', 0, '@SID_PC_DT=2020-02-13 23:43:54@SID_PC=73c9bfc852fc4e1da549b294e91143ef', '{\"Sort\":[{\"No\":\"1\",\"ParentNo\":\"0\",\"Name\":\"流程树\",\"OrgNo\":\"0\",\"DoDomain\":\"\",\"Idx\":0},{\"No\":\"101\",\"ParentNo\":\"1\",\"Name\":\"财务类\",\"OrgNo\":\"0\",\"DoDomain\":\"\",\"Idx\":1},{\"No\":\"100\",\"ParentNo\":\"1\",\"Name\":\"日常办公类\",\"OrgNo\":\"0\",\"DoDomain\":\"\",\"Idx\":2},{\"No\":\"102\",\"ParentNo\":\"1\",\"Name\":\"人力资源类\",\"OrgNo\":\"0\",\"DoDomain\":\"\",\"Idx\":3}],\"Start\":[{\"No\":\"001\",\"Name\":\"请假流程 可用\",\"IsBatchStart\":1,\"FK_FlowSort\":\"101\",\"FK_FlowSortText\":\"财务类\",\"IsStartInMobile\":1,\"Idx\":0},{\"No\":\"002\",\"Name\":\"打架流程\",\"IsBatchStart\":0,\"FK_FlowSort\":\"100\",\"FK_FlowSortText\":\"日常办公类\",\"IsStartInMobile\":1,\"Idx\":0},{\"No\":\"003\",\"Name\":\"流程1\",\"IsBatchStart\":0,\"FK_FlowSort\":\"102\",\"FK_FlowSortText\":\"人力资源类\",\"IsStartInMobile\":1,\"Idx\":0}]}', '', 3, '', '', '', 1);
INSERT INTO `wf_emp` VALUES ('zaoyun', '赵云', 1, '15666666666', '107', '', 3, '', '', 0, '', '', '', '', '', '', '', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for wf_findworkerrole
-- ----------------------------
DROP TABLE IF EXISTS `wf_findworkerrole`;
CREATE TABLE `wf_findworkerrole`  (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Name',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点ID',
  `SortVal0` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortVal0',
  `SortText0` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortText0',
  `SortVal1` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortVal1',
  `SortText1` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortText1',
  `SortVal2` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortText2',
  `SortText2` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortText2',
  `SortVal3` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortVal3',
  `SortText3` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortText3',
  `TagVal0` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagVal0',
  `TagVal1` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagVal1',
  `TagVal2` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagVal2',
  `TagVal3` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagVal3',
  `TagText0` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagText0',
  `TagText1` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagText1',
  `TagText2` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagText2',
  `TagText3` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagText3',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用',
  `Idx` int(11) NULL DEFAULT NULL COMMENT 'IDX',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '找人规则' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_findworkerrole
-- ----------------------------

-- ----------------------------
-- Table structure for wf_flow
-- ----------------------------
DROP TABLE IF EXISTS `wf_flow`;
CREATE TABLE `wf_flow`  (
  `No` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `FK_FlowSort` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程类别,外键:对应物理表:WF_FlowSort,表描述:流程类别',
  `Name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `FlowMark` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程标记',
  `FlowEventEntity` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程事件实体',
  `TitleRole` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题生成规则',
  `IsCanStart` int(11) NULL DEFAULT NULL COMMENT '可以独立启动否？(独立启动的流程可以显示在发起流程列表里)',
  `IsFullSA` int(11) NULL DEFAULT NULL COMMENT '是否自动计算未来的处理人？',
  `IsGuestFlow` int(11) NULL DEFAULT NULL COMMENT '是否外部用户参与流程(非组织结构人员参与的流程)',
  `FlowAppType` int(11) NULL DEFAULT NULL COMMENT '流程应用类型,枚举类型:0 业务流程;1 工程类(项目组流程);2 公文流程(VSTO);',
  `Draft` int(11) NULL DEFAULT NULL COMMENT '草稿规则,枚举类型:0 无(不设草稿);1 保存到待办;2 保存到草稿箱;',
  `FlowDeleteRole` int(11) NULL DEFAULT NULL COMMENT '流程实例删除规则,枚举类型:0 超级管理员可以删除;1 分级管理员可以删除;2 发起人可以删除;3 节点启动删除按钮的操作员;',
  `IsToParentNextNode` int(11) NULL DEFAULT NULL COMMENT '子流程结束时，让父流程自动运行到下一步',
  `HelpUrl` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '帮助文档',
  `SysType` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统类型',
  `Tester` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起测试人',
  `NodeAppType` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务类型枚举(可为Null)',
  `NodeAppTypeText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务类型枚举(可为Null)',
  `ChartType` int(11) NULL DEFAULT NULL COMMENT '节点图形类型,枚举类型:0 几何图形;1 肖像图片;',
  `HostRun` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运行主机(IP+端口)',
  `IsBatchStart` int(11) NULL DEFAULT NULL COMMENT '是否可以批量发起流程？(如果是就要设置发起的需要填写的字段,多个用逗号分开)',
  `BatchStartFields` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起字段s',
  `HistoryFields` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '历史查看字段',
  `IsResetData` int(11) NULL DEFAULT NULL COMMENT '是否启用开始节点数据重置按钮？',
  `IsLoadPriData` int(11) NULL DEFAULT NULL COMMENT '是否自动装载上一笔数据？',
  `IsDBTemplate` int(11) NULL DEFAULT NULL COMMENT '是否启用数据模版？',
  `IsStartInMobile` int(11) NULL DEFAULT NULL COMMENT '是否可以在手机里启用？(如果发起表单特别复杂就不要在手机里启用了)',
  `IsMD5` int(11) NULL DEFAULT NULL COMMENT '是否是数据加密流程(MD5数据加密防篡改)',
  `DataStoreModel` int(11) NULL DEFAULT NULL COMMENT '流程数据存储模式,枚举类型:0 数据轨迹模式;1 数据合并模式;',
  `PTable` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程数据存储表',
  `FlowNoteExp` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注的表达式',
  `BillNoFormat` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据编号格式',
  `DesignerNo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设计者编号',
  `DesignerName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设计者名称',
  `DesignTime` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  `Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '流程描述',
  `FlowRunWay` int(11) NULL DEFAULT NULL COMMENT '启动方式,枚举类型:0 手工启动;1 指定人员按时启动;2 数据集按时启动;3 触发式启动;',
  `RunObj` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运行内容',
  `RunSQL` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程结束执行后执行的SQL',
  `NumOfBill` int(11) NULL DEFAULT NULL COMMENT '是否有单据',
  `NumOfDtl` int(11) NULL DEFAULT NULL COMMENT 'NumOfDtl',
  `AvgDay` double NULL DEFAULT NULL COMMENT '平均运行用天',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序号(在发起列表中)',
  `SDTOfFlowRole` int(11) NULL DEFAULT NULL COMMENT '流程计划完成日期计算规则',
  `SDTOfFlowRoleSQL` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程计划完成日期计算规则SQL',
  `Paras` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `FlowFrmType` int(11) NULL DEFAULT NULL COMMENT '流程表单类型',
  `FrmUrl` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单Url',
  `DRCtrlType` int(11) NULL DEFAULT NULL COMMENT '部门查询权限控制方式',
  `StartLimitRole` int(11) NULL DEFAULT NULL COMMENT '启动限制规则,枚举类型:0 不限制;1 每人每天一次;2 每人每周一次;3 每人每月一次;4 每人每季一次;5 每人每年一次;6 发起的列不能重复,(多个列可以用逗号分开);7 设置的SQL数据源为空,或者返回结果为零时可以启动.;8 设置的SQL数据源为空,或者返回结果为零时不可以启动.;',
  `StartLimitPara` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规则参数',
  `StartLimitAlert` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '限制提示',
  `StartLimitWhen` int(11) NULL DEFAULT NULL COMMENT '提示时间',
  `StartGuideWay` int(11) NULL DEFAULT NULL COMMENT '前置导航方式,枚举类型:0 无;1 按系统的URL-(父子流程)单条模式;2 按系统的URL-(子父流程)多条模式;3 按系统的URL-(实体记录,未完成)单条模式;4 按系统的URL-(实体记录,未完成)多条模式;5 从开始节点Copy数据;10 按自定义的Url;11 按用户输入参数;',
  `StartGuideLink` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '右侧的连接',
  `StartGuideLab` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '连接标签',
  `StartGuidePara1` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数1',
  `StartGuidePara2` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数2',
  `StartGuidePara3` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数3',
  `IsAutoSendSubFlowOver` int(11) NULL DEFAULT NULL COMMENT '(当前节点为子流程时)是否检查所有子流程完成后父流程自动发送',
  `Ver` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本号',
  `AtPara` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  `DTSWay` int(11) NULL DEFAULT NULL COMMENT '同步方式,枚举类型:0 不考核;1 按照时效考核;2 按照工作量考核;',
  `DTSDBSrc` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据库,外键:对应物理表:Sys_SFDBSrc,表描述:数据源',
  `DTSBTable` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务表名',
  `DTSBTablePK` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务表主键',
  `DTSTime` int(11) NULL DEFAULT NULL COMMENT '执行同步时间点,枚举类型:0 所有的节点发送后;1 指定的节点发送后;2 当流程结束时;',
  `DTSSpecNodes` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指定的节点ID',
  `DTSField` int(11) NULL DEFAULT NULL COMMENT '要同步的字段计算方式,枚举类型:0 字段名相同;1 按设置的字段匹配;',
  `DTSFields` varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '要同步的字段s,中间用逗号分开.',
  `MyDeptRole` int(11) NULL DEFAULT NULL COMMENT '本部门发起的流程,枚举类型:0 仅部门领导可以查看;1 部门下所有的人都可以查看;2 本部门里指定岗位的人可以查看;',
  `PStarter` int(11) NULL DEFAULT NULL COMMENT '发起人可看(必选)',
  `PWorker` int(11) NULL DEFAULT NULL COMMENT '参与人可看(必选)',
  `PCCer` int(11) NULL DEFAULT NULL COMMENT '被抄送人可看(必选)',
  `PMyDept` int(11) NULL DEFAULT NULL COMMENT '本部门人可看',
  `PPMyDept` int(11) NULL DEFAULT NULL COMMENT '直属上级部门可看(比如:我是)',
  `PPDept` int(11) NULL DEFAULT NULL COMMENT '上级部门可看',
  `PSameDept` int(11) NULL DEFAULT NULL COMMENT '平级部门可看',
  `PSpecDept` int(11) NULL DEFAULT NULL COMMENT '指定部门可看',
  `PSpecDeptExt` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门编号',
  `PSpecSta` int(11) NULL DEFAULT NULL COMMENT '指定的岗位可看',
  `PSpecStaExt` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位编号',
  `PSpecGroup` int(11) NULL DEFAULT NULL COMMENT '指定的权限组可看',
  `PSpecGroupExt` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限组',
  `PSpecEmp` int(11) NULL DEFAULT NULL COMMENT '指定的人员可看',
  `PSpecEmpExt` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指定的人员编号',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '流程模版主表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_flow
-- ----------------------------
INSERT INTO `wf_flow` VALUES ('001', '101', '请假流程 可用', '0002', '', '', 1, 1, 0, 0, 0, 2, 0, '', '', '', '', '', 1, '', 1, '', '', 0, 0, 0, 1, 0, 1, 'ND1Rpt', '', '', '', '', '', '', 0, '', '', 0, 0, 0, 0, 0, '', '@StartNodeX=200@StartNodeY=50@EndNodeX=200@EndNodeY=350', 0, '', 0, 0, '', '', 0, 0, '', '', '', '', '', 0, '2020-02-13 18:41', '', 0, '', '', '', 0, '', 0, '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `wf_flow` VALUES ('002', '100', '打架流程', '', '', '', 1, 0, 0, 0, 0, 0, 0, '', '', '', '', '', 1, '', 0, '', '', 0, 0, 0, 1, 0, 1, '', '', '', 'admin', '超级管理员', '2020-02-14 11:00:51', '', 0, '', '', 0, 0, 0, 0, 0, '', '@StartNodeX=200@StartNodeY=50@EndNodeX=200@EndNodeY=350', 0, '', 0, 0, '', '', 0, 0, '', '', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `wf_flow` VALUES ('003', '102', '流程1', '', '', '', 1, 0, 0, 0, 0, 0, 0, '', '', '', '', '', 1, '', 0, '', '', 0, 0, 0, 1, 0, 1, '', '', '', 'admin', '超级管理员', '2020-02-14 12:28:30', '', 0, '', '', 0, 0, 0, 0, 0, '', '@StartNodeX=200@StartNodeY=50@EndNodeX=200@EndNodeY=350', 0, '', 0, 0, '', '', 0, 0, '', '', '', '', '', 0, '', '', 0, '', '', '', 0, '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for wf_flowsort
-- ----------------------------
DROP TABLE IF EXISTS `wf_flowsort`;
CREATE TABLE `wf_flowsort`  (
  `No` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `ParentNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父节点No',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `OrgNo` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织编号(0为系统组织)',
  `DoDomain` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '域/系统编号',
  `Idx` int(11) NULL DEFAULT NULL COMMENT 'Idx',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '流程类别' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_flowsort
-- ----------------------------
INSERT INTO `wf_flowsort` VALUES ('1', '0', '流程树', '0', '', 0);
INSERT INTO `wf_flowsort` VALUES ('100', '1', '日常办公类', '0', '', 2);
INSERT INTO `wf_flowsort` VALUES ('101', '1', '财务类', '0', '', 1);
INSERT INTO `wf_flowsort` VALUES ('102', '1', '人力资源类', '0', '', 3);

-- ----------------------------
-- Table structure for wf_frmnode
-- ----------------------------
DROP TABLE IF EXISTS `wf_frmnode`;
CREATE TABLE `wf_frmnode`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Frm` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点编号',
  `FK_Flow` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `OfficeOpenLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存标签',
  `OfficeSaveEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeOverLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套红按钮标签',
  `OfficeOverEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeMarksEnable` int(11) NULL DEFAULT NULL COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印按钮标签',
  `OfficePrintEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeSealLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签章按钮标签',
  `OfficeSealEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeNodeInfo` int(11) NULL DEFAULT NULL COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) NULL DEFAULT NULL COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeIsMarks` int(11) NULL DEFAULT NULL COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指定文档模板',
  `OfficeIsParent` int(11) NULL DEFAULT NULL COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) NULL DEFAULT NULL COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自动套红模板',
  `FrmSln` int(11) NULL DEFAULT NULL COMMENT '表单控制方案',
  `FrmType` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单类型',
  `IsPrint` int(11) NULL DEFAULT NULL COMMENT '是否可以打印',
  `IsEnableLoadData` int(11) NULL DEFAULT NULL COMMENT '是否启用装载填充事件',
  `IsDefaultOpen` int(11) NULL DEFAULT NULL COMMENT '是否默认打开',
  `IsCloseEtcFrm` int(11) NULL DEFAULT NULL COMMENT '打开时是否关闭其它的页面？',
  `IsEnableFWC` int(11) NULL DEFAULT NULL COMMENT '是否启用审核组件？',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `WhoIsPK` int(11) NULL DEFAULT NULL COMMENT '谁是主键？',
  `Is1ToN` int(11) NULL DEFAULT NULL COMMENT '是否1变N？',
  `HuiZong` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子线程要汇总的数据表',
  `FrmEnableRole` int(11) NULL DEFAULT NULL COMMENT '表单启用规则',
  `FrmEnableExp` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '启用的表达式',
  `TempleteFile` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模版文件',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否显示',
  `GuanJianZiDuan` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关键字段',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '节点表单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_frmnode
-- ----------------------------

-- ----------------------------
-- Table structure for wf_generworkerlist
-- ----------------------------
DROP TABLE IF EXISTS `wf_generworkerlist`;
CREATE TABLE `wf_generworkerlist`  (
  `WorkID` int(11) NOT NULL COMMENT '工作ID - 主键',
  `FK_Emp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '人员 - 主键',
  `FK_Node` int(11) NOT NULL COMMENT '节点ID - 主键',
  `FID` int(11) NULL DEFAULT NULL COMMENT '流程ID',
  `FK_EmpText` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员名称',
  `FK_NodeText` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `FK_Flow` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用部门',
  `SDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应完成日期',
  `DTOfWarning` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '警告日期',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录时间',
  `CDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '完成时间',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用',
  `IsRead` int(11) NULL DEFAULT NULL COMMENT '是否读取',
  `IsPass` int(11) NULL DEFAULT NULL COMMENT '是否通过(对合流节点有效)',
  `WhoExeIt` int(11) NULL DEFAULT NULL COMMENT '谁执行它',
  `Sender` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送人',
  `PRI` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `PressTimes` int(11) NULL DEFAULT NULL COMMENT '催办次数',
  `DTOfHungUp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '挂起时间',
  `DTOfUnHungUp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预计解除挂起时间',
  `HungUpTimes` int(11) NULL DEFAULT NULL COMMENT '挂起次数',
  `GuestNo` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外部用户编号',
  `GuestName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外部用户名称',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AtPara',
  PRIMARY KEY (`WorkID`, `FK_Emp`, `FK_Node`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工作者' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_generworkerlist
-- ----------------------------
INSERT INTO `wf_generworkerlist` VALUES (100, 'admin', 101, 0, '若依', 'Start Node', '001', '103', '无', '2020-02-13 16:00', '2020-02-13 16:00', '2020-02-13 16:00', 1, 1, 0, 0, '', 1, 0, '', '', 0, '', '', '');
INSERT INTO `wf_generworkerlist` VALUES (101, 'admin', 101, 0, '若依', '开始', '001', '103', '2020-02-17 18:39', '2020-02-13 16:04', '2020-02-13 18:39', '2020-02-13 16:04', 1, 1, 0, 0, '超级管理员,admin', 1, 0, '', '', 0, '', '', '');
INSERT INTO `wf_generworkerlist` VALUES (101, 'admin', 102, 0, '若依', '申请', '001', '103', '2020-02-14 16:04:19', '2020-02-14 16:04', '2020-02-13 16:04', '2020-02-13 18:38', 1, 1, 0, 0, 'admin,若依', 1, 0, '', '', 0, '', '', '@FK_DeptT=研发部门');
INSERT INTO `wf_generworkerlist` VALUES (102, 'admin', 101, 0, '超级管理员', '开始', '001', '100', '无', '2020-02-13 18:44', '2020-02-13 18:44', '2020-02-13 18:44', 1, 0, 1, 0, '', 1, 0, '', '', 0, '', '', '');
INSERT INTO `wf_generworkerlist` VALUES (102, 'admin', 102, 0, '超级管理员', '申请', '001', '100', '2020-02-14 18:44:13', '2020-02-14 18:44', '2020-02-13 18:44', '2020-02-13 18:44', 1, 1, 1, 0, 'admin,超级管理员', 1, 0, '', '', 0, '', '', '@FK_DeptT=组织架构');
INSERT INTO `wf_generworkerlist` VALUES (102, 'zaoyun', 103, 0, '赵云', '科长审批', '001', '107', '2020-02-14 18:44:22', '2020-02-14 18:44', '2020-02-13 18:44', '2020-02-13 18:44', 1, 0, 0, 0, 'admin,超级管理员', 1, 0, '', '', 0, '', '', '@FK_DeptT=运维部门');
INSERT INTO `wf_generworkerlist` VALUES (103, 'admin', 101, 0, '超级管理员', '开始', '001', '100', '无', '2020-02-13 18:49', '2020-02-13 18:49', '2020-02-13 18:49', 1, 0, 1, 0, '', 1, 0, '', '', 0, '', '', '');
INSERT INTO `wf_generworkerlist` VALUES (103, 'admin', 102, 0, '超级管理员', '申请', '001', '100', '2020-02-14 18:49:52', '2020-02-14 18:49', '2020-02-13 18:49', '2020-02-13 18:50', 1, 1, 1, 0, 'admin,超级管理员', 1, 0, '', '', 0, '', '', '@FK_DeptT=组织架构');
INSERT INTO `wf_generworkerlist` VALUES (103, 'zaoyun', 103, 0, '赵云', '科长审批', '001', '107', '2020-02-14 18:50:16', '2020-02-14 18:50', '2020-02-13 18:50', '2020-02-13 18:44', 1, 0, 0, 0, 'admin,超级管理员', 1, 0, '', '', 0, '', '', '@FK_DeptT=运维部门');

-- ----------------------------
-- Table structure for wf_generworkflow
-- ----------------------------
DROP TABLE IF EXISTS `wf_generworkflow`;
CREATE TABLE `wf_generworkflow`  (
  `WorkID` int(11) NOT NULL COMMENT 'WorkID - 主键',
  `FID` int(11) NULL DEFAULT NULL COMMENT '流程ID',
  `FK_FlowSort` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程类别',
  `SysType` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统类别',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `FlowName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程名称',
  `Title` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `WFSta` int(11) NULL DEFAULT NULL COMMENT '状态,枚举类型:0 运行中;1 已完成;2 其他;',
  `WFState` int(11) NULL DEFAULT NULL COMMENT '流程状态,枚举类型:0 空白;1 草稿;2 运行中;3 已完成;4 挂起;5 退回;6 转发;7 删除;8 加签;9 冻结;10 批处理;11 加签回复状态;',
  `Starter` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起人',
  `StarterName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起人名称',
  `Sender` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送人',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  `SendDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程活动时间',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点',
  `NodeName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门',
  `DeptName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `PRI` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `SDTOfNode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点应完成时间',
  `SDTOfFlow` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程应完成时间',
  `SDTOfFlowWarning` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程预警时间',
  `PFlowNo` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父流程编号',
  `PWorkID` int(11) NULL DEFAULT NULL COMMENT '父流程ID',
  `PNodeID` int(11) NULL DEFAULT NULL COMMENT '父流程调用节点',
  `PFID` int(11) NULL DEFAULT NULL COMMENT '父流程调用的PFID',
  `PEmp` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子流程的调用人',
  `GuestNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户编号',
  `GuestName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户名称',
  `BillNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `FlowNote` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `TodoEmps` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '待办人员',
  `TodoEmpsNum` int(11) NULL DEFAULT NULL COMMENT '待办人员数量',
  `TaskSta` int(11) NULL DEFAULT NULL COMMENT '共享状态',
  `AtPara` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数(流程运行设置临时存储的参数)',
  `Emps` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '参与人',
  `GUID` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `FK_NY` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年月',
  `WeekNum` int(11) NULL DEFAULT NULL COMMENT '周次',
  `TSpan` int(11) NULL DEFAULT NULL COMMENT '时间间隔',
  `TodoSta` int(11) NULL DEFAULT NULL COMMENT '待办状态',
  `DoDomain` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '域/系统编号',
  `PrjNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'PrjNo',
  `PrjName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'PrjNo',
  `MyNum` int(11) NULL DEFAULT NULL COMMENT '个数',
  PRIMARY KEY (`WorkID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '流程实例' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_generworkflow
-- ----------------------------
INSERT INTO `wf_generworkflow` VALUES (100, 0, '101', '', '001', '请假流程 可用', '研发部门-admin,若依在2020-02-13 16:00发起.', 0, 2, 'admin', '若依', '', '2020-02-13 16:00', '2020-02-12 14:31', 101, 'Start Node', '103', '研发部门', 1, '2020-02-12 14:31', '', '', '', 0, 0, 0, '', '', '', '', '', '', 0, 0, '', '', '', '2020-02', 0, 0, 0, '', '', '', NULL);
INSERT INTO `wf_generworkflow` VALUES (101, 0, '101', '', '001', '请假流程 可用', '研发部门-admin,若依在2020-02-13 16:04发起.', 0, 5, 'admin', '若依', 'admin,超级管理员', '2020-02-13 16:04', '2020-02-13 18:39', 101, '开始', '103', '研发部门', 1, '2020-02-17 18:39', '', '', '', 0, 0, 0, '', '', '', '', '', 'admin,若依;', 1, 0, '@LastTruckID=1142873752@HuiQianTaskSta=0@HuiQianZhuChiRen=@HuiQianZhuChiRenName=', '@admin@admin,超级管理员@', '', '2020-02', 0, 0, 0, '', '', '', NULL);
INSERT INTO `wf_generworkflow` VALUES (102, 0, '101', '', '001', '请假流程 可用', '组织架构-admin,超级管理员在2020-02-13 18:44发起.', 0, 2, 'admin', '超级管理员', 'admin', '2020-02-13 18:44', '2020-02-13 18:44', 103, '科长审批', '100', '组织架构', 1, '2020-02-14 18:44', '', '', '', 0, 0, 0, '', '', '', '', '', 'zaoyun,赵云;', 1, 0, '@LastTruckID=98571581', '@admin@', '', '2020-02', 0, 0, 0, '', '', '', NULL);
INSERT INTO `wf_generworkflow` VALUES (103, 0, '101', '', '001', '请假流程 可用', '组织架构-admin,超级管理员在2020-02-13 18:49发起.', 0, 2, 'admin', '超级管理员', 'admin', '2020-02-13 18:49', '2020-02-13 18:50', 103, '科长审批', '100', '组织架构', 1, '2020-02-14 18:50', '', '', '', 0, 0, 0, '', '', '', '', '', 'zaoyun,赵云;', 1, 0, '@LastTruckID=1180853948', '@admin@', '', '2020-02', 0, 0, 0, '', '', '', NULL);
INSERT INTO `wf_generworkflow` VALUES (104, 0, '101', '', '001', '请假流程 可用', '组织架构-admin,超级管理员在2020-02-13 18:49发起.', 2, 0, 'admin', '超级管理员', '', '2020-02-13 18:49', '2020-02-13 18:44', 101, '开始', '100', '组织架构', 1, '2020-02-13 18:44', '', '', '', 0, 0, 0, '', '', '', '', '', '', 0, 0, '', '', '', '2020-02', 0, 0, 0, '', '', '', NULL);

-- ----------------------------
-- Table structure for wf_hungup
-- ----------------------------
DROP TABLE IF EXISTS `wf_hungup`;
CREATE TABLE `wf_hungup`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点ID',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT 'WorkID',
  `HungUpWay` int(11) NULL DEFAULT NULL COMMENT '挂起方式,枚举类型:0 无限挂起;1 按指定的时间解除挂起并通知我自己;2 按指定的时间解除挂起并通知所有人;',
  `Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '挂起原因(标题与内容支持变量)',
  `Rec` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '挂起人',
  `DTOfHungUp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '挂起时间',
  `DTOfUnHungUp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实际解除挂起时间',
  `DTOfUnHungUpPlan` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预计解除挂起时间',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '挂起' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_hungup
-- ----------------------------

-- ----------------------------
-- Table structure for wf_labnote
-- ----------------------------
DROP TABLE IF EXISTS `wf_labnote`;
CREATE TABLE `wf_labnote`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Name` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'null',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `X` int(11) NULL DEFAULT NULL COMMENT 'X坐标',
  `Y` int(11) NULL DEFAULT NULL COMMENT 'Y坐标',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标签' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_labnote
-- ----------------------------

-- ----------------------------
-- Table structure for wf_node
-- ----------------------------
DROP TABLE IF EXISTS `wf_node`;
CREATE TABLE `wf_node`  (
  `NodeID` int(11) NOT NULL COMMENT 'NodeID - 主键',
  `Step` int(11) NULL DEFAULT NULL COMMENT '步骤',
  `FK_Flow` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程名',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `Tip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作提示',
  `WhoExeIt` int(11) NULL DEFAULT NULL COMMENT '谁执行它,枚举类型:0 操作员执行;1 机器执行;2 混合执行;',
  `ReadReceipts` int(11) NULL DEFAULT NULL COMMENT '已读回执,枚举类型:0 不回执;1 自动回执;2 由上一节点表单字段决定;3 由SDK开发者参数决定;',
  `CondModel` int(11) NULL DEFAULT NULL COMMENT '方向条件控制规则,枚举类型:0 由连接线条件控制;1 按照用户选择计算;2 发送按钮旁下拉框选择;',
  `CancelRole` int(11) NULL DEFAULT NULL COMMENT '撤销规则,枚举类型:0 上一步可以撤销;1 不能撤销;2 上一步与开始节点可以撤销;3 指定的节点可以撤销;',
  `CancelDisWhenRead` int(11) NULL DEFAULT NULL COMMENT '对方已经打开就不能撤销',
  `IsTask` int(11) NULL DEFAULT NULL COMMENT '允许分配工作否?',
  `IsExpSender` int(11) NULL DEFAULT NULL COMMENT '本节点接收人不允许包含上一步发送人',
  `IsRM` int(11) NULL DEFAULT NULL COMMENT '是否启用投递路径自动记忆功能?',
  `IsOpenOver` int(11) NULL DEFAULT NULL COMMENT '已阅即完成?',
  `IsToParentNextNode` int(11) NULL DEFAULT NULL COMMENT '子流程运行到该节点时，让父流程自动运行到下一步',
  `IsYouLiTai` int(11) NULL DEFAULT NULL COMMENT '该节点是否是游离态',
  `DTFrom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生命周期从',
  `DTTo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生命周期到',
  `IsBUnit` int(11) NULL DEFAULT NULL COMMENT '是否是节点模版（业务单元）?',
  `FocusField` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '焦点字段',
  `IsGuestNode` int(11) NULL DEFAULT NULL COMMENT '是否是外部用户执行的节点(非组织结构人员参与处理工作的节点)?',
  `NodeAppType` int(11) NULL DEFAULT NULL COMMENT '节点业务类型',
  `FWCSta` int(11) NULL DEFAULT NULL COMMENT '节点状态',
  `FWCAth` int(11) NULL DEFAULT NULL COMMENT '审核附件是否启用',
  `SelfParas` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义属性',
  `RunModel` int(11) NULL DEFAULT NULL COMMENT '运行模式',
  `SubThreadType` int(11) NULL DEFAULT NULL COMMENT '子线程类型,枚举类型:0 同表单;1 异表单;',
  `PassRate` double NULL DEFAULT NULL COMMENT '完成通过率',
  `SubFlowStartWay` int(11) NULL DEFAULT NULL COMMENT '子线程启动方式,枚举类型:0 不启动;1 指定的字段启动;2 按明细表启动;',
  `SubFlowStartParas` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '启动参数',
  `ThreadIsCanDel` int(11) NULL DEFAULT NULL COMMENT '是否可以删除子线程(当前节点已经发送出去的线程，并且当前节点是分流，或者分合流有效，在子线程退回后的操作)？',
  `ThreadIsCanShift` int(11) NULL DEFAULT NULL COMMENT '是否可以移交子线程(当前节点已经发送出去的线程，并且当前节点是分流，或者分合流有效，在子线程退回后的操作)？',
  `IsAllowRepeatEmps` int(11) NULL DEFAULT NULL COMMENT '是否允许子线程接受人员重复(仅当分流点向子线程发送时有效)?',
  `AutoRunEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用自动运行？(仅当分流点向子线程发送时有效)',
  `AutoRunParas` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自动运行SQL',
  `AutoJumpRole0` int(11) NULL DEFAULT NULL COMMENT '处理人就是发起人',
  `AutoJumpRole1` int(11) NULL DEFAULT NULL COMMENT '处理人已经出现过',
  `AutoJumpRole2` int(11) NULL DEFAULT NULL COMMENT '处理人与上一步相同',
  `WhenNoWorker` int(11) NULL DEFAULT NULL COMMENT '(是)找不到人就跳转,(否)提示错误.',
  `SendLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送按钮标签',
  `SendJS` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按钮JS函数',
  `SaveLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存按钮标签',
  `SaveEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `ThreadLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子线程按钮标签',
  `ThreadEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `ThreadKillRole` int(11) NULL DEFAULT NULL COMMENT '子线程删除方式,枚举类型:0 不能删除;1 手工删除;2 自动删除;',
  `JumpWayLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转按钮标签',
  `JumpWay` int(11) NULL DEFAULT NULL COMMENT '跳转规则,枚举类型:0 不能跳转;1 只能向后跳转;2 只能向前跳转;3 任意节点跳转;4 按指定规则跳转;',
  `JumpToNodes` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '可跳转的节点',
  `ReturnLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '退回按钮标签',
  `ReturnRole` int(11) NULL DEFAULT NULL COMMENT '退回规则,枚举类型:0 不能退回;1 只能退回上一个节点;2 可退回以前任意节点;3 可退回指定的节点;4 由流程图设计的退回路线决定;',
  `ReturnAlert` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '被退回后信息提示',
  `IsBackTracking` int(11) NULL DEFAULT NULL COMMENT '是否可以原路返回(启用退回功能才有效)',
  `ReturnField` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '退回信息填写字段',
  `ReturnReasonsItems` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '退回原因',
  `ReturnOneNodeRole` int(11) NULL DEFAULT NULL COMMENT '单节点退回规则,枚举类型:0 不启用;1 按照[退回信息填写字段]作为退回意见直接退回;2 按照[审核组件]填写的信息作为退回意见直接退回;',
  `CCLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送按钮标签',
  `CCRole` int(11) NULL DEFAULT NULL COMMENT '抄送规则,枚举类型:0 不能抄送;1 手工抄送;2 自动抄送;3 手工与自动;4 按表单SysCCEmps字段计算;5 在发送前打开抄送窗口;',
  `CCWriteTo` int(11) NULL DEFAULT NULL COMMENT '抄送写入规则,枚举类型:0 写入抄送列表;1 写入待办;2 写入待办与抄送列表;',
  `DoOutTime` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '超时处理内容',
  `DoOutTimeCond` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '执行超时的条件',
  `ShiftLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移交按钮标签',
  `ShiftEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `DelLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除按钮标签',
  `DelEnable` int(11) NULL DEFAULT NULL COMMENT '删除规则,枚举类型:0 不能删除;1 逻辑删除;2 记录日志方式删除;3 彻底删除;4 让用户决定删除方式;',
  `EndFlowLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结束流程按钮标签',
  `EndFlowEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `ShowParentFormLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查看父流程按钮标签',
  `ShowParentFormEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeBtnLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公文按钮标签',
  `OfficeBtnEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `PrintHtmlLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印Html标签',
  `PrintHtmlEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `PrintPDFLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印pdf标签',
  `PrintPDFEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `PrintPDFModle` int(11) NULL DEFAULT NULL COMMENT 'PDF打印规则,枚举类型:0 全部打印;1 单个表单打印(针对树形表单);',
  `ShuiYinModle` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印水印规则',
  `PrintZipLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打包下载zip按钮标签',
  `PrintZipEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `PrintDocLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印单据按钮标签',
  `PrintDocEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `TrackLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '轨迹按钮标签',
  `TrackEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `HungLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '挂起按钮标签',
  `HungEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `SearchLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询按钮标签',
  `SearchEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `HuiQianLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '会签标签',
  `HuiQianRole` int(11) NULL DEFAULT NULL COMMENT '会签模式,枚举类型:0 不启用;1 协作模式;4 组长模式;',
  `HuiQianLeaderRole` int(11) NULL DEFAULT NULL COMMENT '组长会签规则,枚举类型:0 只有一个组长;1 最后一个组长发送;2 任意组长发送;',
  `TCLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流转自定义',
  `TCEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `WebOffice` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文档按钮标签',
  `WebOfficeEnable` int(11) NULL DEFAULT NULL COMMENT '文档启用方式',
  `PRILab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '重要性',
  `PRIEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `CHLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点时限',
  `CHRole` int(11) NULL DEFAULT NULL COMMENT '时限规则,枚举类型:0 禁用;1 启用;2 只读;3 启用并可以调整流程应完成时间;',
  `AllotLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分配按钮标签',
  `AllotEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `FocusLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关注',
  `FocusEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `ConfirmLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '确认按钮标签',
  `ConfirmEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `ListLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列表按钮标签',
  `ListEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `BatchLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '批量审核标签',
  `BatchEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `NoteLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注标签',
  `NoteEnable` int(11) NULL DEFAULT NULL COMMENT '启用规则,枚举类型:0 禁用;1 启用;2 只读;',
  `HelpLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '帮助标签',
  `HelpRole` int(11) NULL DEFAULT NULL COMMENT '帮助显示规则,枚举类型:0 禁用;1 启用;2 强制提示;3 选择性提示;',
  `TAlertRole` int(11) NULL DEFAULT NULL COMMENT '逾期提醒规则',
  `TAlertWay` int(11) NULL DEFAULT NULL COMMENT '逾期提醒方式',
  `WAlertRole` int(11) NULL DEFAULT NULL COMMENT '预警提醒规则',
  `WAlertWay` int(11) NULL DEFAULT NULL COMMENT '预警提醒方式',
  `TCent` float NULL DEFAULT NULL COMMENT '扣分(每延期1小时)',
  `CHWay` int(11) NULL DEFAULT NULL COMMENT '考核方式',
  `IsEval` int(11) NULL DEFAULT NULL COMMENT '是否工作质量考核',
  `OutTimeDeal` int(11) NULL DEFAULT NULL COMMENT '超时处理方式',
  `CCIsAttr` int(11) NULL DEFAULT NULL COMMENT '按表单字段抄送',
  `CCFormAttr` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送人员字段',
  `CCIsStations` int(11) NULL DEFAULT NULL COMMENT '是否启用？-按照岗位抄送',
  `CCStaWay` int(11) NULL DEFAULT NULL COMMENT '抄送岗位计算方式,枚举类型:0 仅按岗位计算;1 按岗位智能计算(当前节点);2 按岗位智能计算(发送到节点);3 按岗位与部门的交集;4 按直线上级部门找岗位下的人员(当前节点);5 按直线上级部门找岗位下的人员(接受节点);',
  `CCIsDepts` int(11) NULL DEFAULT NULL COMMENT '是否启用？-按照部门抄送',
  `CCIsEmps` int(11) NULL DEFAULT NULL COMMENT '是否启用？-按照人员抄送',
  `CCIsSQLs` int(11) NULL DEFAULT NULL COMMENT '是否启用？-按照SQL抄送',
  `CCSQL` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SQL表达式',
  `CCTitle` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送标题',
  `CCDoc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '抄送内容(标题与内容支持变量)',
  `FWCLab` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示标签',
  `FWCShowModel` int(11) NULL DEFAULT NULL COMMENT '显示方式,枚举类型:0 表格方式;1 自由模式;',
  `FWCType` int(11) NULL DEFAULT NULL COMMENT '审核组件,枚举类型:0 审核组件;1 日志组件;2 周报组件;3 月报组件;',
  `FWCNodeName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点意见名称',
  `FWCTrackEnable` int(11) NULL DEFAULT NULL COMMENT '轨迹图是否显示？',
  `FWCListEnable` int(11) NULL DEFAULT NULL COMMENT '历史审核信息是否显示？(否,仅出现意见框)',
  `FWCIsShowAllStep` int(11) NULL DEFAULT NULL COMMENT '在轨迹表里是否显示所有的步骤？',
  `FWCOpLabel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作名词(审核/审阅/批示)',
  `FWCDefInfo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认审核信息',
  `SigantureEnabel` int(11) NULL DEFAULT NULL COMMENT '操作人是否显示为图片签名？',
  `FWCIsFullInfo` int(11) NULL DEFAULT NULL COMMENT '如果用户未审核是否按照默认意见填充？',
  `FWC_X` float(11, 2) NULL DEFAULT NULL COMMENT '位置X',
  `FWC_Y` float(11, 2) NULL DEFAULT NULL COMMENT '位置Y',
  `FWC_H` float(11, 2) NULL DEFAULT NULL COMMENT '高度(0=100%)',
  `FWC_W` float(11, 2) NULL DEFAULT NULL COMMENT '宽度(0=100%)',
  `FWCFields` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审批格式字段',
  `FWCIsShowTruck` int(11) NULL DEFAULT NULL COMMENT '是否显示未审核的轨迹？',
  `FWCIsShowReturnMsg` int(11) NULL DEFAULT NULL COMMENT '是否显示退回信息？',
  `FWCOrderModel` int(11) NULL DEFAULT NULL COMMENT '协作模式下操作员显示顺序,枚举类型:0 按审批时间先后排序;1 按照接受人员列表先后顺序(官职大小);',
  `FWCMsgShow` int(11) NULL DEFAULT NULL COMMENT '审核意见显示方式,枚举类型:0 都显示;1 仅显示自己的意见;',
  `FWCVer` int(11) NULL DEFAULT NULL COMMENT '审核意见版本号,枚举类型:0 2018;1 2019;',
  `FWCView` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核意见立场',
  `CheckNodes` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工作节点s',
  `DeliveryWay` int(11) NULL DEFAULT NULL COMMENT '访问规则',
  `ICON` varchar(70) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点ICON图片路径',
  `NodeWorkType` int(11) NULL DEFAULT NULL COMMENT '节点类型',
  `FrmAttr` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FrmAttr',
  `TimeLimit` float(11, 2) NULL DEFAULT NULL COMMENT '限期(天)',
  `TWay` int(11) NULL DEFAULT NULL COMMENT '时间计算方式',
  `WarningDay` float(11, 2) NULL DEFAULT NULL COMMENT '工作预警(天)',
  `Doc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `DeliveryParas` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '访问规则设置',
  `NodeFrmID` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点表单ID',
  `SaveModel` int(11) NULL DEFAULT NULL COMMENT '保存模式',
  `IsCanDelFlow` int(11) NULL DEFAULT NULL COMMENT '是否可以删除流程',
  `TodolistModel` int(11) NULL DEFAULT NULL COMMENT '多人处理规则',
  `TeamLeaderConfirmRole` int(11) NULL DEFAULT NULL COMMENT '组长确认规则',
  `TeamLeaderConfirmDoc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组长确认设置内容',
  `IsHandOver` int(11) NULL DEFAULT NULL COMMENT '是否可以移交',
  `BlockModel` int(11) NULL DEFAULT NULL COMMENT '阻塞模式',
  `BlockExp` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '阻塞表达式',
  `BlockAlert` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '被阻塞提示信息',
  `SFActiveFlows` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '可触发的子流程编号(多个用逗号分开)',
  `BatchRole` int(11) NULL DEFAULT NULL COMMENT '批处理',
  `BatchListCount` int(11) NULL DEFAULT NULL COMMENT '批处理数量',
  `BatchParas` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `FormType` int(11) NULL DEFAULT NULL COMMENT '表单类型',
  `FormUrl` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单URL',
  `TurnToDeal` int(11) NULL DEFAULT NULL COMMENT '转向处理',
  `TurnToDealDoc` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送后提示信息',
  `NodePosType` int(11) NULL DEFAULT NULL COMMENT '位置',
  `IsCCFlow` int(11) NULL DEFAULT NULL COMMENT '是否有流程完成条件',
  `HisStas` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位',
  `HisDeptStrs` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门',
  `HisToNDs` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '转到的节点',
  `HisBillIDs` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据IDs',
  `HisSubFlows` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'HisSubFlows',
  `PTable` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物理表',
  `GroupStaNDs` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位分组节点',
  `X` int(11) NULL DEFAULT NULL COMMENT 'X坐标',
  `Y` int(11) NULL DEFAULT NULL COMMENT 'Y坐标',
  `RefOneFrmTreeType` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '独立表单类型',
  `AtPara` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  `SF_H` float(11, 2) NULL DEFAULT NULL COMMENT '高度',
  `SF_W` float(11, 2) NULL DEFAULT NULL COMMENT '宽度',
  `SelectAccepterLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接受人按钮标签',
  `SelectAccepterEnable` int(11) NULL DEFAULT NULL COMMENT '方式,枚举类型:0 不启用;1 单独启用;2 在发送前打开;3 转入新页面;',
  `WorkCheckLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核按钮标签',
  `WorkCheckEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `AskforLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '加签标签',
  `AskforEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeOpenLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存标签',
  `OfficeSaveEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeOverLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套红标签',
  `OfficeOverEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeMarksEnable` int(11) NULL DEFAULT NULL COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印标签',
  `OfficePrintEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeSealLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签章标签',
  `OfficeSealEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeNodeInfo` int(11) NULL DEFAULT NULL COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) NULL DEFAULT NULL COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeIsMarks` int(11) NULL DEFAULT NULL COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指定文档模板',
  `OfficeIsParent` int(11) NULL DEFAULT NULL COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) NULL DEFAULT NULL COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自动套红模板',
  `OfficeOpen` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenTemplate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开模板标签',
  `OfficeSave` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存标签',
  `OfficeAccept` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接受修订标签',
  `OfficeRefuse` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeOver` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套红按钮标签',
  `OfficeMarks` int(11) NULL DEFAULT NULL COMMENT '是否查看用户留痕',
  `OfficeReadOnly` int(11) NULL DEFAULT NULL COMMENT '是否只读',
  `OfficePrint` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印按钮标签',
  `OfficeSeal` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签章按钮标签',
  `OfficeInsertFlow` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插入流程标签',
  `OfficeIsTrueTH` int(11) NULL DEFAULT NULL COMMENT '是否自动套红',
  `WebOfficeFrmModel` int(11) NULL DEFAULT NULL COMMENT '表单工作方式,枚举类型:',
  `SFLab` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示标签',
  `SFSta` int(11) NULL DEFAULT NULL COMMENT '组件状态,枚举类型:0 禁用;1 启用;2 只读;',
  `SFShowModel` int(11) NULL DEFAULT NULL COMMENT '显示方式,枚举类型:0 表格方式;1 自由模式;',
  `SFCaption` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '连接标题',
  `SFDefInfo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '可启动的子流程编号(多个用逗号分开)',
  `SF_X` float(11, 2) NULL DEFAULT NULL COMMENT '位置X',
  `SF_Y` float(11, 2) NULL DEFAULT NULL COMMENT '位置Y',
  `SFFields` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审批格式字段',
  `SFShowCtrl` int(11) NULL DEFAULT NULL COMMENT '显示控制方式,枚举类型:0 可以看所有的子流程;1 仅仅可以看自己发起的子流程;',
  `SFOpenType` int(11) NULL DEFAULT NULL COMMENT '打开子流程显示,枚举类型:0 工作查看器;1 傻瓜表单轨迹查看器;',
  `FrmThreadLab` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示标签',
  `FrmThreadSta` int(11) NULL DEFAULT NULL COMMENT '组件状态,枚举类型:0 禁用;1 启用;',
  `FrmThread_X` float(11, 2) NULL DEFAULT NULL COMMENT '位置X',
  `FrmThread_Y` float(11, 2) NULL DEFAULT NULL COMMENT '位置Y',
  `FrmThread_H` float(11, 2) NULL DEFAULT NULL COMMENT '高度',
  `FrmThread_W` float(11, 2) NULL DEFAULT NULL COMMENT '宽度',
  `FrmTrackLab` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示标签',
  `FrmTrackSta` int(11) NULL DEFAULT NULL COMMENT '组件状态,枚举类型:0 禁用;1 显示轨迹图;2 显示轨迹表;',
  `FrmTrack_X` float(11, 2) NULL DEFAULT NULL COMMENT '位置X',
  `FrmTrack_Y` float(11, 2) NULL DEFAULT NULL COMMENT '位置Y',
  `FrmTrack_H` float(11, 2) NULL DEFAULT NULL COMMENT '高度',
  `FrmTrack_W` float(11, 2) NULL DEFAULT NULL COMMENT '宽度',
  `FTCLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示标签',
  `FTCSta` int(11) NULL DEFAULT NULL COMMENT '组件状态,枚举类型:0 禁用;1 只读;2 可设置人员;',
  `FTCWorkModel` int(11) NULL DEFAULT NULL COMMENT '工作模式,枚举类型:0 简洁模式;1 高级模式;',
  `FTC_X` float(11, 2) NULL DEFAULT NULL COMMENT '位置X',
  `FTC_Y` float(11, 2) NULL DEFAULT NULL COMMENT '位置Y',
  `FTC_H` float(11, 2) NULL DEFAULT NULL COMMENT '高度',
  `FTC_W` float(11, 2) NULL DEFAULT NULL COMMENT '宽度',
  `SelectorModel` int(11) NULL DEFAULT NULL COMMENT '显示方式,枚举类型:0 按岗位;1 按部门;2 按人员;3 按SQL;4 按SQL模版计算;5 使用通用人员选择器;6 部门与岗位的交集;7 自定义Url;8 使用通用部门岗位人员选择器;9 按岗位智能计算(操作员所在部门);',
  `FK_SQLTemplate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SQL模版',
  `FK_SQLTemplateText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SQL模版',
  `IsAutoLoadEmps` int(11) NULL DEFAULT NULL COMMENT '是否自动加载上一次选择的人员？',
  `IsSimpleSelector` int(11) NULL DEFAULT NULL COMMENT '是否单项选择(只能选择一个人)？',
  `IsEnableDeptRange` int(11) NULL DEFAULT NULL COMMENT '是否启用部门搜索范围限定(对使用通用人员选择器有效)？',
  `IsEnableStaRange` int(11) NULL DEFAULT NULL COMMENT '是否启用岗位搜索范围限定(对使用通用人员选择器有效)？',
  `SelectorP1` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '分组参数:可以为空,比如:SELECT No,Name,ParentNo FROM  Port_Dept',
  `SelectorP2` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '操作员数据源:比如:SELECT No,Name,FK_Dept FROM  Port_Emp',
  `SelectorP3` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '默认选择的数据源:比如:SELECT FK_Emp FROM  WF_GenerWorkerList WHERE FK_Node=102 AND WorkID=@WorkID',
  `SelectorP4` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '强制选择的数据源:比如:SELECT FK_Emp FROM  WF_GenerWorkerList WHERE FK_Node=102 AND WorkID=@WorkID',
  PRIMARY KEY (`NodeID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '选择器' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_node
-- ----------------------------
INSERT INTO `wf_node` VALUES (101, 1, '001', '请假流程 可用', '开始', '', 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, '', 0, 0, 100, 0, '', 0, 0, 0, 0, '', 0, 0, 0, 0, '发送', '', '保存', 0, '子线程', 0, 0, '跳转', 0, '', '退回', 0, '', 1, '', '', 0, '抄送', 0, 0, '', '', '移交', 0, '删除', 1, '结束流程', 0, '查看父流程', 0, '公文主文件', 0, '打印Html', 0, '打印pdf', 0, 0, '', '打包下载', 0, '打印单据', 0, '轨迹', 0, '挂起', 0, '查询', 0, '会签', 0, 0, '流转自定义', 0, '公文', 0, '重要性', 0, '节点时限', 0, '分配', 0, '关注', 0, '确认', 0, '列表', 0, '批量审核', 0, '备注', 0, '帮助', 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', '', '', '审核信息', 1, 0, '', 1, 1, 0, '审核', '同意', 0, 1, 300.00, 500.00, 0.00, 400.00, '', 0, 0, 0, 0, 0, '', NULL, 4, '前台', 1, '', 2.00, 0, 1.00, '', '', '', 0, 0, 0, 0, '', 0, 0, '', '', '', 0, 12, '', 0, 'http://', 0, '', 0, 0, '', '', '@102', '', '', '', '@101@102', 504, 156, '', '@IsYouLiTai=0', 300.00, 400.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '子流程', 0, 0, '启动子流程', '', 0.00, 0.00, '', 0, 0, '子线程', 0, 0.00, 0.00, 0.00, 0.00, '轨迹', 0, 0.00, 0.00, 0.00, 0.00, '流转自定义', 0, 0, 0.00, 0.00, 0.00, 0.00, 5, '', '', 0, 0, 0, 0, '', '', '', '');
INSERT INTO `wf_node` VALUES (102, 2, '001', '请假流程 可用', '申请', '', 0, 0, 2, 0, 0, 1, 1, 1, 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, '', 0, 0, 100, 0, '', 0, 0, 0, 0, '', 0, 0, 0, 0, '发送', '', '保存', 0, '子线程', 0, 0, '跳转', 0, '', '退回', 1, '', 1, '', '', 0, '抄送', 0, 0, '', '', '移交', 0, '删除', 0, '结束流程', 0, '查看父流程', 0, '公文主文件', 0, '打印Html', 0, '打印pdf', 0, 0, '', '打包下载', 0, '打印单据', 0, '轨迹', 0, '挂起', 0, '查询', 0, '会签', 0, 0, '流转自定义', 0, '公文', 0, '重要性', 0, '节点时限', 0, '分配', 0, '关注', 0, '确认', 0, '列表', 0, '批量审核', 0, '备注', 0, '帮助', 0, 0, 0, 0, 0, 2, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, '', '', '', '审核信息', 1, 0, '', 1, 1, 0, '审核', '同意', 0, 1, 300.00, 500.00, 0.00, 400.00, '', 0, 0, 0, 0, 0, '', NULL, 3, '审核', 0, '', 1.00, 1, 1.00, '', '', '', 0, 0, 0, 0, '', 0, 0, '', '', '', 0, 12, '', 0, 'http://', 0, '', 1, 0, '', '@100@101@102@103@104@105@106@107@108@109', '@103', '', '', '', '@101@102', 200, 250, '', '@IsYouLiTai=0', 300.00, 400.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '子流程', 0, 0, '启动子流程', '', 0.00, 0.00, '', 0, 0, '子线程', 0, 0.00, 0.00, 0.00, 0.00, '轨迹', 0, 0.00, 0.00, 0.00, 0.00, '流转自定义', 0, 0, 0.00, 0.00, 0.00, 0.00, 5, '', '', 0, 0, 0, 0, '', '', '', '');
INSERT INTO `wf_node` VALUES (103, 3, '001', '请假流程 可用', '科长审批', '', 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, '', '', 0, '审核意见:@KZSPYJ_Note', 0, 0, 0, 0, '', 0, 0, 100, 0, '', 0, 0, 0, 0, '', 0, 0, 0, 0, '发送', '', '保存', 0, '子线程', 0, 0, '跳转', 0, '', '退回', 1, '', 1, '', '', 0, '抄送', 0, 0, '', '', '移交', 0, '删除', 0, '结束流程', 0, '查看父流程', 0, '公文主文件', 0, '打印Html', 0, '打印pdf', 0, 0, '', '打包下载', 0, '打印单据', 0, '轨迹', 0, '挂起', 0, '查询', 0, '会签', 0, 0, '流转自定义', 0, '公文', 0, '重要性', 0, '节点时限', 0, '分配', 0, '关注', 0, '确认', 0, '列表', 0, '批量审核', 0, '备注', 0, '帮助', 0, 0, 0, 0, 0, 2, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, '', '', '', '审核信息', 1, 0, '', 1, 1, 0, '审核', '同意', 0, 1, 300.00, 500.00, 500.00, 400.00, '', 0, 0, 0, 0, 1, '', NULL, 1, '审核.png', 0, '', 1.00, 1, 1.00, '', '', 'Pri', 0, 0, 0, 0, '', 0, 0, '', '', '', 0, 12, '', 0, 'http://', 0, '', 1, 0, '@100001', '@100001@100@101@102@103@104@105@106@107@108@109@200', '@104', '', '', '', '@103', 501, 345, '', '@IsYouLiTai=0', 300.00, 400.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '子流程', 0, 0, '启动子流程', '', 0.00, 0.00, '', 0, 0, '子线程', 0, 0.00, 0.00, 0.00, 0.00, '轨迹', 0, 0.00, 0.00, 0.00, 0.00, '流转自定义', 0, 0, 0.00, 0.00, 0.00, 0.00, 5, '', '', 0, 0, 0, 0, '', '', '', '');
INSERT INTO `wf_node` VALUES (104, 4, '001', '请假流程 可用', '部长审批', '', 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, '', '', 0, '审核意见:@BZSPYJ_Note', 0, 0, 0, 0, '', 0, 0, 100, 0, '', 0, 0, 0, 0, '', 0, 0, 0, 0, '发送', '', '保存', 0, '子线程', 0, 0, '跳转', 0, '', '退回', 1, '', 1, '', '', 0, '抄送', 0, 0, '', '', '移交', 0, '删除', 0, '结束流程', 0, '查看父流程', 0, '公文主文件', 0, '打印Html', 0, '打印pdf', 0, 0, '', '打包下载', 0, '打印单据', 0, '轨迹', 0, '挂起', 0, '查询', 0, '会签', 0, 0, '流转自定义', 0, '公文', 0, '重要性', 0, '节点时限', 0, '分配', 0, '关注', 0, '确认', 0, '列表', 0, '批量审核', 0, '备注', 0, '帮助', 0, 0, 0, 0, 0, 2, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, '', '', '', '审核信息', 1, 0, '', 1, 1, 0, '审核', '同意', 0, 1, 300.00, 500.00, 0.00, 400.00, '', 0, 0, 0, 0, 1, '', NULL, 6, '审核.png', 0, '', 1.00, 1, 1.00, '', '', 'ND104', 0, 0, 0, 0, '', 0, 0, '', '', '', 0, 12, '', 0, 'http://', 0, '', 1, 0, '@100010', '@100010', '@105', '', '', '', '@104', 215, 435, '', '@IsYouLiTai=0', 300.00, 400.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '子流程', 0, 0, '启动子流程', '', 0.00, 0.00, '', 0, 0, '子线程', 0, 0.00, 0.00, 0.00, 0.00, '轨迹', 0, 0.00, 0.00, 0.00, 0.00, '流转自定义', 0, 0, 0.00, 0.00, 0.00, 0.00, 5, '', '', 0, 0, 0, 0, '', '', '', '');
INSERT INTO `wf_node` VALUES (105, 5, '001', '请假流程 可用', '董事长审批', '', 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, '', 0, 0, 100, 0, '', 0, 0, 0, 0, '', 0, 0, 0, 0, '发送', '', '保存', 0, '子线程', 0, 0, '跳转', 0, '', '退回', 1, '', 1, '', '', 0, '抄送', 0, 0, '', '', '移交', 0, '删除', 0, '结束流程', 0, '查看父流程', 0, '公文主文件', 0, '打印Html', 0, '打印pdf', 0, 0, '', '打包下载', 0, '打印单据', 0, '轨迹', 0, '挂起', 0, '查询', 0, '会签', 0, 0, '流转自定义', 0, '公文', 0, '重要性', 0, '节点时限', 0, '分配', 0, '关注', 0, '确认', 0, '列表', 0, '批量审核', 0, '备注', 0, '帮助', 0, 0, 0, 0, 0, 2, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, '', '', '', '审核信息', 1, 0, '', 1, 1, 0, '审核', '同意', 0, 1, 300.00, 500.00, 0.00, 400.00, '', 0, 0, 0, 0, 1, '', NULL, 11, '审核.png', 0, '', 1.00, 1, 1.00, '', '104,', '', 0, 0, 0, 0, '', 0, 0, '', '', '', 0, 12, '', 0, 'http://', 0, '', 2, 0, '@100100', '@100100', '', '', '', '', '@105', 521, 565, '', '@IsYouLiTai=0', 300.00, 400.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '子流程', 0, 0, '启动子流程', '', 0.00, 0.00, '', 0, 0, '子线程', 0, 0.00, 0.00, 0.00, 0.00, '轨迹', 0, 0.00, 0.00, 0.00, 0.00, '流转自定义', 0, 0, 0.00, 0.00, 0.00, 0.00, 5, '', '', 0, 0, 0, 0, '', '', '', '');
INSERT INTO `wf_node` VALUES (201, 1, '002', '打架流程', 'Start Node', '', 0, 0, 2, 0, 0, 1, 1, 1, 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, '', 0, 0, 100, 0, '', 0, 0, 0, 0, '', 0, 0, 0, 0, '发送', '', '保存', 0, '子线程', 0, 0, '跳转', 0, '', '退回', 0, '', 1, '', '', 0, '抄送', 0, 0, '', '', '移交', 0, '删除', 1, '结束流程', 0, '查看父流程', 0, '公文主文件', 0, '打印Html', 0, '打印pdf', 0, 0, '', '打包下载', 0, '打印单据', 0, '轨迹', 0, '挂起', 0, '查询', 0, '会签', 0, 0, '流转自定义', 0, '公文', 0, '重要性', 0, '节点时限', 0, '分配', 0, '关注', 0, '确认', 0, '列表', 0, '批量审核', 0, '备注', 0, '帮助', 0, 0, 0, 0, 0, 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 4, '前台', 1, '', 2.00, 0, 1.00, '', '', '', 0, 0, 0, 0, '', 0, 0, '', '', '', 0, 12, '', 0, 'http://', 0, '', 0, 0, '', '', '@202', '', '', '', '@201@202', 200, 150, '', '@IsYouLiTai=0', 300.00, 400.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, '', '', 0, 0, 0, 0, '', '', '', '');
INSERT INTO `wf_node` VALUES (202, 2, '002', '打架流程', 'Node 2', '', 0, 0, 2, 0, 0, 1, 1, 1, 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, '', 0, 0, 100, 0, '', 0, 0, 0, 0, '', 0, 0, 0, 0, '发送', '', '保存', 0, '子线程', 0, 0, '跳转', 0, '', '退回', 1, '', 1, '', '', 0, '抄送', 0, 0, '', '', '移交', 0, '删除', 0, '结束流程', 0, '查看父流程', 0, '公文主文件', 0, '打印Html', 0, '打印pdf', 0, 0, '', '打包下载', 0, '打印单据', 0, '轨迹', 0, '挂起', 0, '查询', 0, '会签', 0, 0, '流转自定义', 0, '公文', 0, '重要性', 0, '节点时限', 0, '分配', 0, '关注', 0, '确认', 0, '列表', 0, '批量审核', 0, '备注', 0, '帮助', 0, 0, 0, 0, 0, 2, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 4, '审核', 0, '', 1.00, 1, 1.00, '', '', '', 0, 0, 0, 0, '', 0, 0, '', '', '', 0, 12, '', 0, 'http://', 0, '', 2, 0, '', '', '', '', '', '', '@201@202', 200, 250, '', '@IsYouLiTai=0', 300.00, 400.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, '', '', 0, 0, 0, 0, '', '', '', '');
INSERT INTO `wf_node` VALUES (301, 1, '003', '流程1', 'Start Node', '', 0, 0, 2, 0, 0, 1, 1, 1, 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, '', 0, 0, 100, 0, '', 0, 0, 0, 0, '', 0, 0, 0, 0, '发送', '', '保存', 0, '子线程', 0, 0, '跳转', 0, '', '退回', 0, '', 1, '', '', 0, '抄送', 0, 0, '', '', '移交', 0, '删除', 1, '结束流程', 0, '查看父流程', 0, '公文主文件', 0, '打印Html', 0, '打印pdf', 0, 0, '', '打包下载', 0, '打印单据', 0, '轨迹', 0, '挂起', 0, '查询', 0, '会签', 0, 0, '流转自定义', 0, '公文', 0, '重要性', 0, '节点时限', 0, '分配', 0, '关注', 0, '确认', 0, '列表', 0, '批量审核', 0, '备注', 0, '帮助', 0, 0, 0, 0, 0, 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 4, '前台', 1, '', 2.00, 0, 1.00, '', '', '', 0, 0, 0, 0, '', 0, 0, '', '', '', 0, 12, '', 0, 'http://', 0, '', 0, 0, '', '', '@302', '', '', '', '@301@302', 200, 150, '', '@IsYouLiTai=0', 300.00, 400.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, '', '', 0, 0, 0, 0, '', '', '', '');
INSERT INTO `wf_node` VALUES (302, 2, '003', '流程1', 'Node 2', '', 0, 0, 2, 0, 0, 1, 1, 1, 0, 0, 0, '', '', 0, '', 0, 0, 0, 0, '', 0, 0, 100, 0, '', 0, 0, 0, 0, '', 0, 0, 0, 0, '发送', '', '保存', 0, '子线程', 0, 0, '跳转', 0, '', '退回', 1, '', 1, '', '', 0, '抄送', 0, 0, '', '', '移交', 0, '删除', 0, '结束流程', 0, '查看父流程', 0, '公文主文件', 0, '打印Html', 0, '打印pdf', 0, 0, '', '打包下载', 0, '打印单据', 0, '轨迹', 0, '挂起', 0, '查询', 0, '会签', 0, 0, '流转自定义', 0, '公文', 0, '重要性', 0, '节点时限', 0, '分配', 0, '关注', 0, '确认', 0, '列表', 0, '批量审核', 0, '备注', 0, '帮助', 0, 0, 0, 0, 0, 2, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 4, '审核', 0, '', 1.00, 1, 1.00, '', '', '', 0, 0, 0, 0, '', 0, 0, '', '', '', 0, 12, '', 0, 'http://', 0, '', 2, 0, '', '', '', '', '', '', '@301@302', 200, 250, '', '@IsYouLiTai=0', 300.00, 400.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, '', '', 0, 0, 0, 0, '', '', '', '');

-- ----------------------------
-- Table structure for wf_nodecancel
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodecancel`;
CREATE TABLE `wf_nodecancel`  (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `CancelTo` int(11) NOT NULL COMMENT '撤销到 - 主键',
  PRIMARY KEY (`FK_Node`, `CancelTo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '可撤销的节点' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_nodecancel
-- ----------------------------

-- ----------------------------
-- Table structure for wf_nodedept
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodedept`;
CREATE TABLE `wf_nodedept`  (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Node`, `FK_Dept`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '节点部门' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_nodedept
-- ----------------------------
INSERT INTO `wf_nodedept` VALUES (102, '100');
INSERT INTO `wf_nodedept` VALUES (102, '101');
INSERT INTO `wf_nodedept` VALUES (102, '102');
INSERT INTO `wf_nodedept` VALUES (102, '103');
INSERT INTO `wf_nodedept` VALUES (102, '104');
INSERT INTO `wf_nodedept` VALUES (102, '105');
INSERT INTO `wf_nodedept` VALUES (102, '106');
INSERT INTO `wf_nodedept` VALUES (102, '107');
INSERT INTO `wf_nodedept` VALUES (102, '108');
INSERT INTO `wf_nodedept` VALUES (102, '109');
INSERT INTO `wf_nodedept` VALUES (103, '100');
INSERT INTO `wf_nodedept` VALUES (103, '101');
INSERT INTO `wf_nodedept` VALUES (103, '102');
INSERT INTO `wf_nodedept` VALUES (103, '103');
INSERT INTO `wf_nodedept` VALUES (103, '104');
INSERT INTO `wf_nodedept` VALUES (103, '105');
INSERT INTO `wf_nodedept` VALUES (103, '106');
INSERT INTO `wf_nodedept` VALUES (103, '107');
INSERT INTO `wf_nodedept` VALUES (103, '108');
INSERT INTO `wf_nodedept` VALUES (103, '109');
INSERT INTO `wf_nodedept` VALUES (103, '200');

-- ----------------------------
-- Table structure for wf_nodeemp
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodeemp`;
CREATE TABLE `wf_nodeemp`  (
  `FK_Node` int(11) NOT NULL COMMENT 'Node - 主键',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '到人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Node`, `FK_Emp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '节点人员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_nodeemp
-- ----------------------------
INSERT INTO `wf_nodeemp` VALUES (102, 'admin');

-- ----------------------------
-- Table structure for wf_nodereturn
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodereturn`;
CREATE TABLE `wf_nodereturn`  (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `ReturnTo` int(11) NOT NULL COMMENT '退回到 - 主键',
  `Dots` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '轨迹信息',
  PRIMARY KEY (`FK_Node`, `ReturnTo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '可退回的节点' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_nodereturn
-- ----------------------------

-- ----------------------------
-- Table structure for wf_nodestation
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodestation`;
CREATE TABLE `wf_nodestation`  (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '工作岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Node`, `FK_Station`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '节点岗位' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_nodestation
-- ----------------------------
INSERT INTO `wf_nodestation` VALUES (103, '100001');
INSERT INTO `wf_nodestation` VALUES (104, '100010');
INSERT INTO `wf_nodestation` VALUES (105, '100100');

-- ----------------------------
-- Table structure for wf_nodesubflow
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodesubflow`;
CREATE TABLE `wf_nodesubflow`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Flow` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主流程编号',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '主流程节点',
  `SubFlowType` int(11) NULL DEFAULT NULL COMMENT '子流程类型',
  `SubFlowModel` int(11) NULL DEFAULT NULL COMMENT '子流程模式',
  `IsAutoSendSubFlowOver` int(11) NULL DEFAULT NULL COMMENT '父子流程结束规则',
  `IsAutoSendSLSubFlowOver` int(11) NULL DEFAULT NULL COMMENT '同级子流程结束规则',
  `SubFlowNo` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子流程编号',
  `SubFlowName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子流程名称',
  `StartOnceOnly` int(11) NULL DEFAULT NULL COMMENT '仅能被调用1次',
  `IsEnableSpecFlowStart` int(11) NULL DEFAULT NULL COMMENT '指定的流程启动后,才能启动该子流程(请在文本框配置子流程).',
  `SpecFlowStart` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子流程编号',
  `SpecFlowStartNote` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `IsEnableSpecFlowOver` int(11) NULL DEFAULT NULL COMMENT '指定的流程结束后,才能启动该子流程(请在文本框配置子流程).',
  `SpecFlowOver` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子流程编号',
  `SpecFlowOverNote` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ExpType` int(11) NULL DEFAULT NULL COMMENT '表达式类型',
  `CondExp` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '条件表达式',
  `YBFlowReturnRole` int(11) NULL DEFAULT NULL COMMENT '退回方式',
  `ReturnToNode` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '要退回的节点',
  `SendModel` int(11) NULL DEFAULT NULL COMMENT '自动触发的子流程发送方式',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序',
  `InvokeTime` int(11) NULL DEFAULT NULL COMMENT '调用时间,枚举类型:0 发送时;1 工作到达时;',
  `CompleteReStart` int(11) NULL DEFAULT NULL COMMENT '该子流程运行结束后才可以重新发起.',
  `IsEnableSQL` int(11) NULL DEFAULT NULL COMMENT '按照指定的SQL配置.',
  `SpecSQL` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SQL语句',
  `IsEnableSameLevelNode` int(11) NULL DEFAULT NULL COMMENT '按照指定平级子流程节点完成后启动.',
  `SameLevelNode` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '平级子流程节点',
  `ReturnToNodeText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '要退回的节点',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '子流程(所有类型子流程属性)' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_nodesubflow
-- ----------------------------

-- ----------------------------
-- Table structure for wf_nodetoolbar
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodetoolbar`;
CREATE TABLE `wf_nodetoolbar`  (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `ExcType` int(11) NULL DEFAULT NULL COMMENT '执行类型,枚举类型:0 超链接;1 函数;',
  `Url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '连接/函数',
  `Target` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目标',
  `ShowWhere` int(11) NULL DEFAULT NULL COMMENT '显示位置,枚举类型:0 树形表单;1 工具栏;',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点',
  `MyFileName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) NULL DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) NULL DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float NULL DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '自定义工具栏' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_nodetoolbar
-- ----------------------------

-- ----------------------------
-- Table structure for wf_part
-- ----------------------------
DROP TABLE IF EXISTS `wf_part`;
CREATE TABLE `wf_part`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点ID',
  `PartType` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `Tag0` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag0',
  `Tag1` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag5',
  `Tag6` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag6',
  `Tag7` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag7',
  `Tag8` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag8',
  `Tag9` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag9',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '配件' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_part
-- ----------------------------

-- ----------------------------
-- Table structure for wf_powermodel
-- ----------------------------
DROP TABLE IF EXISTS `wf_powermodel`;
CREATE TABLE `wf_powermodel`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Model` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块',
  `PowerFlag` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `PowerFlagName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标记名称',
  `PowerCtrlType` int(11) NULL DEFAULT NULL COMMENT '控制类型,枚举类型:0 岗位;1 人员;',
  `EmpNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员编号',
  `EmpName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员名称',
  `StaNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位编号',
  `StaName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位名称',
  `FlowNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FrmID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限模型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_powermodel
-- ----------------------------

-- ----------------------------
-- Table structure for wf_pushmsg
-- ----------------------------
DROP TABLE IF EXISTS `wf_pushmsg`;
CREATE TABLE `wf_pushmsg`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Flow` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点',
  `FK_Event` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '事件类型',
  `PushWay` int(11) NULL DEFAULT NULL COMMENT '推送方式,枚举类型:0 按照指定节点的工作人员;1 按照指定的工作人员;2 按照指定的工作岗位;3 按照指定的部门;4 按照指定的SQL;5 按照系统指定的字段;',
  `PushDoc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '推送保存内容',
  `Tag` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag',
  `SMSPushWay` int(11) NULL DEFAULT NULL COMMENT '短消息发送方式',
  `SMSField` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '短消息字段',
  `SMSDoc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '短消息内容模版',
  `SMSNodes` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SMS节点s',
  `SMSPushModel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '短消息发送设置',
  `MailPushWay` int(11) NULL DEFAULT NULL COMMENT '邮件发送方式',
  `MailAddress` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮件字段',
  `MailTitle` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮件标题模版',
  `MailDoc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '邮件内容模版',
  `MailNodes` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Mail节点s',
  `BySQL` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按照SQL计算',
  `ByEmps` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送给指定的人员',
  `AtPara` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息推送' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_pushmsg
-- ----------------------------
INSERT INTO `wf_pushmsg` VALUES ('03df896f75c24a9989fbbb811ab6fdbd', '002', 202, 'SendSuccess', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('1a95f3357afb41c598e32108a77d11eb', '001', 104, 'SendSuccess', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('5381d01082004f799eb03c3dcd4607c8', '001', 105, 'ReturnAfter', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('553f51ac02974fab9f5438a8caaa9f89', '001', 101, 'ReturnAfter', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('58317310a1964e3880c03a55c0a8b740', '001', 103, 'ReturnAfter', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('5a826e9e05a941c3a40f3941c34aec99', '001', 101, 'SendSuccess', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('6fbe34774ba848db952a0284cdee6fdd', '003', 302, 'SendSuccess', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('82d55c65fedb4d89ac48feda6d5e7286', '003', 301, 'ReturnAfter', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('859e76654a6648e898a159d9abea0382', '001', 103, 'SendSuccess', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('9778d7bd30ea456aad18598173709fbc', '003', 302, 'ReturnAfter', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('99be4d28ec804aa7945ec54a1113c48e', '002', 201, 'SendSuccess', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('9c1c9ecfdcac4d13bb75b0350fee12a0', '003', 301, 'SendSuccess', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('a2c422c1d7204c6cb9c244f8e767dd3a', '001', 105, 'SendSuccess', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('a2e61d67ee23430d80c736da5b01bada', '002', 202, 'ReturnAfter', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('ac909b1443d84390991e4bb82601ccb8', '001', 104, 'ReturnAfter', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('ae0d94013c6a42ca93e611283410b0d2', '001', 102, 'SendSuccess', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('db9e140a25ed471bb0edfaea2733aa16', '002', 201, 'ReturnAfter', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('e499fb9dae9a49d5acfdbddd0ce6c984', '001', 102, 'ReturnAfter', 0, '', '', 1, '', '', '', '', 0, '', '', '', '', '', '', '');

-- ----------------------------
-- Table structure for wf_rememberme
-- ----------------------------
DROP TABLE IF EXISTS `wf_rememberme`;
CREATE TABLE `wf_rememberme`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点',
  `FK_Emp` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当前操作人员',
  `Objs` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '分配人员',
  `ObjsExt` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '分配人员Ext',
  `Emps` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '所有的工作人员',
  `EmpsExt` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '工作人员Ext',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '记忆我' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_rememberme
-- ----------------------------

-- ----------------------------
-- Table structure for wf_returnwork
-- ----------------------------
DROP TABLE IF EXISTS `wf_returnwork`;
CREATE TABLE `wf_returnwork`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT 'WorkID',
  `ReturnNode` int(11) NULL DEFAULT NULL COMMENT '退回节点',
  `ReturnNodeName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '退回节点名称',
  `Returner` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '退回人',
  `ReturnerName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '退回人名称',
  `ReturnToNode` int(11) NULL DEFAULT NULL COMMENT 'ReturnToNode',
  `ReturnToEmp` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '退回给',
  `BeiZhu` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '退回原因',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '退回日期',
  `IsBackTracking` int(11) NULL DEFAULT NULL COMMENT '是否要原路返回?',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '退回轨迹' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_returnwork
-- ----------------------------
INSERT INTO `wf_returnwork` VALUES ('8d33ed6d2a8a4c1a82281b33f955d4ef', 101, 102, '申请', 'admin', '超级管理员', 101, 'admin', '345345', '2020-02-13 18:39', 1);

-- ----------------------------
-- Table structure for wf_selectaccper
-- ----------------------------
DROP TABLE IF EXISTS `wf_selectaccper`;
CREATE TABLE `wf_selectaccper`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '接受人节点',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT 'WorkID',
  `FK_Emp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_Emp',
  `EmpName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'EmpName',
  `DeptName` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `AccType` int(11) NULL DEFAULT NULL COMMENT '类型(@0=接受人@1=抄送人)',
  `Rec` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录人',
  `Info` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '办理意见信息',
  `IsRemember` int(11) NULL DEFAULT NULL COMMENT '以后发送是否按本次计算',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号(可以用于流程队列审核模式)',
  `Tag` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '维度信息Tag',
  `TimeLimit` int(11) NULL DEFAULT NULL COMMENT '时限-天',
  `TSpanHour` float NULL DEFAULT NULL COMMENT '时限-小时',
  `ADT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到达日期(计划)',
  `SDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应完成日期(计划)',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '选择接受/抄送人信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_selectaccper
-- ----------------------------

-- ----------------------------
-- Table structure for wf_shiftwork
-- ----------------------------
DROP TABLE IF EXISTS `wf_shiftwork`;
CREATE TABLE `wf_shiftwork`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT '工作ID',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT 'FK_Node',
  `FK_Emp` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移交人',
  `FK_EmpName` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移交人名称',
  `ToEmp` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移交给',
  `ToEmpName` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移交给名称',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移交时间',
  `Note` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移交原因',
  `IsRead` int(11) NULL DEFAULT NULL COMMENT '是否读取？',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '移交记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_shiftwork
-- ----------------------------

-- ----------------------------
-- Table structure for wf_sqltemplate
-- ----------------------------
DROP TABLE IF EXISTS `wf_sqltemplate`;
CREATE TABLE `wf_sqltemplate`  (
  `No` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `SQLType` int(11) NULL DEFAULT NULL COMMENT '模版SQL类型,枚举类型:0 方向条件;1 接受人规则;2 下拉框数据过滤;3 级联下拉框;4 PopVal开窗返回值;5 人员选择器人员选择范围;',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SQL说明',
  `Docs` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'SQL模版',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'SQL模板' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_sqltemplate
-- ----------------------------

-- ----------------------------
-- Table structure for wf_task
-- ----------------------------
DROP TABLE IF EXISTS `wf_task`;
CREATE TABLE `wf_task`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Flow` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `Starter` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起人',
  `ToNode` int(11) NULL DEFAULT NULL COMMENT '到达的节点',
  `ToEmps` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到达人员',
  `Paras` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '参数',
  `TaskSta` int(11) NULL DEFAULT NULL COMMENT '任务状态',
  `Msg` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '消息',
  `StartDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起时间',
  `RDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插入数据时间',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '任务' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_task
-- ----------------------------

-- ----------------------------
-- Table structure for wf_testapi
-- ----------------------------
DROP TABLE IF EXISTS `wf_testapi`;
CREATE TABLE `wf_testapi`  (
  `No` varchar(92) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '测试过程' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_testapi
-- ----------------------------

-- ----------------------------
-- Table structure for wf_testcase
-- ----------------------------
DROP TABLE IF EXISTS `wf_testcase`;
CREATE TABLE `wf_testcase`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `ParaType` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数类型',
  `Vals` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '值s',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '自定义流程测试' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_testcase
-- ----------------------------

-- ----------------------------
-- Table structure for wf_testsample
-- ----------------------------
DROP TABLE IF EXISTS `wf_testsample`;
CREATE TABLE `wf_testsample`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '测试名称',
  `FK_API` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '测试的API,外键:对应物理表:WF_TestAPI,表描述:测试过程',
  `FK_Ver` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '测试的版本,外键:对应物理表:WF_TestVer,表描述:测试版本',
  `DTFrom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '从',
  `DTTo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到',
  `TimeUse` float NULL DEFAULT NULL COMMENT '用时(毫秒)',
  `TimesPerSecond` float NULL DEFAULT NULL COMMENT '每秒跑多少个?',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '测试明细' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_testsample
-- ----------------------------

-- ----------------------------
-- Table structure for wf_testver
-- ----------------------------
DROP TABLE IF EXISTS `wf_testver`;
CREATE TABLE `wf_testver`  (
  `No` varchar(92) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '测试版本' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_testver
-- ----------------------------

-- ----------------------------
-- Table structure for wf_transfercustom
-- ----------------------------
DROP TABLE IF EXISTS `wf_transfercustom`;
CREATE TABLE `wf_transfercustom`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT 'WorkID',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点ID',
  `NodeName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `Worker` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '处理人(多个人用逗号分开)',
  `WorkerName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '处理人(多个人用逗号分开)',
  `SubFlowNo` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '要经过的子流程编号',
  `PlanDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '计划完成日期',
  `TodolistModel` int(11) NULL DEFAULT NULL COMMENT '多人工作处理模式',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '自定义运行路径' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_transfercustom
-- ----------------------------

-- ----------------------------
-- Table structure for wf_workflowdeletelog
-- ----------------------------
DROP TABLE IF EXISTS `wf_workflowdeletelog`;
CREATE TABLE `wf_workflowdeletelog`  (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `FID` int(11) NULL DEFAULT NULL COMMENT 'FID',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门,外键:对应物理表:Port_Dept,表描述:部门',
  `Title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `FlowStarter` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起人',
  `FlowStartRDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起时间',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程,外键:对应物理表:WF_Flow,表描述:流程',
  `FlowEnderRDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后处理时间',
  `FlowEndNode` int(11) NULL DEFAULT NULL COMMENT '停留节点',
  `FlowDaySpan` float NULL DEFAULT NULL COMMENT '跨度(天)',
  `FlowEmps` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参与人',
  `Oper` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除人员',
  `OperDept` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除人员部门',
  `OperDeptName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除人员名称',
  `DeleteNote` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '删除原因',
  `DeleteDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除日期',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '流程删除日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wf_workflowdeletelog
-- ----------------------------
-- ----------------------------
-- 1、存储每一个已配置的 jobDetail 的详细信息
-- ----------------------------
drop table if exists QRTZ_JOB_DETAILS;
create table QRTZ_JOB_DETAILS (
    sched_name           varchar(120)    not null,
    job_name             varchar(200)    not null,
    job_group            varchar(200)    not null,
    description          varchar(250)    null,
    job_class_name       varchar(250)    not null,
    is_durable           varchar(1)      not null,
    is_nonconcurrent     varchar(1)      not null,
    is_update_data       varchar(1)      not null,
    requests_recovery    varchar(1)      not null,
    job_data             blob            null,
    primary key (sched_name,job_name,job_group)
) engine=innodb default charset=utf8;

-- ----------------------------
-- 2、 存储已配置的 Trigger 的信息
-- ----------------------------
drop table if exists QRTZ_TRIGGERS;
create table QRTZ_TRIGGERS (
    sched_name           varchar(120)    not null,
    trigger_name         varchar(200)    not null,
    trigger_group        varchar(200)    not null,
    job_name             varchar(200)    not null,
    job_group            varchar(200)    not null,
    description          varchar(250)    null,
    next_fire_time       bigint(13)      null,
    prev_fire_time       bigint(13)      null,
    priority             integer         null,
    trigger_state        varchar(16)     not null,
    trigger_type         varchar(8)      not null,
    start_time           bigint(13)      not null,
    end_time             bigint(13)      null,
    calendar_name        varchar(200)    null,
    misfire_instr        smallint(2)     null,
    job_data             blob            null,
    primary key (sched_name,trigger_name,trigger_group),
    foreign key (sched_name,job_name,job_group) references QRTZ_JOB_DETAILS(sched_name,job_name,job_group)
) engine=innodb default charset=utf8;

-- ----------------------------
-- 3、 存储简单的 Trigger，包括重复次数，间隔，以及已触发的次数
-- ----------------------------
drop table if exists QRTZ_SIMPLE_TRIGGERS;
create table QRTZ_SIMPLE_TRIGGERS (
    sched_name           varchar(120)    not null,
    trigger_name         varchar(200)    not null,
    trigger_group        varchar(200)    not null,
    repeat_count         bigint(7)       not null,
    repeat_interval      bigint(12)      not null,
    times_triggered      bigint(10)      not null,
    primary key (sched_name,trigger_name,trigger_group),
    foreign key (sched_name,trigger_name,trigger_group) references QRTZ_TRIGGERS(sched_name,trigger_name,trigger_group)
) engine=innodb default charset=utf8;

-- ----------------------------
-- 4、 存储 Cron Trigger，包括 Cron 表达式和时区信息
-- ---------------------------- 
drop table if exists QRTZ_CRON_TRIGGERS;
create table QRTZ_CRON_TRIGGERS (
    sched_name           varchar(120)    not null,
    trigger_name         varchar(200)    not null,
    trigger_group        varchar(200)    not null,
    cron_expression      varchar(200)    not null,
    time_zone_id         varchar(80),
    primary key (sched_name,trigger_name,trigger_group),
    foreign key (sched_name,trigger_name,trigger_group) references QRTZ_TRIGGERS(sched_name,trigger_name,trigger_group)
) engine=innodb default charset=utf8;

-- ----------------------------
-- 5、 Trigger 作为 Blob 类型存储(用于 Quartz 用户用 JDBC 创建他们自己定制的 Trigger 类型，JobStore 并不知道如何存储实例的时候)
-- ---------------------------- 
drop table if exists QRTZ_BLOB_TRIGGERS;
create table QRTZ_BLOB_TRIGGERS (
    sched_name           varchar(120)    not null,
    trigger_name         varchar(200)    not null,
    trigger_group        varchar(200)    not null,
    blob_data            blob            null,
    primary key (sched_name,trigger_name,trigger_group),
    foreign key (sched_name,trigger_name,trigger_group) references QRTZ_TRIGGERS(sched_name,trigger_name,trigger_group)
) engine=innodb default charset=utf8;

-- ----------------------------
-- 6、 以 Blob 类型存储存放日历信息， quartz可配置一个日历来指定一个时间范围
-- ---------------------------- 
drop table if exists QRTZ_CALENDARS;
create table QRTZ_CALENDARS (
    sched_name           varchar(120)    not null,
    calendar_name        varchar(200)    not null,
    calendar             blob            not null,
    primary key (sched_name,calendar_name)
) engine=innodb default charset=utf8;

-- ----------------------------
-- 7、 存储已暂停的 Trigger 组的信息
-- ---------------------------- 
drop table if exists QRTZ_PAUSED_TRIGGER_GRPS;
create table QRTZ_PAUSED_TRIGGER_GRPS (
    sched_name           varchar(120)    not null,
    trigger_group        varchar(200)    not null,
    primary key (sched_name,trigger_group)
) engine=innodb default charset=utf8;

-- ----------------------------
-- 8、 存储与已触发的 Trigger 相关的状态信息，以及相联 Job 的执行信息
-- ---------------------------- 
drop table if exists QRTZ_FIRED_TRIGGERS;
create table QRTZ_FIRED_TRIGGERS (
    sched_name           varchar(120)    not null,
    entry_id             varchar(95)     not null,
    trigger_name         varchar(200)    not null,
    trigger_group        varchar(200)    not null,
    instance_name        varchar(200)    not null,
    fired_time           bigint(13)      not null,
    sched_time           bigint(13)      not null,
    priority             integer         not null,
    state                varchar(16)     not null,
    job_name             varchar(200)    null,
    job_group            varchar(200)    null,
    is_nonconcurrent     varchar(1)      null,
    requests_recovery    varchar(1)      null,
    primary key (sched_name,entry_id)
) engine=innodb default charset=utf8;

-- ----------------------------
-- 9、 存储少量的有关 Scheduler 的状态信息，假如是用于集群中，可以看到其他的 Scheduler 实例
-- ---------------------------- 
drop table if exists QRTZ_SCHEDULER_STATE; 
create table QRTZ_SCHEDULER_STATE (
    sched_name           varchar(120)    not null,
    instance_name        varchar(200)    not null,
    last_checkin_time    bigint(13)      not null,
    checkin_interval     bigint(13)      not null,
    primary key (sched_name,instance_name)
) engine=innodb default charset=utf8;

-- ----------------------------
-- 10、 存储程序的悲观锁的信息(假如使用了悲观锁)
-- ---------------------------- 
drop table if exists QRTZ_LOCKS;
create table QRTZ_LOCKS (
    sched_name           varchar(120)    not null,
    lock_name            varchar(40)     not null,
    primary key (sched_name,lock_name)
) engine=innodb default charset=utf8;

drop table if exists QRTZ_SIMPROP_TRIGGERS;
create table QRTZ_SIMPROP_TRIGGERS (
    sched_name           varchar(120)    not null,
    trigger_name         varchar(200)    not null,
    trigger_group        varchar(200)    not null,
    str_prop_1           varchar(512)    null,
    str_prop_2           varchar(512)    null,
    str_prop_3           varchar(512)    null,
    int_prop_1           int             null,
    int_prop_2           int             null,
    long_prop_1          bigint          null,
    long_prop_2          bigint          null,
    dec_prop_1           numeric(13,4)   null,
    dec_prop_2           numeric(13,4)   null,
    bool_prop_1          varchar(1)      null,
    bool_prop_2          varchar(1)      null,
    primary key (sched_name,trigger_name,trigger_group),
    foreign key (sched_name,trigger_name,trigger_group) references QRTZ_TRIGGERS(sched_name,trigger_name,trigger_group)
) engine=innodb default charset=utf8;

commit;


-- ----------------------------
-- View structure for port_dept
-- ----------------------------
DROP VIEW IF EXISTS `port_dept`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_dept` AS select `d`.`dept_id` AS `No`,`d`.`dept_name` AS `Name`,`jflow_dept`.`NameOfPath` AS `NameOfPath`,`d`.`parent_id` AS `ParentNo`,`jflow_dept`.`Idx` AS `Idx`,`jflow_dept`.`OrgNo` AS `OrgNo` from (`sys_dept` `d` left join `jflow_dept` on((`d`.`dept_id` = `jflow_dept`.`dept_id`)))where (`d`.`del_flag` = 0);

-- ----------------------------
-- View structure for port_deptemp
-- ----------------------------
DROP VIEW IF EXISTS `port_deptemp`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_deptemp` AS select `jflow_deptemp`.`MyPK` AS `MyPK`,`jflow_deptemp`.`FK_Dept` AS `FK_Dept`,`jflow_deptemp`.`FK_Emp` AS `FK_Emp` from `jflow_deptemp`;

-- ----------------------------
-- View structure for port_deptempstation
-- ----------------------------
DROP VIEW IF EXISTS `port_deptempstation`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_deptempstation` AS select `jflow_deptempstation`.`MyPK` AS `MyPK`,`jflow_deptempstation`.`FK_Dept` AS `FK_Dept`,`jflow_deptempstation`.`FK_Station` AS `FK_Station`,`jflow_deptempstation`.`FK_Emp` AS `FK_Emp` from `jflow_deptempstation`;

-- ----------------------------
-- View structure for port_deptstation
-- ----------------------------
DROP VIEW IF EXISTS `port_deptstation`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_deptstation` AS select `jflow_deptstation`.`FK_Dept` AS `FK_Dept`,`jflow_deptstation`.`FK_Station` AS `FK_Station` from `jflow_deptstation`;

-- ----------------------------
-- View structure for port_emp
-- ----------------------------
DROP VIEW IF EXISTS `port_emp`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_emp` AS select `u`.`login_name` AS `NO`,`u`.`user_name` AS `Name`,`u`.`password` AS `Pass`,concat(`u`.`dept_id`,'') AS `FK_Dept`,`e`.`SID` AS `SID`,`u`.`phonenumber` AS `Tel`,`u`.`email` AS `Email`,`e`.`PinYin` AS `PinYin`,`e`.`SignType` AS `SignType`,`e`.`Idx` AS `Idx` from (`sys_user` `u` left join `jflow_emp` `e` on((`u`.`user_id` = `e`.`user_id`)));

-- ----------------------------
-- View structure for port_inc
-- ----------------------------
DROP VIEW IF EXISTS `port_inc`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_inc` AS select `port_dept`.`No` AS `No`,`port_dept`.`Name` AS `Name`,`port_dept`.`NameOfPath` AS `NameOfPath`,`port_dept`.`ParentNo` AS `ParentNo`,`port_dept`.`Idx` AS `Idx`,`port_dept`.`OrgNo` AS `OrgNo` from `port_dept` where ((`port_dept`.`No` = '100') or (`port_dept`.`No` = '1060') or (`port_dept`.`No` = '1070'));

-- ----------------------------
-- View structure for port_station
-- ----------------------------
DROP VIEW IF EXISTS `port_station`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_station` AS select concat(`p`.`post_id`,'') AS `No`,`p`.`post_name` AS `Name`,`s`.`FK_StationType` AS `FK_StationType`,`s`.`OrgNo` AS `OrgNo` from (`sys_post` `p` left join `jflow_station` `s` on((`p`.`post_id` = `s`.`post_id`)));

-- ----------------------------
-- View structure for port_stationtype
-- ----------------------------
DROP VIEW IF EXISTS `port_stationtype`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_stationtype` AS select `jflow_stationtype`.`No` AS `No`,`jflow_stationtype`.`Name` AS `Name`,`jflow_stationtype`.`Idx` AS `Idx`,`jflow_stationtype`.`OrgNo` AS `OrgNo` from `jflow_stationtype`;

-- ----------------------------
-- View structure for v_flowstarter
-- ----------------------------
DROP VIEW IF EXISTS `v_flowstarter`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_flowstarter` AS select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`FK_Emp` AS `FK_Emp` from ((`wf_node` `a` join `wf_nodestation` `b`) join `port_deptempstation` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Station` = `c`.`FK_Station`) and ((`a`.`DeliveryWay` = 0) or (`a`.`DeliveryWay` = 14))) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`NO` AS `No` from ((`wf_node` `a` join `wf_nodedept` `b`) join `port_emp` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Dept` = convert(`c`.`FK_Dept` using utf8)) and (`a`.`DeliveryWay` = 1)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`FK_Emp` AS `FK_Emp` from (`wf_node` `a` join `wf_nodeemp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`a`.`DeliveryWay` = 3)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`NO` AS `FK_Emp` from (`wf_node` `a` join `port_emp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`DeliveryWay` = 4)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`e`.`FK_Emp` AS `FK_Emp` from ((((`wf_node` `a` join `wf_nodedept` `b`) join `wf_nodestation` `c`) join `port_emp` `d`) join `port_deptempstation` `e`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`a`.`NodeID` = `c`.`FK_Node`) and (`b`.`FK_Dept` = convert(`d`.`FK_Dept` using utf8)) and (`c`.`FK_Station` = `e`.`FK_Station`) and (`a`.`DeliveryWay` = 9));

-- ----------------------------
-- View structure for v_flowstarterbpm
-- ----------------------------
DROP VIEW IF EXISTS `v_flowstarterbpm`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_flowstarterbpm` AS select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`FK_Emp` AS `FK_Emp` from ((`wf_node` `a` join `wf_nodestation` `b`) join `port_deptempstation` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Station` = `c`.`FK_Station`) and ((`a`.`DeliveryWay` = 0) or (`a`.`DeliveryWay` = 14))) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`FK_Emp` AS `FK_Emp` from ((`wf_node` `a` join `wf_nodedept` `b`) join `port_deptemp` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Dept` = `c`.`FK_Dept`) and (`a`.`DeliveryWay` = 1)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`FK_Emp` AS `FK_Emp` from (`wf_node` `a` join `wf_nodeemp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`a`.`DeliveryWay` = 3)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`NO` AS `FK_Emp` from (`wf_node` `a` join `port_emp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`DeliveryWay` = 4)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`e`.`FK_Emp` AS `FK_Emp` from (((`wf_node` `a` join `wf_nodedept` `b`) join `wf_nodestation` `c`) join `port_deptempstation` `e`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`a`.`NodeID` = `c`.`FK_Node`) and (`b`.`FK_Dept` = `e`.`FK_Dept`) and (`c`.`FK_Station` = `e`.`FK_Station`) and (`a`.`DeliveryWay` = 9));

-- ----------------------------
-- View structure for v_myflowdata
-- ----------------------------
DROP VIEW IF EXISTS `v_myflowdata`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_myflowdata` AS select `a`.`WorkID` AS `WorkID`,`a`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SysType` AS `SysType`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`Title` AS `Title`,`a`.`WFSta` AS `WFSta`,`a`.`WFState` AS `WFState`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,`a`.`Sender` AS `Sender`,`a`.`RDT` AS `RDT`,`a`.`SendDT` AS `SendDT`,`a`.`FK_Node` AS `FK_Node`,`a`.`NodeName` AS `NodeName`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`PRI` AS `PRI`,`a`.`SDTOfNode` AS `SDTOfNode`,`a`.`SDTOfFlow` AS `SDTOfFlow`,`a`.`SDTOfFlowWarning` AS `SDTOfFlowWarning`,`a`.`PFlowNo` AS `PFlowNo`,`a`.`PWorkID` AS `PWorkID`,`a`.`PNodeID` AS `PNodeID`,`a`.`PFID` AS `PFID`,`a`.`PEmp` AS `PEmp`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,`a`.`TaskSta` AS `TaskSta`,`a`.`AtPara` AS `AtPara`,`a`.`Emps` AS `Emps`,`a`.`GUID` AS `GUID`,`a`.`FK_NY` AS `FK_NY`,`a`.`WeekNum` AS `WeekNum`,`a`.`TSpan` AS `TSpan`,`a`.`TodoSta` AS `TodoSta`,`a`.`DoDomain` AS `DoDomain`,`a`.`PrjNo` AS `PrjNo`,`a`.`PrjName` AS `PrjName`,`a`.`MyNum` AS `MyNum`,`b`.`EmpNo` AS `MyEmpNo` from (`wf_generworkflow` `a` join `wf_powermodel` `b`) where ((`a`.`FK_Flow` = `b`.`FlowNo`) and (`b`.`PowerCtrlType` = 1) and (`a`.`WFState` >= 2)) union select `a`.`WorkID` AS `WorkID`,`a`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SysType` AS `SysType`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`Title` AS `Title`,`a`.`WFSta` AS `WFSta`,`a`.`WFState` AS `WFState`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,`a`.`Sender` AS `Sender`,`a`.`RDT` AS `RDT`,`a`.`SendDT` AS `SendDT`,`a`.`FK_Node` AS `FK_Node`,`a`.`NodeName` AS `NodeName`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`PRI` AS `PRI`,`a`.`SDTOfNode` AS `SDTOfNode`,`a`.`SDTOfFlow` AS `SDTOfFlow`,`a`.`SDTOfFlowWarning` AS `SDTOfFlowWarning`,`a`.`PFlowNo` AS `PFlowNo`,`a`.`PWorkID` AS `PWorkID`,`a`.`PNodeID` AS `PNodeID`,`a`.`PFID` AS `PFID`,`a`.`PEmp` AS `PEmp`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,`a`.`TaskSta` AS `TaskSta`,`a`.`AtPara` AS `AtPara`,`a`.`Emps` AS `Emps`,`a`.`GUID` AS `GUID`,`a`.`FK_NY` AS `FK_NY`,`a`.`WeekNum` AS `WeekNum`,`a`.`TSpan` AS `TSpan`,`a`.`TodoSta` AS `TodoSta`,`a`.`DoDomain` AS `DoDomain`,`a`.`PrjNo` AS `PrjNo`,`a`.`PrjName` AS `PrjName`,`a`.`MyNum` AS `MyNum`,`c`.`NO` AS `MyEmpNo` from (((`wf_generworkflow` `a` join `wf_powermodel` `b`) join `port_emp` `c`) join `port_deptempstation` `d`) where ((`a`.`FK_Flow` = `b`.`FlowNo`) and (`b`.`PowerCtrlType` = 0) and (`c`.`NO` = `d`.`FK_Emp`) and (`b`.`StaNo` = `d`.`FK_Station`) and (`a`.`WFState` >= 2));

-- ----------------------------
-- View structure for v_totalch
-- ----------------------------
DROP VIEW IF EXISTS `v_totalch`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_totalch` AS select `wf_ch`.`FK_Emp` AS `FK_Emp`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`)) AS `AllNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `ASNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` >= 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `CSNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 0) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `JiShi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `ANQI`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `YuQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 3) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `ChaoQi`,round((((select cast(count(`a`.`MyPK`) as decimal(10,0)) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) / (select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) * 100),2) AS `WCRate` from `wf_ch` group by `wf_ch`.`FK_Emp`;

-- ----------------------------
-- View structure for v_totalchweek
-- ----------------------------
DROP VIEW IF EXISTS `v_totalchweek`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_totalchweek` AS select `wf_ch`.`FK_Emp` AS `FK_Emp`,`wf_ch`.`WeekNum` AS `WeekNum`,`wf_ch`.`FK_NY` AS `FK_NY`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `AllNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `ASNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` >= 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `CSNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 0) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `JiShi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `AnQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `YuQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 3) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `ChaoQi`,round((((select cast(count(`a`.`MyPK`) as decimal(10,0)) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) / (select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`)))) * 100),2) AS `WCRate` from `wf_ch` group by `wf_ch`.`FK_Emp`,`wf_ch`.`WeekNum`,`wf_ch`.`FK_NY`;

-- ----------------------------
-- View structure for v_totalchyf
-- ----------------------------
DROP VIEW IF EXISTS `v_totalchyf`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_totalchyf` AS select `wf_ch`.`FK_Emp` AS `FK_Emp`,`wf_ch`.`FK_NY` AS `FK_NY`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `AllNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `ASNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` >= 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `CSNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 0) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `JiShi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `AnQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `YuQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 3) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `ChaoQi`,round((((select cast(count(`a`.`MyPK`) as decimal(10,0)) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) / (select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`)))) * 100),2) AS `WCRate` from `wf_ch` group by `wf_ch`.`FK_Emp`,`wf_ch`.`FK_NY`;

-- ----------------------------
-- View structure for v_wf_authtodolist
-- ----------------------------
DROP VIEW IF EXISTS `v_wf_authtodolist`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_wf_authtodolist` AS select `b`.`FK_Emp` AS `Auther`,`b`.`FK_EmpText` AS `AuthName`,`a`.`PWorkID` AS `PWorkID`,`a`.`FK_Node` AS `FK_Node`,`a`.`FID` AS `FID`,`a`.`WorkID` AS `WorkID`,`c`.`EmpNo` AS `EmpNo`,`c`.`TakeBackDT` AS `TakeBackDT`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`Title` AS `Title` from ((`wf_generworkflow` `a` join `wf_generworkerlist` `b`) join `wf_auth` `c`) where ((`a`.`WorkID` = `b`.`WorkID`) and (`c`.`AuthType` = 1) and (`b`.`FK_Emp` = `c`.`Auther`) and (`b`.`IsPass` = 0) and (`b`.`IsEnable` = 1) and (`a`.`FK_Node` = `b`.`FK_Node`) and (`a`.`WFState` >= 2)) union select `b`.`FK_Emp` AS `Auther`,`b`.`FK_EmpText` AS `AuthName`,`a`.`PWorkID` AS `PWorkID`,`a`.`FK_Node` AS `FK_Node`,`a`.`FID` AS `FID`,`a`.`WorkID` AS `WorkID`,`c`.`EmpNo` AS `EmpNo`,`c`.`TakeBackDT` AS `TakeBackDT`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`Title` AS `Title` from ((`wf_generworkflow` `a` join `wf_generworkerlist` `b`) join `wf_auth` `c`) where ((`a`.`WorkID` = `b`.`WorkID`) and (`c`.`AuthType` = 2) and (`b`.`FK_Emp` = `c`.`Auther`) and (`b`.`IsPass` = 0) and (`b`.`IsEnable` = 1) and (`a`.`FK_Node` = `b`.`FK_Node`) and (`a`.`WFState` >= 2) and (`a`.`FK_Flow` = `c`.`FlowNo`));


-- ----------------------------
-- View structure for wf_empworks
-- ----------------------------
DROP VIEW IF EXISTS `wf_empworks`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `wf_empworks` AS select `a`.`PRI` AS `PRI`,`a`.`WorkID` AS `WorkID`,`b`.`IsRead` AS `IsRead`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,`a`.`WFState` AS `WFState`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`PWorkID` AS `PWorkID`,`a`.`PFlowNo` AS `PFlowNo`,`b`.`FK_Node` AS `FK_Node`,`b`.`FK_NodeText` AS `NodeName`,`b`.`FK_Dept` AS `WorkerDept`,`a`.`Title` AS `Title`,`a`.`RDT` AS `RDT`,`b`.`RDT` AS `ADT`,`b`.`SDT` AS `SDT`,`b`.`FK_Emp` AS `FK_Emp`,`b`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SysType` AS `SysType`,`a`.`SDTOfNode` AS `SDTOfNode`,`b`.`PressTimes` AS `PressTimes`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,`a`.`TodoSta` AS `TodoSta`,`a`.`TaskSta` AS `TaskSta`,0 AS `ListType`,`a`.`Sender` AS `Sender`,`a`.`AtPara` AS `AtPara`,1 AS `MyNum` from (`wf_generworkflow` `a` join `wf_generworkerlist` `b`) where ((`b`.`IsEnable` = 1) and (`b`.`IsPass` = 0) and (`a`.`WorkID` = `b`.`WorkID`) and (`a`.`FK_Node` = `b`.`FK_Node`) and (`a`.`WFState` <> 0) and (`b`.`WhoExeIt` <> 1)) union select `a`.`PRI` AS `PRI`,`a`.`WorkID` AS `WorkID`,`b`.`Sta` AS `IsRead`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,2 AS `WFState`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`PWorkID` AS `PWorkID`,`a`.`PFlowNo` AS `PFlowNo`,`b`.`FK_Node` AS `FK_Node`,`b`.`NodeName` AS `NodeName`,`b`.`CCToDept` AS `WorkerDept`,`a`.`Title` AS `Title`,`a`.`RDT` AS `RDT`,`b`.`RDT` AS `ADT`,`b`.`RDT` AS `SDT`,`b`.`CCTo` AS `FK_Emp`,`b`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SysType` AS `SysType`,`a`.`SDTOfNode` AS `SDTOfNode`,0 AS `PressTimes`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,0 AS `TodoSta`,0 AS `TaskSta`,1 AS `ListType`,`b`.`Rec` AS `Sender`,('@IsCC=1' or `a`.`AtPara`) AS `AtPara`,1 AS `MyNum` from (`wf_generworkflow` `a` join `wf_cclist` `b`) where ((`a`.`WorkID` = `b`.`WorkID`) and (`b`.`Sta` <= 1) and (`b`.`InEmpWorks` = 1) and (`a`.`WFState` <> 0));

-- ----------------------------
-- View structure for v_wf_delay
-- ----------------------------
DROP VIEW IF EXISTS `v_wf_delay`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_wf_delay` AS select concat(`wf_empworks`.`WorkID`,'_',`wf_empworks`.`FK_Emp`,'_',`wf_empworks`.`FK_Node`) AS `MyPK`,`wf_empworks`.`PRI` AS `PRI`,`wf_empworks`.`WorkID` AS `WorkID`,`wf_empworks`.`IsRead` AS `IsRead`,`wf_empworks`.`Starter` AS `Starter`,`wf_empworks`.`StarterName` AS `StarterName`,`wf_empworks`.`WFState` AS `WFState`,`wf_empworks`.`FK_Dept` AS `FK_Dept`,`wf_empworks`.`DeptName` AS `DeptName`,`wf_empworks`.`FK_Flow` AS `FK_Flow`,`wf_empworks`.`FlowName` AS `FlowName`,`wf_empworks`.`PWorkID` AS `PWorkID`,`wf_empworks`.`PFlowNo` AS `PFlowNo`,`wf_empworks`.`FK_Node` AS `FK_Node`,`wf_empworks`.`NodeName` AS `NodeName`,`wf_empworks`.`WorkerDept` AS `WorkerDept`,`wf_empworks`.`Title` AS `Title`,`wf_empworks`.`RDT` AS `RDT`,`wf_empworks`.`ADT` AS `ADT`,`wf_empworks`.`SDT` AS `SDT`,`wf_empworks`.`FK_Emp` AS `FK_Emp`,`wf_empworks`.`FID` AS `FID`,`wf_empworks`.`FK_FlowSort` AS `FK_FlowSort`,`wf_empworks`.`SysType` AS `SysType`,`wf_empworks`.`SDTOfNode` AS `SDTOfNode`,`wf_empworks`.`PressTimes` AS `PressTimes`,`wf_empworks`.`GuestNo` AS `GuestNo`,`wf_empworks`.`GuestName` AS `GuestName`,`wf_empworks`.`BillNo` AS `BillNo`,`wf_empworks`.`FlowNote` AS `FlowNote`,`wf_empworks`.`TodoEmps` AS `TodoEmps`,`wf_empworks`.`TodoEmpsNum` AS `TodoEmpsNum`,`wf_empworks`.`TodoSta` AS `TodoSta`,`wf_empworks`.`TaskSta` AS `TaskSta`,`wf_empworks`.`ListType` AS `ListType`,`wf_empworks`.`Sender` AS `Sender`,`wf_empworks`.`AtPara` AS `AtPara`,`wf_empworks`.`MyNum` AS `MyNum` from `wf_empworks` where (`wf_empworks`.`SDT` > now());

SET FOREIGN_KEY_CHECKS = 1;


